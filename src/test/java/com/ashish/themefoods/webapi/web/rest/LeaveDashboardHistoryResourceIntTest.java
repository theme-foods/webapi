package com.ashish.themefoods.webapi.web.rest;

import com.ashish.themefoods.webapi.WebapiApp;

import com.ashish.themefoods.webapi.domain.LeaveDashboardHistory;
import com.ashish.themefoods.webapi.repository.LeaveDashboardHistoryRepository;
import com.ashish.themefoods.webapi.service.LeaveDashboardHistoryService;
import com.ashish.themefoods.webapi.service.dto.LeaveDashboardHistoryDTO;
import com.ashish.themefoods.webapi.service.mapper.LeaveDashboardHistoryMapper;
import com.ashish.themefoods.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.ashish.themefoods.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LeaveDashboardHistoryResource REST controller.
 *
 * @see LeaveDashboardHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class LeaveDashboardHistoryResourceIntTest {

    private static final Double DEFAULT_AVAILABLE_LEAVES = 1D;
    private static final Double UPDATED_AVAILABLE_LEAVES = 2D;

    private static final Double DEFAULT_TAKEN_LEAVES = 1D;
    private static final Double UPDATED_TAKEN_LEAVES = 2D;

    @Autowired
    private LeaveDashboardHistoryRepository leaveDashboardHistoryRepository;

    @Autowired
    private LeaveDashboardHistoryMapper leaveDashboardHistoryMapper;

    @Autowired
    private LeaveDashboardHistoryService leaveDashboardHistoryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restLeaveDashboardHistoryMockMvc;

    private LeaveDashboardHistory leaveDashboardHistory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LeaveDashboardHistoryResource leaveDashboardHistoryResource = new LeaveDashboardHistoryResource(leaveDashboardHistoryService);
        this.restLeaveDashboardHistoryMockMvc = MockMvcBuilders.standaloneSetup(leaveDashboardHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LeaveDashboardHistory createEntity(EntityManager em) {
        LeaveDashboardHistory leaveDashboardHistory = new LeaveDashboardHistory()
            .availableLeaves(DEFAULT_AVAILABLE_LEAVES)
            .takenLeaves(DEFAULT_TAKEN_LEAVES);
        return leaveDashboardHistory;
    }

    @Before
    public void initTest() {
        leaveDashboardHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createLeaveDashboardHistory() throws Exception {
        int databaseSizeBeforeCreate = leaveDashboardHistoryRepository.findAll().size();

        // Create the LeaveDashboardHistory
        LeaveDashboardHistoryDTO leaveDashboardHistoryDTO = leaveDashboardHistoryMapper.toDto(leaveDashboardHistory);
        restLeaveDashboardHistoryMockMvc.perform(post("/api/leave-dashboard-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(leaveDashboardHistoryDTO)))
            .andExpect(status().isCreated());

        // Validate the LeaveDashboardHistory in the database
        List<LeaveDashboardHistory> leaveDashboardHistoryList = leaveDashboardHistoryRepository.findAll();
        assertThat(leaveDashboardHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        LeaveDashboardHistory testLeaveDashboardHistory = leaveDashboardHistoryList.get(leaveDashboardHistoryList.size() - 1);
        assertThat(testLeaveDashboardHistory.getAvailableLeaves()).isEqualTo(DEFAULT_AVAILABLE_LEAVES);
        assertThat(testLeaveDashboardHistory.getTakenLeaves()).isEqualTo(DEFAULT_TAKEN_LEAVES);
    }

    @Test
    @Transactional
    public void createLeaveDashboardHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = leaveDashboardHistoryRepository.findAll().size();

        // Create the LeaveDashboardHistory with an existing ID
        leaveDashboardHistory.setId(1L);
        LeaveDashboardHistoryDTO leaveDashboardHistoryDTO = leaveDashboardHistoryMapper.toDto(leaveDashboardHistory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLeaveDashboardHistoryMockMvc.perform(post("/api/leave-dashboard-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(leaveDashboardHistoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LeaveDashboardHistory in the database
        List<LeaveDashboardHistory> leaveDashboardHistoryList = leaveDashboardHistoryRepository.findAll();
        assertThat(leaveDashboardHistoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLeaveDashboardHistories() throws Exception {
        // Initialize the database
        leaveDashboardHistoryRepository.saveAndFlush(leaveDashboardHistory);

        // Get all the leaveDashboardHistoryList
        restLeaveDashboardHistoryMockMvc.perform(get("/api/leave-dashboard-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(leaveDashboardHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].availableLeaves").value(hasItem(DEFAULT_AVAILABLE_LEAVES.doubleValue())))
            .andExpect(jsonPath("$.[*].takenLeaves").value(hasItem(DEFAULT_TAKEN_LEAVES.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getLeaveDashboardHistory() throws Exception {
        // Initialize the database
        leaveDashboardHistoryRepository.saveAndFlush(leaveDashboardHistory);

        // Get the leaveDashboardHistory
        restLeaveDashboardHistoryMockMvc.perform(get("/api/leave-dashboard-histories/{id}", leaveDashboardHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(leaveDashboardHistory.getId().intValue()))
            .andExpect(jsonPath("$.availableLeaves").value(DEFAULT_AVAILABLE_LEAVES.doubleValue()))
            .andExpect(jsonPath("$.takenLeaves").value(DEFAULT_TAKEN_LEAVES.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingLeaveDashboardHistory() throws Exception {
        // Get the leaveDashboardHistory
        restLeaveDashboardHistoryMockMvc.perform(get("/api/leave-dashboard-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLeaveDashboardHistory() throws Exception {
        // Initialize the database
        leaveDashboardHistoryRepository.saveAndFlush(leaveDashboardHistory);

        int databaseSizeBeforeUpdate = leaveDashboardHistoryRepository.findAll().size();

        // Update the leaveDashboardHistory
        LeaveDashboardHistory updatedLeaveDashboardHistory = leaveDashboardHistoryRepository.findById(leaveDashboardHistory.getId()).get();
        // Disconnect from session so that the updates on updatedLeaveDashboardHistory are not directly saved in db
        em.detach(updatedLeaveDashboardHistory);
        updatedLeaveDashboardHistory
            .availableLeaves(UPDATED_AVAILABLE_LEAVES)
            .takenLeaves(UPDATED_TAKEN_LEAVES);
        LeaveDashboardHistoryDTO leaveDashboardHistoryDTO = leaveDashboardHistoryMapper.toDto(updatedLeaveDashboardHistory);

        restLeaveDashboardHistoryMockMvc.perform(put("/api/leave-dashboard-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(leaveDashboardHistoryDTO)))
            .andExpect(status().isOk());

        // Validate the LeaveDashboardHistory in the database
        List<LeaveDashboardHistory> leaveDashboardHistoryList = leaveDashboardHistoryRepository.findAll();
        assertThat(leaveDashboardHistoryList).hasSize(databaseSizeBeforeUpdate);
        LeaveDashboardHistory testLeaveDashboardHistory = leaveDashboardHistoryList.get(leaveDashboardHistoryList.size() - 1);
        assertThat(testLeaveDashboardHistory.getAvailableLeaves()).isEqualTo(UPDATED_AVAILABLE_LEAVES);
        assertThat(testLeaveDashboardHistory.getTakenLeaves()).isEqualTo(UPDATED_TAKEN_LEAVES);
    }

    @Test
    @Transactional
    public void updateNonExistingLeaveDashboardHistory() throws Exception {
        int databaseSizeBeforeUpdate = leaveDashboardHistoryRepository.findAll().size();

        // Create the LeaveDashboardHistory
        LeaveDashboardHistoryDTO leaveDashboardHistoryDTO = leaveDashboardHistoryMapper.toDto(leaveDashboardHistory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLeaveDashboardHistoryMockMvc.perform(put("/api/leave-dashboard-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(leaveDashboardHistoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LeaveDashboardHistory in the database
        List<LeaveDashboardHistory> leaveDashboardHistoryList = leaveDashboardHistoryRepository.findAll();
        assertThat(leaveDashboardHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLeaveDashboardHistory() throws Exception {
        // Initialize the database
        leaveDashboardHistoryRepository.saveAndFlush(leaveDashboardHistory);

        int databaseSizeBeforeDelete = leaveDashboardHistoryRepository.findAll().size();

        // Delete the leaveDashboardHistory
        restLeaveDashboardHistoryMockMvc.perform(delete("/api/leave-dashboard-histories/{id}", leaveDashboardHistory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LeaveDashboardHistory> leaveDashboardHistoryList = leaveDashboardHistoryRepository.findAll();
        assertThat(leaveDashboardHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LeaveDashboardHistory.class);
        LeaveDashboardHistory leaveDashboardHistory1 = new LeaveDashboardHistory();
        leaveDashboardHistory1.setId(1L);
        LeaveDashboardHistory leaveDashboardHistory2 = new LeaveDashboardHistory();
        leaveDashboardHistory2.setId(leaveDashboardHistory1.getId());
        assertThat(leaveDashboardHistory1).isEqualTo(leaveDashboardHistory2);
        leaveDashboardHistory2.setId(2L);
        assertThat(leaveDashboardHistory1).isNotEqualTo(leaveDashboardHistory2);
        leaveDashboardHistory1.setId(null);
        assertThat(leaveDashboardHistory1).isNotEqualTo(leaveDashboardHistory2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LeaveDashboardHistoryDTO.class);
        LeaveDashboardHistoryDTO leaveDashboardHistoryDTO1 = new LeaveDashboardHistoryDTO();
        leaveDashboardHistoryDTO1.setId(1L);
        LeaveDashboardHistoryDTO leaveDashboardHistoryDTO2 = new LeaveDashboardHistoryDTO();
        assertThat(leaveDashboardHistoryDTO1).isNotEqualTo(leaveDashboardHistoryDTO2);
        leaveDashboardHistoryDTO2.setId(leaveDashboardHistoryDTO1.getId());
        assertThat(leaveDashboardHistoryDTO1).isEqualTo(leaveDashboardHistoryDTO2);
        leaveDashboardHistoryDTO2.setId(2L);
        assertThat(leaveDashboardHistoryDTO1).isNotEqualTo(leaveDashboardHistoryDTO2);
        leaveDashboardHistoryDTO1.setId(null);
        assertThat(leaveDashboardHistoryDTO1).isNotEqualTo(leaveDashboardHistoryDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(leaveDashboardHistoryMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(leaveDashboardHistoryMapper.fromId(null)).isNull();
    }
}
