package com.ashish.themefoods.webapi.web.rest;

import com.ashish.themefoods.webapi.WebapiApp;

import com.ashish.themefoods.webapi.domain.EmployeeLeave;
import com.ashish.themefoods.webapi.repository.EmployeeLeaveRepository;
import com.ashish.themefoods.webapi.service.EmployeeLeaveService;
import com.ashish.themefoods.webapi.service.dto.EmployeeLeaveDTO;
import com.ashish.themefoods.webapi.service.mapper.EmployeeLeaveMapper;
import com.ashish.themefoods.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.ashish.themefoods.webapi.web.rest.TestUtil.sameInstant;
import static com.ashish.themefoods.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EmployeeLeaveResource REST controller.
 *
 * @see EmployeeLeaveResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class EmployeeLeaveResourceIntTest {

    private static final ZonedDateTime DEFAULT_FROM_DATE = ZonedDateTime.ofLocalInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FROM_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_TO_DATE = ZonedDateTime.ofLocalInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TO_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Float DEFAULT_NO_OF_DAYS = 1F;
    private static final Float UPDATED_NO_OF_DAYS = 2F;

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    @Autowired
    private EmployeeLeaveRepository employeeLeaveRepository;

    @Autowired
    private EmployeeLeaveMapper employeeLeaveMapper;

    @Autowired
    private EmployeeLeaveService employeeLeaveService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEmployeeLeaveMockMvc;

    private EmployeeLeave employeeLeave;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EmployeeLeaveResource employeeLeaveResource = new EmployeeLeaveResource(employeeLeaveService);
        this.restEmployeeLeaveMockMvc = MockMvcBuilders.standaloneSetup(employeeLeaveResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmployeeLeave createEntity(EntityManager em) {
        EmployeeLeave employeeLeave = new EmployeeLeave()
            .fromDate(DEFAULT_FROM_DATE)
            .toDate(DEFAULT_TO_DATE)
            .noOfDays(DEFAULT_NO_OF_DAYS)
            .reason(DEFAULT_REASON)
            .status(DEFAULT_STATUS);
        return employeeLeave;
    }

    @Before
    public void initTest() {
        employeeLeave = createEntity(em);
    }

    @Test
    @Transactional
    public void createEmployeeLeave() throws Exception {
        int databaseSizeBeforeCreate = employeeLeaveRepository.findAll().size();

        // Create the EmployeeLeave
        EmployeeLeaveDTO employeeLeaveDTO = employeeLeaveMapper.toDto(employeeLeave);
        restEmployeeLeaveMockMvc.perform(post("/api/employee-leaves")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeLeaveDTO)))
            .andExpect(status().isCreated());

        // Validate the EmployeeLeave in the database
        List<EmployeeLeave> employeeLeaveList = employeeLeaveRepository.findAll();
        assertThat(employeeLeaveList).hasSize(databaseSizeBeforeCreate + 1);
        EmployeeLeave testEmployeeLeave = employeeLeaveList.get(employeeLeaveList.size() - 1);
        assertThat(testEmployeeLeave.getFromDate()).isEqualTo(DEFAULT_FROM_DATE);
        assertThat(testEmployeeLeave.getToDate()).isEqualTo(DEFAULT_TO_DATE);
        assertThat(testEmployeeLeave.getNoOfDays()).isEqualTo(DEFAULT_NO_OF_DAYS);
        assertThat(testEmployeeLeave.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testEmployeeLeave.isStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createEmployeeLeaveWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = employeeLeaveRepository.findAll().size();

        // Create the EmployeeLeave with an existing ID
        employeeLeave.setId(1L);
        EmployeeLeaveDTO employeeLeaveDTO = employeeLeaveMapper.toDto(employeeLeave);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmployeeLeaveMockMvc.perform(post("/api/employee-leaves")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeLeaveDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EmployeeLeave in the database
        List<EmployeeLeave> employeeLeaveList = employeeLeaveRepository.findAll();
        assertThat(employeeLeaveList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkReasonIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeLeaveRepository.findAll().size();
        // set the field null
        employeeLeave.setReason(null);

        // Create the EmployeeLeave, which fails.
        EmployeeLeaveDTO employeeLeaveDTO = employeeLeaveMapper.toDto(employeeLeave);

        restEmployeeLeaveMockMvc.perform(post("/api/employee-leaves")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeLeaveDTO)))
            .andExpect(status().isBadRequest());

        List<EmployeeLeave> employeeLeaveList = employeeLeaveRepository.findAll();
        assertThat(employeeLeaveList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEmployeeLeaves() throws Exception {
        // Initialize the database
        employeeLeaveRepository.saveAndFlush(employeeLeave);

        // Get all the employeeLeaveList
        restEmployeeLeaveMockMvc.perform(get("/api/employee-leaves?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(employeeLeave.getId().intValue())))
            .andExpect(jsonPath("$.[*].fromDate").value(hasItem(sameInstant(DEFAULT_FROM_DATE))))
            .andExpect(jsonPath("$.[*].toDate").value(hasItem(sameInstant(DEFAULT_TO_DATE))))
            .andExpect(jsonPath("$.[*].noOfDays").value(hasItem(DEFAULT_NO_OF_DAYS.doubleValue())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getEmployeeLeave() throws Exception {
        // Initialize the database
        employeeLeaveRepository.saveAndFlush(employeeLeave);

        // Get the employeeLeave
        restEmployeeLeaveMockMvc.perform(get("/api/employee-leaves/{id}", employeeLeave.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(employeeLeave.getId().intValue()))
            .andExpect(jsonPath("$.fromDate").value(sameInstant(DEFAULT_FROM_DATE)))
            .andExpect(jsonPath("$.toDate").value(sameInstant(DEFAULT_TO_DATE)))
            .andExpect(jsonPath("$.noOfDays").value(DEFAULT_NO_OF_DAYS.doubleValue()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingEmployeeLeave() throws Exception {
        // Get the employeeLeave
        restEmployeeLeaveMockMvc.perform(get("/api/employee-leaves/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEmployeeLeave() throws Exception {
        // Initialize the database
        employeeLeaveRepository.saveAndFlush(employeeLeave);

        int databaseSizeBeforeUpdate = employeeLeaveRepository.findAll().size();

        // Update the employeeLeave
        EmployeeLeave updatedEmployeeLeave = employeeLeaveRepository.findById(employeeLeave.getId()).get();
        // Disconnect from session so that the updates on updatedEmployeeLeave are not directly saved in db
        em.detach(updatedEmployeeLeave);
        updatedEmployeeLeave
            .fromDate(UPDATED_FROM_DATE)
            .toDate(UPDATED_TO_DATE)
            .noOfDays(UPDATED_NO_OF_DAYS)
            .reason(UPDATED_REASON)
            .status(UPDATED_STATUS);
        EmployeeLeaveDTO employeeLeaveDTO = employeeLeaveMapper.toDto(updatedEmployeeLeave);

        restEmployeeLeaveMockMvc.perform(put("/api/employee-leaves")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeLeaveDTO)))
            .andExpect(status().isOk());

        // Validate the EmployeeLeave in the database
        List<EmployeeLeave> employeeLeaveList = employeeLeaveRepository.findAll();
        assertThat(employeeLeaveList).hasSize(databaseSizeBeforeUpdate);
        EmployeeLeave testEmployeeLeave = employeeLeaveList.get(employeeLeaveList.size() - 1);
        assertThat(testEmployeeLeave.getFromDate()).isEqualTo(UPDATED_FROM_DATE);
        assertThat(testEmployeeLeave.getToDate()).isEqualTo(UPDATED_TO_DATE);
        assertThat(testEmployeeLeave.getNoOfDays()).isEqualTo(UPDATED_NO_OF_DAYS);
        assertThat(testEmployeeLeave.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testEmployeeLeave.isStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingEmployeeLeave() throws Exception {
        int databaseSizeBeforeUpdate = employeeLeaveRepository.findAll().size();

        // Create the EmployeeLeave
        EmployeeLeaveDTO employeeLeaveDTO = employeeLeaveMapper.toDto(employeeLeave);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmployeeLeaveMockMvc.perform(put("/api/employee-leaves")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeLeaveDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EmployeeLeave in the database
        List<EmployeeLeave> employeeLeaveList = employeeLeaveRepository.findAll();
        assertThat(employeeLeaveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEmployeeLeave() throws Exception {
        // Initialize the database
        employeeLeaveRepository.saveAndFlush(employeeLeave);

        int databaseSizeBeforeDelete = employeeLeaveRepository.findAll().size();

        // Delete the employeeLeave
        restEmployeeLeaveMockMvc.perform(delete("/api/employee-leaves/{id}", employeeLeave.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EmployeeLeave> employeeLeaveList = employeeLeaveRepository.findAll();
        assertThat(employeeLeaveList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmployeeLeave.class);
        EmployeeLeave employeeLeave1 = new EmployeeLeave();
        employeeLeave1.setId(1L);
        EmployeeLeave employeeLeave2 = new EmployeeLeave();
        employeeLeave2.setId(employeeLeave1.getId());
        assertThat(employeeLeave1).isEqualTo(employeeLeave2);
        employeeLeave2.setId(2L);
        assertThat(employeeLeave1).isNotEqualTo(employeeLeave2);
        employeeLeave1.setId(null);
        assertThat(employeeLeave1).isNotEqualTo(employeeLeave2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmployeeLeaveDTO.class);
        EmployeeLeaveDTO employeeLeaveDTO1 = new EmployeeLeaveDTO();
        employeeLeaveDTO1.setId(1L);
        EmployeeLeaveDTO employeeLeaveDTO2 = new EmployeeLeaveDTO();
        assertThat(employeeLeaveDTO1).isNotEqualTo(employeeLeaveDTO2);
        employeeLeaveDTO2.setId(employeeLeaveDTO1.getId());
        assertThat(employeeLeaveDTO1).isEqualTo(employeeLeaveDTO2);
        employeeLeaveDTO2.setId(2L);
        assertThat(employeeLeaveDTO1).isNotEqualTo(employeeLeaveDTO2);
        employeeLeaveDTO1.setId(null);
        assertThat(employeeLeaveDTO1).isNotEqualTo(employeeLeaveDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(employeeLeaveMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(employeeLeaveMapper.fromId(null)).isNull();
    }
}
