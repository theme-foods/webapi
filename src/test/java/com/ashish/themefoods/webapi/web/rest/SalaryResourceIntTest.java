package com.ashish.themefoods.webapi.web.rest;

import com.ashish.themefoods.webapi.WebapiApp;

import com.ashish.themefoods.webapi.domain.Salary;
import com.ashish.themefoods.webapi.repository.SalaryRepository;
import com.ashish.themefoods.webapi.service.SalaryService;
import com.ashish.themefoods.webapi.service.dto.SalaryDTO;
import com.ashish.themefoods.webapi.service.mapper.SalaryMapper;
import com.ashish.themefoods.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.ashish.themefoods.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SalaryResource REST controller.
 *
 * @see SalaryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class SalaryResourceIntTest {

    private static final Double DEFAULT_TOTAL_SALARY = 1D;
    private static final Double UPDATED_TOTAL_SALARY = 2D;

    private static final Double DEFAULT_BASIC_DA = 1D;
    private static final Double UPDATED_BASIC_DA = 2D;

    private static final Double DEFAULT_HRA = 1D;
    private static final Double UPDATED_HRA = 2D;

    private static final Double DEFAULT_CONVEYANCE = 1D;
    private static final Double UPDATED_CONVEYANCE = 2D;

    private static final Double DEFAULT_OTHER_ALLOWANCES = 1D;
    private static final Double UPDATED_OTHER_ALLOWANCES = 2D;

    private static final Double DEFAULT_PF = 1D;
    private static final Double UPDATED_PF = 2D;

    private static final Double DEFAULT_ESI = 1D;
    private static final Double UPDATED_ESI = 2D;

    private static final Double DEFAULT_PT = 1D;
    private static final Double UPDATED_PT = 2D;

    @Autowired
    private SalaryRepository salaryRepository;

    @Autowired
    private SalaryMapper salaryMapper;

    @Autowired
    private SalaryService salaryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSalaryMockMvc;

    private Salary salary;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SalaryResource salaryResource = new SalaryResource(salaryService);
        this.restSalaryMockMvc = MockMvcBuilders.standaloneSetup(salaryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Salary createEntity(EntityManager em) {
        Salary salary = new Salary()
            .totalSalary(DEFAULT_TOTAL_SALARY)
            .basicDA(DEFAULT_BASIC_DA)
            .hra(DEFAULT_HRA)
            .conveyance(DEFAULT_CONVEYANCE)
            .otherAllowances(DEFAULT_OTHER_ALLOWANCES)
            .pf(DEFAULT_PF)
            .esi(DEFAULT_ESI)
            .pt(DEFAULT_PT);
        return salary;
    }

    @Before
    public void initTest() {
        salary = createEntity(em);
    }

    @Test
    @Transactional
    public void createSalary() throws Exception {
        int databaseSizeBeforeCreate = salaryRepository.findAll().size();

        // Create the Salary
        SalaryDTO salaryDTO = salaryMapper.toDto(salary);
        restSalaryMockMvc.perform(post("/api/salaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salaryDTO)))
            .andExpect(status().isCreated());

        // Validate the Salary in the database
        List<Salary> salaryList = salaryRepository.findAll();
        assertThat(salaryList).hasSize(databaseSizeBeforeCreate + 1);
        Salary testSalary = salaryList.get(salaryList.size() - 1);
        assertThat(testSalary.getTotalSalary()).isEqualTo(DEFAULT_TOTAL_SALARY);
        assertThat(testSalary.getBasicDA()).isEqualTo(DEFAULT_BASIC_DA);
        assertThat(testSalary.getHra()).isEqualTo(DEFAULT_HRA);
        assertThat(testSalary.getConveyance()).isEqualTo(DEFAULT_CONVEYANCE);
        assertThat(testSalary.getOtherAllowances()).isEqualTo(DEFAULT_OTHER_ALLOWANCES);
        assertThat(testSalary.getPf()).isEqualTo(DEFAULT_PF);
        assertThat(testSalary.getEsi()).isEqualTo(DEFAULT_ESI);
        assertThat(testSalary.getPt()).isEqualTo(DEFAULT_PT);
    }

    @Test
    @Transactional
    public void createSalaryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = salaryRepository.findAll().size();

        // Create the Salary with an existing ID
        salary.setId(1L);
        SalaryDTO salaryDTO = salaryMapper.toDto(salary);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSalaryMockMvc.perform(post("/api/salaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salaryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Salary in the database
        List<Salary> salaryList = salaryRepository.findAll();
        assertThat(salaryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSalaries() throws Exception {
        // Initialize the database
        salaryRepository.saveAndFlush(salary);

        // Get all the salaryList
        restSalaryMockMvc.perform(get("/api/salaries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(salary.getId().intValue())))
            .andExpect(jsonPath("$.[*].totalSalary").value(hasItem(DEFAULT_TOTAL_SALARY.doubleValue())))
            .andExpect(jsonPath("$.[*].basicDA").value(hasItem(DEFAULT_BASIC_DA.doubleValue())))
            .andExpect(jsonPath("$.[*].hra").value(hasItem(DEFAULT_HRA.doubleValue())))
            .andExpect(jsonPath("$.[*].conveyance").value(hasItem(DEFAULT_CONVEYANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].otherAllowances").value(hasItem(DEFAULT_OTHER_ALLOWANCES.doubleValue())))
            .andExpect(jsonPath("$.[*].pf").value(hasItem(DEFAULT_PF.doubleValue())))
            .andExpect(jsonPath("$.[*].esi").value(hasItem(DEFAULT_ESI.doubleValue())))
            .andExpect(jsonPath("$.[*].pt").value(hasItem(DEFAULT_PT.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getSalary() throws Exception {
        // Initialize the database
        salaryRepository.saveAndFlush(salary);

        // Get the salary
        restSalaryMockMvc.perform(get("/api/salaries/{id}", salary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(salary.getId().intValue()))
            .andExpect(jsonPath("$.totalSalary").value(DEFAULT_TOTAL_SALARY.doubleValue()))
            .andExpect(jsonPath("$.basicDA").value(DEFAULT_BASIC_DA.doubleValue()))
            .andExpect(jsonPath("$.hra").value(DEFAULT_HRA.doubleValue()))
            .andExpect(jsonPath("$.conveyance").value(DEFAULT_CONVEYANCE.doubleValue()))
            .andExpect(jsonPath("$.otherAllowances").value(DEFAULT_OTHER_ALLOWANCES.doubleValue()))
            .andExpect(jsonPath("$.pf").value(DEFAULT_PF.doubleValue()))
            .andExpect(jsonPath("$.esi").value(DEFAULT_ESI.doubleValue()))
            .andExpect(jsonPath("$.pt").value(DEFAULT_PT.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSalary() throws Exception {
        // Get the salary
        restSalaryMockMvc.perform(get("/api/salaries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSalary() throws Exception {
        // Initialize the database
        salaryRepository.saveAndFlush(salary);

        int databaseSizeBeforeUpdate = salaryRepository.findAll().size();

        // Update the salary
        Salary updatedSalary = salaryRepository.findById(salary.getId()).get();
        // Disconnect from session so that the updates on updatedSalary are not directly saved in db
        em.detach(updatedSalary);
        updatedSalary
            .totalSalary(UPDATED_TOTAL_SALARY)
            .basicDA(UPDATED_BASIC_DA)
            .hra(UPDATED_HRA)
            .conveyance(UPDATED_CONVEYANCE)
            .otherAllowances(UPDATED_OTHER_ALLOWANCES)
            .pf(UPDATED_PF)
            .esi(UPDATED_ESI)
            .pt(UPDATED_PT);
        SalaryDTO salaryDTO = salaryMapper.toDto(updatedSalary);

        restSalaryMockMvc.perform(put("/api/salaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salaryDTO)))
            .andExpect(status().isOk());

        // Validate the Salary in the database
        List<Salary> salaryList = salaryRepository.findAll();
        assertThat(salaryList).hasSize(databaseSizeBeforeUpdate);
        Salary testSalary = salaryList.get(salaryList.size() - 1);
        assertThat(testSalary.getTotalSalary()).isEqualTo(UPDATED_TOTAL_SALARY);
        assertThat(testSalary.getBasicDA()).isEqualTo(UPDATED_BASIC_DA);
        assertThat(testSalary.getHra()).isEqualTo(UPDATED_HRA);
        assertThat(testSalary.getConveyance()).isEqualTo(UPDATED_CONVEYANCE);
        assertThat(testSalary.getOtherAllowances()).isEqualTo(UPDATED_OTHER_ALLOWANCES);
        assertThat(testSalary.getPf()).isEqualTo(UPDATED_PF);
        assertThat(testSalary.getEsi()).isEqualTo(UPDATED_ESI);
        assertThat(testSalary.getPt()).isEqualTo(UPDATED_PT);
    }

    @Test
    @Transactional
    public void updateNonExistingSalary() throws Exception {
        int databaseSizeBeforeUpdate = salaryRepository.findAll().size();

        // Create the Salary
        SalaryDTO salaryDTO = salaryMapper.toDto(salary);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSalaryMockMvc.perform(put("/api/salaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salaryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Salary in the database
        List<Salary> salaryList = salaryRepository.findAll();
        assertThat(salaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSalary() throws Exception {
        // Initialize the database
        salaryRepository.saveAndFlush(salary);

        int databaseSizeBeforeDelete = salaryRepository.findAll().size();

        // Delete the salary
        restSalaryMockMvc.perform(delete("/api/salaries/{id}", salary.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Salary> salaryList = salaryRepository.findAll();
        assertThat(salaryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Salary.class);
        Salary salary1 = new Salary();
        salary1.setId(1L);
        Salary salary2 = new Salary();
        salary2.setId(salary1.getId());
        assertThat(salary1).isEqualTo(salary2);
        salary2.setId(2L);
        assertThat(salary1).isNotEqualTo(salary2);
        salary1.setId(null);
        assertThat(salary1).isNotEqualTo(salary2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SalaryDTO.class);
        SalaryDTO salaryDTO1 = new SalaryDTO();
        salaryDTO1.setId(1L);
        SalaryDTO salaryDTO2 = new SalaryDTO();
        assertThat(salaryDTO1).isNotEqualTo(salaryDTO2);
        salaryDTO2.setId(salaryDTO1.getId());
        assertThat(salaryDTO1).isEqualTo(salaryDTO2);
        salaryDTO2.setId(2L);
        assertThat(salaryDTO1).isNotEqualTo(salaryDTO2);
        salaryDTO1.setId(null);
        assertThat(salaryDTO1).isNotEqualTo(salaryDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(salaryMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(salaryMapper.fromId(null)).isNull();
    }
}
