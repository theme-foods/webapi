package com.ashish.themefoods.webapi.web.rest;

import com.ashish.themefoods.webapi.WebapiApp;

import com.ashish.themefoods.webapi.domain.SalarySlip;
import com.ashish.themefoods.webapi.repository.SalarySlipRepository;
import com.ashish.themefoods.webapi.service.SalarySlipService;
import com.ashish.themefoods.webapi.service.dto.SalarySlipDTO;
import com.ashish.themefoods.webapi.service.mapper.SalarySlipMapper;
import com.ashish.themefoods.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.ashish.themefoods.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SalarySlipResource REST controller.
 *
 * @see SalarySlipResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class SalarySlipResourceIntTest {

    private static final Double DEFAULT_TOTAL_SALARY = 1D;
    private static final Double UPDATED_TOTAL_SALARY = 2D;

    private static final Double DEFAULT_BASIC_DA = 1D;
    private static final Double UPDATED_BASIC_DA = 2D;

    private static final Double DEFAULT_HRA = 1D;
    private static final Double UPDATED_HRA = 2D;

    private static final Double DEFAULT_CONVEYANCE = 1D;
    private static final Double UPDATED_CONVEYANCE = 2D;

    private static final Double DEFAULT_OTHER_ALLOWANCES = 1D;
    private static final Double UPDATED_OTHER_ALLOWANCES = 2D;

    private static final Double DEFAULT_PF = 1D;
    private static final Double UPDATED_PF = 2D;

    private static final Double DEFAULT_ESI = 1D;
    private static final Double UPDATED_ESI = 2D;

    private static final Double DEFAULT_PT = 1D;
    private static final Double UPDATED_PT = 2D;

    private static final Integer DEFAULT_DAYS = 1;
    private static final Integer UPDATED_DAYS = 2;

    private static final Double DEFAULT_ACTUAL_PAID = 1D;
    private static final Double UPDATED_ACTUAL_PAID = 2D;

    private static final Double DEFAULT_PAYOUT = 1D;
    private static final Double UPDATED_PAYOUT = 2D;

    private static final Double DEFAULT_ADVANCE = 1D;
    private static final Double UPDATED_ADVANCE = 2D;

    @Autowired
    private SalarySlipRepository salarySlipRepository;

    @Autowired
    private SalarySlipMapper salarySlipMapper;

    @Autowired
    private SalarySlipService salarySlipService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSalarySlipMockMvc;

    private SalarySlip salarySlip;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SalarySlipResource salarySlipResource = new SalarySlipResource(salarySlipService, employeeService);
        this.restSalarySlipMockMvc = MockMvcBuilders.standaloneSetup(salarySlipResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SalarySlip createEntity(EntityManager em) {
        SalarySlip salarySlip = new SalarySlip()
            .totalSalary(DEFAULT_TOTAL_SALARY)
            .basicDA(DEFAULT_BASIC_DA)
            .hra(DEFAULT_HRA)
            .conveyance(DEFAULT_CONVEYANCE)
            .otherAllowances(DEFAULT_OTHER_ALLOWANCES)
            .pf(DEFAULT_PF)
            .esi(DEFAULT_ESI)
            .pt(DEFAULT_PT)
            .actualPaid(DEFAULT_ACTUAL_PAID)
            .payout(DEFAULT_PAYOUT)
            .advance(DEFAULT_ADVANCE);
        return salarySlip;
    }

    @Before
    public void initTest() {
        salarySlip = createEntity(em);
    }

    @Test
    @Transactional
    public void createSalarySlip() throws Exception {
        int databaseSizeBeforeCreate = salarySlipRepository.findAll().size();

        // Create the SalarySlip
        SalarySlipDTO salarySlipDTO = salarySlipMapper.toDto(salarySlip);
        restSalarySlipMockMvc.perform(post("/api/salary-slips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salarySlipDTO)))
            .andExpect(status().isCreated());

        // Validate the SalarySlip in the database
        List<SalarySlip> salarySlipList = salarySlipRepository.findAll();
        assertThat(salarySlipList).hasSize(databaseSizeBeforeCreate + 1);
        SalarySlip testSalarySlip = salarySlipList.get(salarySlipList.size() - 1);
        assertThat(testSalarySlip.getTotalSalary()).isEqualTo(DEFAULT_TOTAL_SALARY);
        assertThat(testSalarySlip.getBasicDA()).isEqualTo(DEFAULT_BASIC_DA);
        assertThat(testSalarySlip.getHra()).isEqualTo(DEFAULT_HRA);
        assertThat(testSalarySlip.getConveyance()).isEqualTo(DEFAULT_CONVEYANCE);
        assertThat(testSalarySlip.getOtherAllowances()).isEqualTo(DEFAULT_OTHER_ALLOWANCES);
        assertThat(testSalarySlip.getPf()).isEqualTo(DEFAULT_PF);
        assertThat(testSalarySlip.getEsi()).isEqualTo(DEFAULT_ESI);
        assertThat(testSalarySlip.getPt()).isEqualTo(DEFAULT_PT);
        assertThat(testSalarySlip.getDays()).isEqualTo(DEFAULT_DAYS);
        assertThat(testSalarySlip.getActualPaid()).isEqualTo(DEFAULT_ACTUAL_PAID);
        assertThat(testSalarySlip.getPayout()).isEqualTo(DEFAULT_PAYOUT);
        assertThat(testSalarySlip.getAdvance()).isEqualTo(DEFAULT_ADVANCE);
    }

    @Test
    @Transactional
    public void createSalarySlipWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = salarySlipRepository.findAll().size();

        // Create the SalarySlip with an existing ID
        salarySlip.setId(1L);
        SalarySlipDTO salarySlipDTO = salarySlipMapper.toDto(salarySlip);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSalarySlipMockMvc.perform(post("/api/salary-slips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salarySlipDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SalarySlip in the database
        List<SalarySlip> salarySlipList = salarySlipRepository.findAll();
        assertThat(salarySlipList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSalarySlips() throws Exception {
        // Initialize the database
        salarySlipRepository.saveAndFlush(salarySlip);

        // Get all the salarySlipList
        restSalarySlipMockMvc.perform(get("/api/salary-slips?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(salarySlip.getId().intValue())))
            .andExpect(jsonPath("$.[*].totalSalary").value(hasItem(DEFAULT_TOTAL_SALARY.doubleValue())))
            .andExpect(jsonPath("$.[*].basicDA").value(hasItem(DEFAULT_BASIC_DA.doubleValue())))
            .andExpect(jsonPath("$.[*].hra").value(hasItem(DEFAULT_HRA.doubleValue())))
            .andExpect(jsonPath("$.[*].conveyance").value(hasItem(DEFAULT_CONVEYANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].otherAllowances").value(hasItem(DEFAULT_OTHER_ALLOWANCES.doubleValue())))
            .andExpect(jsonPath("$.[*].pf").value(hasItem(DEFAULT_PF.doubleValue())))
            .andExpect(jsonPath("$.[*].esi").value(hasItem(DEFAULT_ESI.doubleValue())))
            .andExpect(jsonPath("$.[*].pt").value(hasItem(DEFAULT_PT.doubleValue())))
            .andExpect(jsonPath("$.[*].days").value(hasItem(DEFAULT_DAYS)))
            .andExpect(jsonPath("$.[*].actualPaid").value(hasItem(DEFAULT_ACTUAL_PAID.doubleValue())))
            .andExpect(jsonPath("$.[*].payout").value(hasItem(DEFAULT_PAYOUT.doubleValue())))
            .andExpect(jsonPath("$.[*].advance").value(hasItem(DEFAULT_ADVANCE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getSalarySlip() throws Exception {
        // Initialize the database
        salarySlipRepository.saveAndFlush(salarySlip);

        // Get the salarySlip
        restSalarySlipMockMvc.perform(get("/api/salary-slips/{id}", salarySlip.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(salarySlip.getId().intValue()))
            .andExpect(jsonPath("$.totalSalary").value(DEFAULT_TOTAL_SALARY.doubleValue()))
            .andExpect(jsonPath("$.basicDA").value(DEFAULT_BASIC_DA.doubleValue()))
            .andExpect(jsonPath("$.hra").value(DEFAULT_HRA.doubleValue()))
            .andExpect(jsonPath("$.conveyance").value(DEFAULT_CONVEYANCE.doubleValue()))
            .andExpect(jsonPath("$.otherAllowances").value(DEFAULT_OTHER_ALLOWANCES.doubleValue()))
            .andExpect(jsonPath("$.pf").value(DEFAULT_PF.doubleValue()))
            .andExpect(jsonPath("$.esi").value(DEFAULT_ESI.doubleValue()))
            .andExpect(jsonPath("$.pt").value(DEFAULT_PT.doubleValue()))
            .andExpect(jsonPath("$.days").value(DEFAULT_DAYS))
            .andExpect(jsonPath("$.actualPaid").value(DEFAULT_ACTUAL_PAID.doubleValue()))
            .andExpect(jsonPath("$.payout").value(DEFAULT_PAYOUT.doubleValue()))
            .andExpect(jsonPath("$.advance").value(DEFAULT_ADVANCE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSalarySlip() throws Exception {
        // Get the salarySlip
        restSalarySlipMockMvc.perform(get("/api/salary-slips/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSalarySlip() throws Exception {
        // Initialize the database
        salarySlipRepository.saveAndFlush(salarySlip);

        int databaseSizeBeforeUpdate = salarySlipRepository.findAll().size();

        // Update the salarySlip
        SalarySlip updatedSalarySlip = salarySlipRepository.findById(salarySlip.getId()).get();
        // Disconnect from session so that the updates on updatedSalarySlip are not directly saved in db
        em.detach(updatedSalarySlip);
        updatedSalarySlip
            .totalSalary(UPDATED_TOTAL_SALARY)
            .basicDA(UPDATED_BASIC_DA)
            .hra(UPDATED_HRA)
            .conveyance(UPDATED_CONVEYANCE)
            .otherAllowances(UPDATED_OTHER_ALLOWANCES)
            .pf(UPDATED_PF)
            .esi(UPDATED_ESI)
            .pt(UPDATED_PT)
            .actualPaid(UPDATED_ACTUAL_PAID)
            .payout(UPDATED_PAYOUT)
            .advance(UPDATED_ADVANCE);
        SalarySlipDTO salarySlipDTO = salarySlipMapper.toDto(updatedSalarySlip);

        restSalarySlipMockMvc.perform(put("/api/salary-slips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salarySlipDTO)))
            .andExpect(status().isOk());

        // Validate the SalarySlip in the database
        List<SalarySlip> salarySlipList = salarySlipRepository.findAll();
        assertThat(salarySlipList).hasSize(databaseSizeBeforeUpdate);
        SalarySlip testSalarySlip = salarySlipList.get(salarySlipList.size() - 1);
        assertThat(testSalarySlip.getTotalSalary()).isEqualTo(UPDATED_TOTAL_SALARY);
        assertThat(testSalarySlip.getBasicDA()).isEqualTo(UPDATED_BASIC_DA);
        assertThat(testSalarySlip.getHra()).isEqualTo(UPDATED_HRA);
        assertThat(testSalarySlip.getConveyance()).isEqualTo(UPDATED_CONVEYANCE);
        assertThat(testSalarySlip.getOtherAllowances()).isEqualTo(UPDATED_OTHER_ALLOWANCES);
        assertThat(testSalarySlip.getPf()).isEqualTo(UPDATED_PF);
        assertThat(testSalarySlip.getEsi()).isEqualTo(UPDATED_ESI);
        assertThat(testSalarySlip.getPt()).isEqualTo(UPDATED_PT);
        assertThat(testSalarySlip.getDays()).isEqualTo(UPDATED_DAYS);
        assertThat(testSalarySlip.getActualPaid()).isEqualTo(UPDATED_ACTUAL_PAID);
        assertThat(testSalarySlip.getPayout()).isEqualTo(UPDATED_PAYOUT);
        assertThat(testSalarySlip.getAdvance()).isEqualTo(UPDATED_ADVANCE);
    }

    @Test
    @Transactional
    public void updateNonExistingSalarySlip() throws Exception {
        int databaseSizeBeforeUpdate = salarySlipRepository.findAll().size();

        // Create the SalarySlip
        SalarySlipDTO salarySlipDTO = salarySlipMapper.toDto(salarySlip);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSalarySlipMockMvc.perform(put("/api/salary-slips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salarySlipDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SalarySlip in the database
        List<SalarySlip> salarySlipList = salarySlipRepository.findAll();
        assertThat(salarySlipList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSalarySlip() throws Exception {
        // Initialize the database
        salarySlipRepository.saveAndFlush(salarySlip);

        int databaseSizeBeforeDelete = salarySlipRepository.findAll().size();

        // Delete the salarySlip
        restSalarySlipMockMvc.perform(delete("/api/salary-slips/{id}", salarySlip.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SalarySlip> salarySlipList = salarySlipRepository.findAll();
        assertThat(salarySlipList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SalarySlip.class);
        SalarySlip salarySlip1 = new SalarySlip();
        salarySlip1.setId(1L);
        SalarySlip salarySlip2 = new SalarySlip();
        salarySlip2.setId(salarySlip1.getId());
        assertThat(salarySlip1).isEqualTo(salarySlip2);
        salarySlip2.setId(2L);
        assertThat(salarySlip1).isNotEqualTo(salarySlip2);
        salarySlip1.setId(null);
        assertThat(salarySlip1).isNotEqualTo(salarySlip2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SalarySlipDTO.class);
        SalarySlipDTO salarySlipDTO1 = new SalarySlipDTO();
        salarySlipDTO1.setId(1L);
        SalarySlipDTO salarySlipDTO2 = new SalarySlipDTO();
        assertThat(salarySlipDTO1).isNotEqualTo(salarySlipDTO2);
        salarySlipDTO2.setId(salarySlipDTO1.getId());
        assertThat(salarySlipDTO1).isEqualTo(salarySlipDTO2);
        salarySlipDTO2.setId(2L);
        assertThat(salarySlipDTO1).isNotEqualTo(salarySlipDTO2);
        salarySlipDTO1.setId(null);
        assertThat(salarySlipDTO1).isNotEqualTo(salarySlipDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(salarySlipMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(salarySlipMapper.fromId(null)).isNull();
    }
}
