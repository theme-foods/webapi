package com.ashish.themefoods.webapi.web.rest;

import com.ashish.themefoods.webapi.WebapiApp;

import com.ashish.themefoods.webapi.domain.AdvanceTransactionHistory;
import com.ashish.themefoods.webapi.repository.AdvanceTransactionHistoryRepository;
import com.ashish.themefoods.webapi.service.AdvanceTransactionHistoryService;
import com.ashish.themefoods.webapi.service.dto.AdvanceTransactionHistoryDTO;
import com.ashish.themefoods.webapi.service.mapper.AdvanceTransactionHistoryMapper;
import com.ashish.themefoods.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.ashish.themefoods.webapi.web.rest.TestUtil.sameInstant;
import static com.ashish.themefoods.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdvanceTransactionHistoryResource REST controller.
 *
 * @see AdvanceTransactionHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class AdvanceTransactionHistoryResourceIntTest {

    private static final ZonedDateTime DEFAULT_TAKEN_ON = ZonedDateTime.ofLocalInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TAKEN_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;

    private static final String DEFAULT_TYPE_OF_ADVANCE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_OF_ADVANCE = "BBBBBBBBBB";

    private static final Integer DEFAULT_TENURE = 1;
    private static final Integer UPDATED_TENURE = 2;

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    @Autowired
    private AdvanceTransactionHistoryRepository advanceTransactionHistoryRepository;

    @Autowired
    private AdvanceTransactionHistoryMapper advanceTransactionHistoryMapper;

    @Autowired
    private AdvanceTransactionHistoryService advanceTransactionHistoryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAdvanceTransactionHistoryMockMvc;

    private AdvanceTransactionHistory advanceTransactionHistory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdvanceTransactionHistoryResource advanceTransactionHistoryResource = new AdvanceTransactionHistoryResource(advanceTransactionHistoryService);
        this.restAdvanceTransactionHistoryMockMvc = MockMvcBuilders.standaloneSetup(advanceTransactionHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdvanceTransactionHistory createEntity(EntityManager em) {
        AdvanceTransactionHistory advanceTransactionHistory = new AdvanceTransactionHistory()
            .takenOn(DEFAULT_TAKEN_ON)
            .amount(DEFAULT_AMOUNT)
            .typeOfAdvance(DEFAULT_TYPE_OF_ADVANCE)
            .tenure(DEFAULT_TENURE)
            .reason(DEFAULT_REASON);
        return advanceTransactionHistory;
    }

    @Before
    public void initTest() {
        advanceTransactionHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdvanceTransactionHistory() throws Exception {
        int databaseSizeBeforeCreate = advanceTransactionHistoryRepository.findAll().size();

        // Create the AdvanceTransactionHistory
        AdvanceTransactionHistoryDTO advanceTransactionHistoryDTO = advanceTransactionHistoryMapper.toDto(advanceTransactionHistory);
        restAdvanceTransactionHistoryMockMvc.perform(post("/api/advance-transaction-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advanceTransactionHistoryDTO)))
            .andExpect(status().isCreated());

        // Validate the AdvanceTransactionHistory in the database
        List<AdvanceTransactionHistory> advanceTransactionHistoryList = advanceTransactionHistoryRepository.findAll();
        assertThat(advanceTransactionHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        AdvanceTransactionHistory testAdvanceTransactionHistory = advanceTransactionHistoryList.get(advanceTransactionHistoryList.size() - 1);
        assertThat(testAdvanceTransactionHistory.getTakenOn()).isEqualTo(DEFAULT_TAKEN_ON);
        assertThat(testAdvanceTransactionHistory.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testAdvanceTransactionHistory.getTypeOfAdvance()).isEqualTo(DEFAULT_TYPE_OF_ADVANCE);
        assertThat(testAdvanceTransactionHistory.getTenure()).isEqualTo(DEFAULT_TENURE);
        assertThat(testAdvanceTransactionHistory.getReason()).isEqualTo(DEFAULT_REASON);
    }

    @Test
    @Transactional
    public void createAdvanceTransactionHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = advanceTransactionHistoryRepository.findAll().size();

        // Create the AdvanceTransactionHistory with an existing ID
        advanceTransactionHistory.setId(1L);
        AdvanceTransactionHistoryDTO advanceTransactionHistoryDTO = advanceTransactionHistoryMapper.toDto(advanceTransactionHistory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdvanceTransactionHistoryMockMvc.perform(post("/api/advance-transaction-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advanceTransactionHistoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdvanceTransactionHistory in the database
        List<AdvanceTransactionHistory> advanceTransactionHistoryList = advanceTransactionHistoryRepository.findAll();
        assertThat(advanceTransactionHistoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAdvanceTransactionHistories() throws Exception {
        // Initialize the database
        advanceTransactionHistoryRepository.saveAndFlush(advanceTransactionHistory);

        // Get all the advanceTransactionHistoryList
        restAdvanceTransactionHistoryMockMvc.perform(get("/api/advance-transaction-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(advanceTransactionHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].takenOn").value(hasItem(sameInstant(DEFAULT_TAKEN_ON))))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].typeOfAdvance").value(hasItem(DEFAULT_TYPE_OF_ADVANCE.toString())))
            .andExpect(jsonPath("$.[*].tenure").value(hasItem(DEFAULT_TENURE)))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())));
    }
    
    @Test
    @Transactional
    public void getAdvanceTransactionHistory() throws Exception {
        // Initialize the database
        advanceTransactionHistoryRepository.saveAndFlush(advanceTransactionHistory);

        // Get the advanceTransactionHistory
        restAdvanceTransactionHistoryMockMvc.perform(get("/api/advance-transaction-histories/{id}", advanceTransactionHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(advanceTransactionHistory.getId().intValue()))
            .andExpect(jsonPath("$.takenOn").value(sameInstant(DEFAULT_TAKEN_ON)))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.typeOfAdvance").value(DEFAULT_TYPE_OF_ADVANCE.toString()))
            .andExpect(jsonPath("$.tenure").value(DEFAULT_TENURE))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAdvanceTransactionHistory() throws Exception {
        // Get the advanceTransactionHistory
        restAdvanceTransactionHistoryMockMvc.perform(get("/api/advance-transaction-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdvanceTransactionHistory() throws Exception {
        // Initialize the database
        advanceTransactionHistoryRepository.saveAndFlush(advanceTransactionHistory);

        int databaseSizeBeforeUpdate = advanceTransactionHistoryRepository.findAll().size();

        // Update the advanceTransactionHistory
        AdvanceTransactionHistory updatedAdvanceTransactionHistory = advanceTransactionHistoryRepository.findById(advanceTransactionHistory.getId()).get();
        // Disconnect from session so that the updates on updatedAdvanceTransactionHistory are not directly saved in db
        em.detach(updatedAdvanceTransactionHistory);
        updatedAdvanceTransactionHistory
            .takenOn(UPDATED_TAKEN_ON)
            .amount(UPDATED_AMOUNT)
            .typeOfAdvance(UPDATED_TYPE_OF_ADVANCE)
            .tenure(UPDATED_TENURE)
            .reason(UPDATED_REASON);
        AdvanceTransactionHistoryDTO advanceTransactionHistoryDTO = advanceTransactionHistoryMapper.toDto(updatedAdvanceTransactionHistory);

        restAdvanceTransactionHistoryMockMvc.perform(put("/api/advance-transaction-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advanceTransactionHistoryDTO)))
            .andExpect(status().isOk());

        // Validate the AdvanceTransactionHistory in the database
        List<AdvanceTransactionHistory> advanceTransactionHistoryList = advanceTransactionHistoryRepository.findAll();
        assertThat(advanceTransactionHistoryList).hasSize(databaseSizeBeforeUpdate);
        AdvanceTransactionHistory testAdvanceTransactionHistory = advanceTransactionHistoryList.get(advanceTransactionHistoryList.size() - 1);
        assertThat(testAdvanceTransactionHistory.getTakenOn()).isEqualTo(UPDATED_TAKEN_ON);
        assertThat(testAdvanceTransactionHistory.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testAdvanceTransactionHistory.getTypeOfAdvance()).isEqualTo(UPDATED_TYPE_OF_ADVANCE);
        assertThat(testAdvanceTransactionHistory.getTenure()).isEqualTo(UPDATED_TENURE);
        assertThat(testAdvanceTransactionHistory.getReason()).isEqualTo(UPDATED_REASON);
    }

    @Test
    @Transactional
    public void updateNonExistingAdvanceTransactionHistory() throws Exception {
        int databaseSizeBeforeUpdate = advanceTransactionHistoryRepository.findAll().size();

        // Create the AdvanceTransactionHistory
        AdvanceTransactionHistoryDTO advanceTransactionHistoryDTO = advanceTransactionHistoryMapper.toDto(advanceTransactionHistory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdvanceTransactionHistoryMockMvc.perform(put("/api/advance-transaction-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advanceTransactionHistoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdvanceTransactionHistory in the database
        List<AdvanceTransactionHistory> advanceTransactionHistoryList = advanceTransactionHistoryRepository.findAll();
        assertThat(advanceTransactionHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAdvanceTransactionHistory() throws Exception {
        // Initialize the database
        advanceTransactionHistoryRepository.saveAndFlush(advanceTransactionHistory);

        int databaseSizeBeforeDelete = advanceTransactionHistoryRepository.findAll().size();

        // Delete the advanceTransactionHistory
        restAdvanceTransactionHistoryMockMvc.perform(delete("/api/advance-transaction-histories/{id}", advanceTransactionHistory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AdvanceTransactionHistory> advanceTransactionHistoryList = advanceTransactionHistoryRepository.findAll();
        assertThat(advanceTransactionHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdvanceTransactionHistory.class);
        AdvanceTransactionHistory advanceTransactionHistory1 = new AdvanceTransactionHistory();
        advanceTransactionHistory1.setId(1L);
        AdvanceTransactionHistory advanceTransactionHistory2 = new AdvanceTransactionHistory();
        advanceTransactionHistory2.setId(advanceTransactionHistory1.getId());
        assertThat(advanceTransactionHistory1).isEqualTo(advanceTransactionHistory2);
        advanceTransactionHistory2.setId(2L);
        assertThat(advanceTransactionHistory1).isNotEqualTo(advanceTransactionHistory2);
        advanceTransactionHistory1.setId(null);
        assertThat(advanceTransactionHistory1).isNotEqualTo(advanceTransactionHistory2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdvanceTransactionHistoryDTO.class);
        AdvanceTransactionHistoryDTO advanceTransactionHistoryDTO1 = new AdvanceTransactionHistoryDTO();
        advanceTransactionHistoryDTO1.setId(1L);
        AdvanceTransactionHistoryDTO advanceTransactionHistoryDTO2 = new AdvanceTransactionHistoryDTO();
        assertThat(advanceTransactionHistoryDTO1).isNotEqualTo(advanceTransactionHistoryDTO2);
        advanceTransactionHistoryDTO2.setId(advanceTransactionHistoryDTO1.getId());
        assertThat(advanceTransactionHistoryDTO1).isEqualTo(advanceTransactionHistoryDTO2);
        advanceTransactionHistoryDTO2.setId(2L);
        assertThat(advanceTransactionHistoryDTO1).isNotEqualTo(advanceTransactionHistoryDTO2);
        advanceTransactionHistoryDTO1.setId(null);
        assertThat(advanceTransactionHistoryDTO1).isNotEqualTo(advanceTransactionHistoryDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(advanceTransactionHistoryMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(advanceTransactionHistoryMapper.fromId(null)).isNull();
    }
}
