package com.ashish.themefoods.webapi.web.rest;

import com.ashish.themefoods.webapi.WebapiApp;

import com.ashish.themefoods.webapi.domain.LeaveDashboard;
import com.ashish.themefoods.webapi.repository.LeaveDashboardRepository;
import com.ashish.themefoods.webapi.service.LeaveDashboardService;
import com.ashish.themefoods.webapi.service.dto.LeaveDashboardDTO;
import com.ashish.themefoods.webapi.service.mapper.LeaveDashboardMapper;
import com.ashish.themefoods.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.ashish.themefoods.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LeaveDashboardResource REST controller.
 *
 * @see LeaveDashboardResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class LeaveDashboardResourceIntTest {

    private static final Double DEFAULT_AVAILABLE_LEAVES = 1D;
    private static final Double UPDATED_AVAILABLE_LEAVES = 2D;

    private static final Double DEFAULT_TAKEN_LEAVES = 1D;
    private static final Double UPDATED_TAKEN_LEAVES = 2D;

    @Autowired
    private LeaveDashboardRepository leaveDashboardRepository;

    @Autowired
    private LeaveDashboardMapper leaveDashboardMapper;

    @Autowired
    private LeaveDashboardService leaveDashboardService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restLeaveDashboardMockMvc;

    private LeaveDashboard leaveDashboard;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LeaveDashboardResource leaveDashboardResource = new LeaveDashboardResource(leaveDashboardService);
        this.restLeaveDashboardMockMvc = MockMvcBuilders.standaloneSetup(leaveDashboardResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LeaveDashboard createEntity(EntityManager em) {
        LeaveDashboard leaveDashboard = new LeaveDashboard()
            .availableLeaves(DEFAULT_AVAILABLE_LEAVES)
            .takenLeaves(DEFAULT_TAKEN_LEAVES);
        return leaveDashboard;
    }

    @Before
    public void initTest() {
        leaveDashboard = createEntity(em);
    }

    @Test
    @Transactional
    public void createLeaveDashboard() throws Exception {
        int databaseSizeBeforeCreate = leaveDashboardRepository.findAll().size();

        // Create the LeaveDashboard
        LeaveDashboardDTO leaveDashboardDTO = leaveDashboardMapper.toDto(leaveDashboard);
        restLeaveDashboardMockMvc.perform(post("/api/leave-dashboards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(leaveDashboardDTO)))
            .andExpect(status().isCreated());

        // Validate the LeaveDashboard in the database
        List<LeaveDashboard> leaveDashboardList = leaveDashboardRepository.findAll();
        assertThat(leaveDashboardList).hasSize(databaseSizeBeforeCreate + 1);
        LeaveDashboard testLeaveDashboard = leaveDashboardList.get(leaveDashboardList.size() - 1);
        assertThat(testLeaveDashboard.getAvailableLeaves()).isEqualTo(DEFAULT_AVAILABLE_LEAVES);
        assertThat(testLeaveDashboard.getTakenLeaves()).isEqualTo(DEFAULT_TAKEN_LEAVES);
    }

    @Test
    @Transactional
    public void createLeaveDashboardWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = leaveDashboardRepository.findAll().size();

        // Create the LeaveDashboard with an existing ID
        leaveDashboard.setId(1L);
        LeaveDashboardDTO leaveDashboardDTO = leaveDashboardMapper.toDto(leaveDashboard);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLeaveDashboardMockMvc.perform(post("/api/leave-dashboards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(leaveDashboardDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LeaveDashboard in the database
        List<LeaveDashboard> leaveDashboardList = leaveDashboardRepository.findAll();
        assertThat(leaveDashboardList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLeaveDashboards() throws Exception {
        // Initialize the database
        leaveDashboardRepository.saveAndFlush(leaveDashboard);

        // Get all the leaveDashboardList
        restLeaveDashboardMockMvc.perform(get("/api/leave-dashboards?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(leaveDashboard.getId().intValue())))
            .andExpect(jsonPath("$.[*].availableLeaves").value(hasItem(DEFAULT_AVAILABLE_LEAVES.doubleValue())))
            .andExpect(jsonPath("$.[*].takenLeaves").value(hasItem(DEFAULT_TAKEN_LEAVES.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getLeaveDashboard() throws Exception {
        // Initialize the database
        leaveDashboardRepository.saveAndFlush(leaveDashboard);

        // Get the leaveDashboard
        restLeaveDashboardMockMvc.perform(get("/api/leave-dashboards/{id}", leaveDashboard.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(leaveDashboard.getId().intValue()))
            .andExpect(jsonPath("$.availableLeaves").value(DEFAULT_AVAILABLE_LEAVES.doubleValue()))
            .andExpect(jsonPath("$.takenLeaves").value(DEFAULT_TAKEN_LEAVES.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingLeaveDashboard() throws Exception {
        // Get the leaveDashboard
        restLeaveDashboardMockMvc.perform(get("/api/leave-dashboards/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLeaveDashboard() throws Exception {
        // Initialize the database
        leaveDashboardRepository.saveAndFlush(leaveDashboard);

        int databaseSizeBeforeUpdate = leaveDashboardRepository.findAll().size();

        // Update the leaveDashboard
        LeaveDashboard updatedLeaveDashboard = leaveDashboardRepository.findById(leaveDashboard.getId()).get();
        // Disconnect from session so that the updates on updatedLeaveDashboard are not directly saved in db
        em.detach(updatedLeaveDashboard);
        updatedLeaveDashboard
            .availableLeaves(UPDATED_AVAILABLE_LEAVES)
            .takenLeaves(UPDATED_TAKEN_LEAVES);
        LeaveDashboardDTO leaveDashboardDTO = leaveDashboardMapper.toDto(updatedLeaveDashboard);

        restLeaveDashboardMockMvc.perform(put("/api/leave-dashboards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(leaveDashboardDTO)))
            .andExpect(status().isOk());

        // Validate the LeaveDashboard in the database
        List<LeaveDashboard> leaveDashboardList = leaveDashboardRepository.findAll();
        assertThat(leaveDashboardList).hasSize(databaseSizeBeforeUpdate);
        LeaveDashboard testLeaveDashboard = leaveDashboardList.get(leaveDashboardList.size() - 1);
        assertThat(testLeaveDashboard.getAvailableLeaves()).isEqualTo(UPDATED_AVAILABLE_LEAVES);
        assertThat(testLeaveDashboard.getTakenLeaves()).isEqualTo(UPDATED_TAKEN_LEAVES);
    }

    @Test
    @Transactional
    public void updateNonExistingLeaveDashboard() throws Exception {
        int databaseSizeBeforeUpdate = leaveDashboardRepository.findAll().size();

        // Create the LeaveDashboard
        LeaveDashboardDTO leaveDashboardDTO = leaveDashboardMapper.toDto(leaveDashboard);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLeaveDashboardMockMvc.perform(put("/api/leave-dashboards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(leaveDashboardDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LeaveDashboard in the database
        List<LeaveDashboard> leaveDashboardList = leaveDashboardRepository.findAll();
        assertThat(leaveDashboardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLeaveDashboard() throws Exception {
        // Initialize the database
        leaveDashboardRepository.saveAndFlush(leaveDashboard);

        int databaseSizeBeforeDelete = leaveDashboardRepository.findAll().size();

        // Delete the leaveDashboard
        restLeaveDashboardMockMvc.perform(delete("/api/leave-dashboards/{id}", leaveDashboard.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LeaveDashboard> leaveDashboardList = leaveDashboardRepository.findAll();
        assertThat(leaveDashboardList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LeaveDashboard.class);
        LeaveDashboard leaveDashboard1 = new LeaveDashboard();
        leaveDashboard1.setId(1L);
        LeaveDashboard leaveDashboard2 = new LeaveDashboard();
        leaveDashboard2.setId(leaveDashboard1.getId());
        assertThat(leaveDashboard1).isEqualTo(leaveDashboard2);
        leaveDashboard2.setId(2L);
        assertThat(leaveDashboard1).isNotEqualTo(leaveDashboard2);
        leaveDashboard1.setId(null);
        assertThat(leaveDashboard1).isNotEqualTo(leaveDashboard2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LeaveDashboardDTO.class);
        LeaveDashboardDTO leaveDashboardDTO1 = new LeaveDashboardDTO();
        leaveDashboardDTO1.setId(1L);
        LeaveDashboardDTO leaveDashboardDTO2 = new LeaveDashboardDTO();
        assertThat(leaveDashboardDTO1).isNotEqualTo(leaveDashboardDTO2);
        leaveDashboardDTO2.setId(leaveDashboardDTO1.getId());
        assertThat(leaveDashboardDTO1).isEqualTo(leaveDashboardDTO2);
        leaveDashboardDTO2.setId(2L);
        assertThat(leaveDashboardDTO1).isNotEqualTo(leaveDashboardDTO2);
        leaveDashboardDTO1.setId(null);
        assertThat(leaveDashboardDTO1).isNotEqualTo(leaveDashboardDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(leaveDashboardMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(leaveDashboardMapper.fromId(null)).isNull();
    }
}
