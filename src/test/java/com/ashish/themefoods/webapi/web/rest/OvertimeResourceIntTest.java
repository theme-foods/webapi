package com.ashish.themefoods.webapi.web.rest;

import com.ashish.themefoods.webapi.WebapiApp;

import com.ashish.themefoods.webapi.domain.Overtime;
import com.ashish.themefoods.webapi.repository.OvertimeRepository;
import com.ashish.themefoods.webapi.service.OvertimeService;
import com.ashish.themefoods.webapi.service.dto.OvertimeDTO;
import com.ashish.themefoods.webapi.service.mapper.OvertimeMapper;
import com.ashish.themefoods.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.ashish.themefoods.webapi.web.rest.TestUtil.sameInstant;
import static com.ashish.themefoods.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OvertimeResource REST controller.
 *
 * @see OvertimeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class OvertimeResourceIntTest {

    private static final ZonedDateTime DEFAULT_DATE = ZonedDateTime.ofLocalInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Float DEFAULT_HOURS = 1F;
    private static final Float UPDATED_HOURS = 2F;

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    @Autowired
    private OvertimeRepository overtimeRepository;

    @Autowired
    private OvertimeMapper overtimeMapper;

    @Autowired
    private OvertimeService overtimeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOvertimeMockMvc;

    private Overtime overtime;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OvertimeResource overtimeResource = new OvertimeResource(overtimeService);
        this.restOvertimeMockMvc = MockMvcBuilders.standaloneSetup(overtimeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Overtime createEntity(EntityManager em) {
        Overtime overtime = new Overtime()
            .date(DEFAULT_DATE)
            .hours(DEFAULT_HOURS)
            .reason(DEFAULT_REASON)
            .status(DEFAULT_STATUS);
        return overtime;
    }

    @Before
    public void initTest() {
        overtime = createEntity(em);
    }

    @Test
    @Transactional
    public void createOvertime() throws Exception {
        int databaseSizeBeforeCreate = overtimeRepository.findAll().size();

        // Create the Overtime
        OvertimeDTO overtimeDTO = overtimeMapper.toDto(overtime);
        restOvertimeMockMvc.perform(post("/api/overtimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(overtimeDTO)))
            .andExpect(status().isCreated());

        // Validate the Overtime in the database
        List<Overtime> overtimeList = overtimeRepository.findAll();
        assertThat(overtimeList).hasSize(databaseSizeBeforeCreate + 1);
        Overtime testOvertime = overtimeList.get(overtimeList.size() - 1);
        assertThat(testOvertime.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testOvertime.getHours()).isEqualTo(DEFAULT_HOURS);
        assertThat(testOvertime.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testOvertime.isStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createOvertimeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = overtimeRepository.findAll().size();

        // Create the Overtime with an existing ID
        overtime.setId(1L);
        OvertimeDTO overtimeDTO = overtimeMapper.toDto(overtime);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOvertimeMockMvc.perform(post("/api/overtimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(overtimeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Overtime in the database
        List<Overtime> overtimeList = overtimeRepository.findAll();
        assertThat(overtimeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = overtimeRepository.findAll().size();
        // set the field null
        overtime.setDate(null);

        // Create the Overtime, which fails.
        OvertimeDTO overtimeDTO = overtimeMapper.toDto(overtime);

        restOvertimeMockMvc.perform(post("/api/overtimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(overtimeDTO)))
            .andExpect(status().isBadRequest());

        List<Overtime> overtimeList = overtimeRepository.findAll();
        assertThat(overtimeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHoursIsRequired() throws Exception {
        int databaseSizeBeforeTest = overtimeRepository.findAll().size();
        // set the field null
        overtime.setHours(null);

        // Create the Overtime, which fails.
        OvertimeDTO overtimeDTO = overtimeMapper.toDto(overtime);

        restOvertimeMockMvc.perform(post("/api/overtimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(overtimeDTO)))
            .andExpect(status().isBadRequest());

        List<Overtime> overtimeList = overtimeRepository.findAll();
        assertThat(overtimeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReasonIsRequired() throws Exception {
        int databaseSizeBeforeTest = overtimeRepository.findAll().size();
        // set the field null
        overtime.setReason(null);

        // Create the Overtime, which fails.
        OvertimeDTO overtimeDTO = overtimeMapper.toDto(overtime);

        restOvertimeMockMvc.perform(post("/api/overtimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(overtimeDTO)))
            .andExpect(status().isBadRequest());

        List<Overtime> overtimeList = overtimeRepository.findAll();
        assertThat(overtimeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOvertimes() throws Exception {
        // Initialize the database
        overtimeRepository.saveAndFlush(overtime);

        // Get all the overtimeList
        restOvertimeMockMvc.perform(get("/api/overtimes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(overtime.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(sameInstant(DEFAULT_DATE))))
            .andExpect(jsonPath("$.[*].hours").value(hasItem(DEFAULT_HOURS.doubleValue())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getOvertime() throws Exception {
        // Initialize the database
        overtimeRepository.saveAndFlush(overtime);

        // Get the overtime
        restOvertimeMockMvc.perform(get("/api/overtimes/{id}", overtime.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(overtime.getId().intValue()))
            .andExpect(jsonPath("$.date").value(sameInstant(DEFAULT_DATE)))
            .andExpect(jsonPath("$.hours").value(DEFAULT_HOURS.doubleValue()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingOvertime() throws Exception {
        // Get the overtime
        restOvertimeMockMvc.perform(get("/api/overtimes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOvertime() throws Exception {
        // Initialize the database
        overtimeRepository.saveAndFlush(overtime);

        int databaseSizeBeforeUpdate = overtimeRepository.findAll().size();

        // Update the overtime
        Overtime updatedOvertime = overtimeRepository.findById(overtime.getId()).get();
        // Disconnect from session so that the updates on updatedOvertime are not directly saved in db
        em.detach(updatedOvertime);
        updatedOvertime
            .date(UPDATED_DATE)
            .hours(UPDATED_HOURS)
            .reason(UPDATED_REASON)
            .status(UPDATED_STATUS);
        OvertimeDTO overtimeDTO = overtimeMapper.toDto(updatedOvertime);

        restOvertimeMockMvc.perform(put("/api/overtimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(overtimeDTO)))
            .andExpect(status().isOk());

        // Validate the Overtime in the database
        List<Overtime> overtimeList = overtimeRepository.findAll();
        assertThat(overtimeList).hasSize(databaseSizeBeforeUpdate);
        Overtime testOvertime = overtimeList.get(overtimeList.size() - 1);
        assertThat(testOvertime.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testOvertime.getHours()).isEqualTo(UPDATED_HOURS);
        assertThat(testOvertime.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testOvertime.isStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingOvertime() throws Exception {
        int databaseSizeBeforeUpdate = overtimeRepository.findAll().size();

        // Create the Overtime
        OvertimeDTO overtimeDTO = overtimeMapper.toDto(overtime);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOvertimeMockMvc.perform(put("/api/overtimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(overtimeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Overtime in the database
        List<Overtime> overtimeList = overtimeRepository.findAll();
        assertThat(overtimeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOvertime() throws Exception {
        // Initialize the database
        overtimeRepository.saveAndFlush(overtime);

        int databaseSizeBeforeDelete = overtimeRepository.findAll().size();

        // Delete the overtime
        restOvertimeMockMvc.perform(delete("/api/overtimes/{id}", overtime.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Overtime> overtimeList = overtimeRepository.findAll();
        assertThat(overtimeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Overtime.class);
        Overtime overtime1 = new Overtime();
        overtime1.setId(1L);
        Overtime overtime2 = new Overtime();
        overtime2.setId(overtime1.getId());
        assertThat(overtime1).isEqualTo(overtime2);
        overtime2.setId(2L);
        assertThat(overtime1).isNotEqualTo(overtime2);
        overtime1.setId(null);
        assertThat(overtime1).isNotEqualTo(overtime2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OvertimeDTO.class);
        OvertimeDTO overtimeDTO1 = new OvertimeDTO();
        overtimeDTO1.setId(1L);
        OvertimeDTO overtimeDTO2 = new OvertimeDTO();
        assertThat(overtimeDTO1).isNotEqualTo(overtimeDTO2);
        overtimeDTO2.setId(overtimeDTO1.getId());
        assertThat(overtimeDTO1).isEqualTo(overtimeDTO2);
        overtimeDTO2.setId(2L);
        assertThat(overtimeDTO1).isNotEqualTo(overtimeDTO2);
        overtimeDTO1.setId(null);
        assertThat(overtimeDTO1).isNotEqualTo(overtimeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(overtimeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(overtimeMapper.fromId(null)).isNull();
    }
}
