package com.ashish.themefoods.webapi.web.rest;

import com.ashish.themefoods.webapi.WebapiApp;

import com.ashish.themefoods.webapi.domain.AdvanceTransaction;
import com.ashish.themefoods.webapi.repository.AdvanceTransactionRepository;
import com.ashish.themefoods.webapi.service.AdvanceTransactionService;
import com.ashish.themefoods.webapi.service.dto.AdvanceTransactionDTO;
import com.ashish.themefoods.webapi.service.mapper.AdvanceTransactionMapper;
import com.ashish.themefoods.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.ashish.themefoods.webapi.web.rest.TestUtil.sameInstant;
import static com.ashish.themefoods.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdvanceTransactionResource REST controller.
 *
 * @see AdvanceTransactionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class AdvanceTransactionResourceIntTest {

    private static final ZonedDateTime DEFAULT_TAKEN_ON = ZonedDateTime.ofLocalInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TAKEN_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;

    private static final String DEFAULT_TYPE_OF_ADVANCE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_OF_ADVANCE = "BBBBBBBBBB";

    private static final Integer DEFAULT_TENURE = 1;
    private static final Integer UPDATED_TENURE = 2;

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    @Autowired
    private AdvanceTransactionRepository advanceTransactionRepository;

    @Autowired
    private AdvanceTransactionMapper advanceTransactionMapper;

    @Autowired
    private AdvanceTransactionService advanceTransactionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAdvanceTransactionMockMvc;

    private AdvanceTransaction advanceTransaction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdvanceTransactionResource advanceTransactionResource = new AdvanceTransactionResource(advanceTransactionService);
        this.restAdvanceTransactionMockMvc = MockMvcBuilders.standaloneSetup(advanceTransactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdvanceTransaction createEntity(EntityManager em) {
        AdvanceTransaction advanceTransaction = new AdvanceTransaction()
            .takenOn(DEFAULT_TAKEN_ON)
            .amount(DEFAULT_AMOUNT)
            .typeOfAdvance(DEFAULT_TYPE_OF_ADVANCE)
            .tenure(DEFAULT_TENURE)
            .reason(DEFAULT_REASON);
        return advanceTransaction;
    }

    @Before
    public void initTest() {
        advanceTransaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdvanceTransaction() throws Exception {
        int databaseSizeBeforeCreate = advanceTransactionRepository.findAll().size();

        // Create the AdvanceTransaction
        AdvanceTransactionDTO advanceTransactionDTO = advanceTransactionMapper.toDto(advanceTransaction);
        restAdvanceTransactionMockMvc.perform(post("/api/advance-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advanceTransactionDTO)))
            .andExpect(status().isCreated());

        // Validate the AdvanceTransaction in the database
        List<AdvanceTransaction> advanceTransactionList = advanceTransactionRepository.findAll();
        assertThat(advanceTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        AdvanceTransaction testAdvanceTransaction = advanceTransactionList.get(advanceTransactionList.size() - 1);
        assertThat(testAdvanceTransaction.getTakenOn()).isEqualTo(DEFAULT_TAKEN_ON);
        assertThat(testAdvanceTransaction.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testAdvanceTransaction.getTypeOfAdvance()).isEqualTo(DEFAULT_TYPE_OF_ADVANCE);
        assertThat(testAdvanceTransaction.getTenure()).isEqualTo(DEFAULT_TENURE);
        assertThat(testAdvanceTransaction.getReason()).isEqualTo(DEFAULT_REASON);
    }

    @Test
    @Transactional
    public void createAdvanceTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = advanceTransactionRepository.findAll().size();

        // Create the AdvanceTransaction with an existing ID
        advanceTransaction.setId(1L);
        AdvanceTransactionDTO advanceTransactionDTO = advanceTransactionMapper.toDto(advanceTransaction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdvanceTransactionMockMvc.perform(post("/api/advance-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advanceTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdvanceTransaction in the database
        List<AdvanceTransaction> advanceTransactionList = advanceTransactionRepository.findAll();
        assertThat(advanceTransactionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAdvanceTransactions() throws Exception {
        // Initialize the database
        advanceTransactionRepository.saveAndFlush(advanceTransaction);

        // Get all the advanceTransactionList
        restAdvanceTransactionMockMvc.perform(get("/api/advance-transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(advanceTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].takenOn").value(hasItem(sameInstant(DEFAULT_TAKEN_ON))))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].typeOfAdvance").value(hasItem(DEFAULT_TYPE_OF_ADVANCE.toString())))
            .andExpect(jsonPath("$.[*].tenure").value(hasItem(DEFAULT_TENURE)))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())));
    }
    
    @Test
    @Transactional
    public void getAdvanceTransaction() throws Exception {
        // Initialize the database
        advanceTransactionRepository.saveAndFlush(advanceTransaction);

        // Get the advanceTransaction
        restAdvanceTransactionMockMvc.perform(get("/api/advance-transactions/{id}", advanceTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(advanceTransaction.getId().intValue()))
            .andExpect(jsonPath("$.takenOn").value(sameInstant(DEFAULT_TAKEN_ON)))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.typeOfAdvance").value(DEFAULT_TYPE_OF_ADVANCE.toString()))
            .andExpect(jsonPath("$.tenure").value(DEFAULT_TENURE))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAdvanceTransaction() throws Exception {
        // Get the advanceTransaction
        restAdvanceTransactionMockMvc.perform(get("/api/advance-transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdvanceTransaction() throws Exception {
        // Initialize the database
        advanceTransactionRepository.saveAndFlush(advanceTransaction);

        int databaseSizeBeforeUpdate = advanceTransactionRepository.findAll().size();

        // Update the advanceTransaction
        AdvanceTransaction updatedAdvanceTransaction = advanceTransactionRepository.findById(advanceTransaction.getId()).get();
        // Disconnect from session so that the updates on updatedAdvanceTransaction are not directly saved in db
        em.detach(updatedAdvanceTransaction);
        updatedAdvanceTransaction
            .takenOn(UPDATED_TAKEN_ON)
            .amount(UPDATED_AMOUNT)
            .typeOfAdvance(UPDATED_TYPE_OF_ADVANCE)
            .tenure(UPDATED_TENURE)
            .reason(UPDATED_REASON);
        AdvanceTransactionDTO advanceTransactionDTO = advanceTransactionMapper.toDto(updatedAdvanceTransaction);

        restAdvanceTransactionMockMvc.perform(put("/api/advance-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advanceTransactionDTO)))
            .andExpect(status().isOk());

        // Validate the AdvanceTransaction in the database
        List<AdvanceTransaction> advanceTransactionList = advanceTransactionRepository.findAll();
        assertThat(advanceTransactionList).hasSize(databaseSizeBeforeUpdate);
        AdvanceTransaction testAdvanceTransaction = advanceTransactionList.get(advanceTransactionList.size() - 1);
        assertThat(testAdvanceTransaction.getTakenOn()).isEqualTo(UPDATED_TAKEN_ON);
        assertThat(testAdvanceTransaction.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testAdvanceTransaction.getTypeOfAdvance()).isEqualTo(UPDATED_TYPE_OF_ADVANCE);
        assertThat(testAdvanceTransaction.getTenure()).isEqualTo(UPDATED_TENURE);
        assertThat(testAdvanceTransaction.getReason()).isEqualTo(UPDATED_REASON);
    }

    @Test
    @Transactional
    public void updateNonExistingAdvanceTransaction() throws Exception {
        int databaseSizeBeforeUpdate = advanceTransactionRepository.findAll().size();

        // Create the AdvanceTransaction
        AdvanceTransactionDTO advanceTransactionDTO = advanceTransactionMapper.toDto(advanceTransaction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdvanceTransactionMockMvc.perform(put("/api/advance-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advanceTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdvanceTransaction in the database
        List<AdvanceTransaction> advanceTransactionList = advanceTransactionRepository.findAll();
        assertThat(advanceTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAdvanceTransaction() throws Exception {
        // Initialize the database
        advanceTransactionRepository.saveAndFlush(advanceTransaction);

        int databaseSizeBeforeDelete = advanceTransactionRepository.findAll().size();

        // Delete the advanceTransaction
        restAdvanceTransactionMockMvc.perform(delete("/api/advance-transactions/{id}", advanceTransaction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AdvanceTransaction> advanceTransactionList = advanceTransactionRepository.findAll();
        assertThat(advanceTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdvanceTransaction.class);
        AdvanceTransaction advanceTransaction1 = new AdvanceTransaction();
        advanceTransaction1.setId(1L);
        AdvanceTransaction advanceTransaction2 = new AdvanceTransaction();
        advanceTransaction2.setId(advanceTransaction1.getId());
        assertThat(advanceTransaction1).isEqualTo(advanceTransaction2);
        advanceTransaction2.setId(2L);
        assertThat(advanceTransaction1).isNotEqualTo(advanceTransaction2);
        advanceTransaction1.setId(null);
        assertThat(advanceTransaction1).isNotEqualTo(advanceTransaction2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdvanceTransactionDTO.class);
        AdvanceTransactionDTO advanceTransactionDTO1 = new AdvanceTransactionDTO();
        advanceTransactionDTO1.setId(1L);
        AdvanceTransactionDTO advanceTransactionDTO2 = new AdvanceTransactionDTO();
        assertThat(advanceTransactionDTO1).isNotEqualTo(advanceTransactionDTO2);
        advanceTransactionDTO2.setId(advanceTransactionDTO1.getId());
        assertThat(advanceTransactionDTO1).isEqualTo(advanceTransactionDTO2);
        advanceTransactionDTO2.setId(2L);
        assertThat(advanceTransactionDTO1).isNotEqualTo(advanceTransactionDTO2);
        advanceTransactionDTO1.setId(null);
        assertThat(advanceTransactionDTO1).isNotEqualTo(advanceTransactionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(advanceTransactionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(advanceTransactionMapper.fromId(null)).isNull();
    }
}
