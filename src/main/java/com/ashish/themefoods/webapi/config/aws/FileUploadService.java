package com.ashish.themefoods.webapi.config.aws;

import com.ashish.themefoods.webapi.web.rest.errors.GlobalCustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.zalando.problem.Status;

import static com.ashish.themefoods.webapi.web.rest.errors.ErrorConstants.UPLOAD_FAILED;
import static com.ashish.themefoods.webapi.web.rest.errors.ErrorConstants.UPLOAD_FAILED_MSG;

@Service
public class FileUploadService {

    private final Logger log = LoggerFactory.getLogger(FileUploadService.class);

    @Autowired
    private AWSS3StorageService storageService;

    @Value("${cloud.aws.s3.product.bucket.name}")
    private String bucket;

    @Value("${cloud.aws.s3.product.bucket.url}")
    private String endPointUrl;

    public String fileUploadWithMultipart(MultipartFile mFile, String fileName) {
        try {
            storageService.uploadStream(mFile, fileName, bucket);
            return endPointUrl + "/" + bucket + "/" + fileName;
        } catch (GlobalCustomException globalException) {
            log.debug("Error while uploading: " + globalException.getLocalizedMessage());
            throw new GlobalCustomException(UPLOAD_FAILED, UPLOAD_FAILED_MSG, Status.BAD_REQUEST);
        }
    }
}
