package com.ashish.themefoods.webapi.config.aws;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.ashish.themefoods.webapi.web.rest.errors.GlobalCustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.zalando.problem.Status;

import java.io.IOException;

import static com.ashish.themefoods.webapi.web.rest.errors.ErrorConstants.UPLOAD_FAILED;
import static com.ashish.themefoods.webapi.web.rest.errors.ErrorConstants.UPLOAD_FAILED_MSG;

@Service
public class AWSS3StorageService {

    private final Logger log = LoggerFactory.getLogger(AWSS3StorageService.class);

    @Autowired
    private AmazonS3 amazonS3;

    @Autowired
    private TransferManager transferManager;


    public void uploadStream(MultipartFile mFile, String uploadKey, String bucket) {

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(mFile.getSize());
        metadata.setContentType(mFile.getContentType());

        try {
            Upload xfer = transferManager.upload(new PutObjectRequest(bucket, uploadKey, mFile.getInputStream(), metadata).withCannedAcl(CannedAccessControlList.PublicRead));
            xfer.waitForUploadResult();
        } catch (AmazonServiceException | InterruptedException | IOException e) {
            log.error(e.getMessage());
            throw new GlobalCustomException(UPLOAD_FAILED, UPLOAD_FAILED_MSG, Status.BAD_REQUEST);
        }
    }
}
