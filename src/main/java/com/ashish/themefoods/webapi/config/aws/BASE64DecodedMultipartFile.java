package com.ashish.themefoods.webapi.config.aws;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;

//TODO: To support file upload through Base64 in future during development. Remove once the approach is confirmed
public class BASE64DecodedMultipartFile implements MultipartFile {
    private final byte[] imgContent;

    public BASE64DecodedMultipartFile(byte[] imgContent) {
        this.imgContent = imgContent;
    }

    @Override
    public String getName() {
        // TODO - implementation depends on your requirements
        return null;
    }

    @Override
    public String getOriginalFilename() {
        // TODO - implementation depends on your requirements
        return null;
    }

    @Override
    public String getContentType() {
        // TODO - implementation depends on your requirements
        return null;
    }

    @Override
    public boolean isEmpty() {
        return imgContent == null || imgContent.length == 0;
    }

    @Override
    public long getSize() {
        return imgContent.length;
    }

    @Override
    public byte[] getBytes() throws IOException {
        return imgContent;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(imgContent);
    }


    @Override
    public void transferTo(File dest) throws IOException {
        OutputStream outputStream = new FileOutputStream(dest);
        try {
            outputStream.write(imgContent);
        } finally {
            outputStream.close();
        }
    }
}
