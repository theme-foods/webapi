package com.ashish.themefoods.webapi.service;

import com.ashish.themefoods.webapi.domain.LeaveDashboard;
import com.ashish.themefoods.webapi.domain.Overtime;
import com.ashish.themefoods.webapi.repository.LeaveDashboardRepository;
import com.ashish.themefoods.webapi.repository.OvertimeRepository;
import com.ashish.themefoods.webapi.service.dto.OvertimeDTO;
import com.ashish.themefoods.webapi.service.mapper.OvertimeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Overtime.
 */
@Service
@Transactional
public class OvertimeService {

    private final Logger log = LoggerFactory.getLogger(OvertimeService.class);

    private final OvertimeRepository overtimeRepository;

    private final OvertimeMapper overtimeMapper;

    private final LeaveDashboardRepository leaveDashboardRepository;

    public OvertimeService(OvertimeRepository overtimeRepository, OvertimeMapper overtimeMapper, LeaveDashboardRepository leaveDashboardRepository) {
        this.overtimeRepository = overtimeRepository;
        this.overtimeMapper = overtimeMapper;
        this.leaveDashboardRepository = leaveDashboardRepository;
    }

    /**
     * Save a overtime.
     *
     * @param overtimeDTO the entity to save
     * @return the persisted entity
     */
    public OvertimeDTO save(OvertimeDTO overtimeDTO) {
        log.debug("Request to save Overtime : {}", overtimeDTO);
        Overtime overtime = overtimeMapper.toEntity(overtimeDTO);
        overtime = overtimeRepository.save(overtime);
        return overtimeMapper.toDto(overtime);
    }

    /**
     * Get all the overtimes.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<OvertimeDTO> findAll() {
        log.debug("Request to get all Overtimes");
        return overtimeRepository.findAllByOrderByDateDesc().stream()
                .map(overtimeMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one overtime by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<OvertimeDTO> findOne(Long id) {
        log.debug("Request to get Overtime : {}", id);
        return overtimeRepository.findById(id)
                .map(overtimeMapper::toDto);
    }

    /**
     * Delete the overtime by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Overtime : {}", id);
        overtimeRepository.deleteById(id);
    }
}
