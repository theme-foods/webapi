package com.ashish.themefoods.webapi.service.mapper;

import com.ashish.themefoods.webapi.domain.*;
import com.ashish.themefoods.webapi.service.dto.AdvanceTransactionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AdvanceTransaction and its DTO AdvanceTransactionDTO.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface AdvanceTransactionMapper extends EntityMapper<AdvanceTransactionDTO, AdvanceTransaction> {

    @Mapping(source = "employee.id", target = "employeeId")
    AdvanceTransactionDTO toDto(AdvanceTransaction advanceTransaction);

    @Mapping(source = "employeeId", target = "employee")
    AdvanceTransaction toEntity(AdvanceTransactionDTO advanceTransactionDTO);

    default AdvanceTransaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdvanceTransaction advanceTransaction = new AdvanceTransaction();
        advanceTransaction.setId(id);
        return advanceTransaction;
    }
}
