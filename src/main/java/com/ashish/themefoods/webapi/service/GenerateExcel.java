package com.ashish.themefoods.webapi.service;

import com.ashish.themefoods.webapi.domain.SalarySlip;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class GenerateExcel {

    private static String[] columns = {"Sr No.", "Employee Name", "Office", "Days", "Salary", "Basic + DA", "HRA", "Other Allow.",
            "PF(12%)", "ESI(1.75%)", "PT", "Extra Leaves","Absents", "Advance", "Overtime", "Payout", "Deductions", "Actual Paid"};

    public static ByteArrayInputStream salaryInExcel(List<SalarySlip> salarySlipList) {
        Collections.sort(salarySlipList, (s1, s2) -> {
            if(s1.getEmployee() != null && s1.getEmployee().getOffice() != null && s2.getEmployee().getOffice() != null) {
                return  s1.getEmployee().getOffice().compareTo(s2.getEmployee().getOffice());
            }
            return -1;
        });
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Workbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Date date = Date.from(salarySlipList.get(0).getSalaryDate().toInstant());
        String month = new SimpleDateFormat("MMMM").format(date);
        Sheet sheet = workbook.createSheet(month + " " + salarySlipList.get(0).getSalaryDate().getYear());

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 10);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        // Create Other rows and cells with employees data
        int rowNum = 1;
        for (SalarySlip salarySlip : salarySlipList) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0)
                    .setCellValue(rowNum - 1);

            row.createCell(1)
                    .setCellValue(salarySlip.getEmployee() != null ? ((salarySlip.getEmployee().getFirstName() != null ? salarySlip.getEmployee().getFirstName() : "") + " " +
                            (salarySlip.getEmployee().getLastName() != null ? salarySlip.getEmployee().getLastName() : "")) : "");

            row.createCell(2).setCellValue(salarySlip.getEmployee() != null ? (salarySlip.getEmployee().getOffice() != null ? salarySlip.getEmployee().getOffice(): "") : "");
            row.createCell(3).setCellValue(salarySlip.getDays());

            row.createCell(4).setCellValue(salarySlip.getTotalSalary());
            row.createCell(5).setCellValue(salarySlip.getBasicDA() != null ? Double.toString(Math.round(salarySlip.getBasicDA())) : "0");
            row.createCell(6).setCellValue(salarySlip.getHra() != null ? Double.toString(Math.round(salarySlip.getHra())) : "0");
            row.createCell(7).setCellValue(salarySlip.getOtherAllowances() != null ? Double.toString(Math.round(salarySlip.getOtherAllowances())) : "0");
            row.createCell(8).setCellValue(salarySlip.getPf() != null ? Double.toString(Math.round(salarySlip.getPf())) : "0");
            row.createCell(9).setCellValue(salarySlip.getEsi() != null ? Double.toString(Math.round(salarySlip.getEsi())) : "0");
            row.createCell(10).setCellValue(salarySlip.getPt() != null ? Double.toString(Math.round(salarySlip.getPt())) : "0");
            row.createCell(11).setCellValue(salarySlip.getExtraLeaves() != null ? Double.toString(Math.round(salarySlip.getExtraLeaves())) : "0");
            row.createCell(12).setCellValue(salarySlip.getAbsents() != null ? Double.toString(Math.round(salarySlip.getAbsents())) : "0");
            row.createCell(13).setCellValue(salarySlip.getAdvance() != null ? Double.toString(Math.round(salarySlip.getAdvance())) : "0");
            row.createCell(14).setCellValue(salarySlip.getOvertime() != null ? Double.toString(Math.round(salarySlip.getOvertime())) : "0");
            row.createCell(15).setCellValue(salarySlip.getPayout() != null ? Double.toString(Math.round(salarySlip.getPayout())) : "0");

            Double totalDeductions = (salarySlip.getEsi() != null ? salarySlip.getEsi() : 0) + (salarySlip.getPt() != null ? salarySlip.getPt() : 0) +
                    (salarySlip.getPf() != null ? salarySlip.getPf() : 0);

            row.createCell(16).setCellValue(totalDeductions != null ? Double.toString(Math.round(totalDeductions)) : "0");
            row.createCell(17).setCellValue(salarySlip.getActualPaid() != null ? Double.toString(Math.round(salarySlip.getActualPaid())) : "0");
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        try {
            workbook.write(bos);
            return new ByteArrayInputStream(bos.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
