package com.ashish.themefoods.webapi.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the LeaveDashboard entity.
 */
public class LeaveDashboardDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private Double availableLeaves;

    private Double takenLeaves;


    private Long employeeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAvailableLeaves() {
        return availableLeaves;
    }

    public void setAvailableLeaves(Double availableLeaves) {
        this.availableLeaves = availableLeaves;
    }

    public Double getTakenLeaves() {
        return takenLeaves;
    }

    public void setTakenLeaves(Double takenLeaves) {
        this.takenLeaves = takenLeaves;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LeaveDashboardDTO leaveDashboardDTO = (LeaveDashboardDTO) o;
        if (leaveDashboardDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), leaveDashboardDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LeaveDashboardDTO{" +
            "id=" + getId() +
            ", availableLeaves=" + getAvailableLeaves() +
            ", takenLeaves=" + getTakenLeaves() +
            ", employee=" + getEmployeeId() +
            "}";
    }
}
