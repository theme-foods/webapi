package com.ashish.themefoods.webapi.service.dto;

public interface AdvanceTransactionProjection {

    Long getEmployeeId();

    Double getAmount();

    Double getAmountRemaining();

    Integer getTenure();
}
