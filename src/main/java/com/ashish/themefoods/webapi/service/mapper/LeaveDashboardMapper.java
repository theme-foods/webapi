package com.ashish.themefoods.webapi.service.mapper;

import com.ashish.themefoods.webapi.domain.*;
import com.ashish.themefoods.webapi.service.dto.LeaveDashboardDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity LeaveDashboard and its DTO LeaveDashboardDTO.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface LeaveDashboardMapper extends EntityMapper<LeaveDashboardDTO, LeaveDashboard> {

    @Mapping(source = "employee.id", target = "employeeId")
    LeaveDashboardDTO toDto(LeaveDashboard leaveDashboard);

    @Mapping(source = "employeeId", target = "employee")
    LeaveDashboard toEntity(LeaveDashboardDTO leaveDashboardDTO);

    default LeaveDashboard fromId(Long id) {
        if (id == null) {
            return null;
        }
        LeaveDashboard leaveDashboard = new LeaveDashboard();
        leaveDashboard.setId(id);
        return leaveDashboard;
    }
}
