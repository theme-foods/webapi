package com.ashish.themefoods.webapi.service.mapper;

import com.ashish.themefoods.webapi.domain.*;
import com.ashish.themefoods.webapi.service.dto.SalarySlipDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SalarySlip and its DTO SalarySlipDTO.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface SalarySlipMapper extends EntityMapper<SalarySlipDTO, SalarySlip> {

    @Mapping(source = "employee.id", target = "employeeId")
    SalarySlipDTO toDto(SalarySlip salarySlip);

    @Mapping(source = "employeeId", target = "employee")
    SalarySlip toEntity(SalarySlipDTO salarySlipDTO);

    default SalarySlip fromId(Long id) {
        if (id == null) {
            return null;
        }
        SalarySlip salarySlip = new SalarySlip();
        salarySlip.setId(id);
        return salarySlip;
    }
}
