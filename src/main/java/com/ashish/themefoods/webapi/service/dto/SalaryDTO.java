package com.ashish.themefoods.webapi.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Salary entity.
 */
public class SalaryDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private Double totalSalary;

    private Double basicDA;

    private Double hra;

    private Double conveyance;

    private Double otherAllowances;

    private Double pf;

    private Double esi;

    private Double pt;


    private Long employeeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTotalSalary() {
        return totalSalary;
    }

    public void setTotalSalary(Double totalSalary) {
        this.totalSalary = totalSalary;
    }

    public Double getBasicDA() {
        return basicDA;
    }

    public void setBasicDA(Double basicDA) {
        this.basicDA = basicDA;
    }

    public Double getHra() {
        return hra;
    }

    public void setHra(Double hra) {
        this.hra = hra;
    }

    public Double getConveyance() {
        return conveyance;
    }

    public void setConveyance(Double conveyance) {
        this.conveyance = conveyance;
    }

    public Double getOtherAllowances() {
        return otherAllowances;
    }

    public void setOtherAllowances(Double otherAllowances) {
        this.otherAllowances = otherAllowances;
    }

    public Double getPf() {
        return pf;
    }

    public void setPf(Double pf) {
        this.pf = pf;
    }

    public Double getEsi() {
        return esi;
    }

    public void setEsi(Double esi) {
        this.esi = esi;
    }

    public Double getPt() {
        return pt;
    }

    public void setPt(Double pt) {
        this.pt = pt;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SalaryDTO salaryDTO = (SalaryDTO) o;
        if (salaryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), salaryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SalaryDTO{" +
            "id=" + getId() +
            ", totalSalary=" + getTotalSalary() +
            ", basicDA=" + getBasicDA() +
            ", hra=" + getHra() +
            ", conveyance=" + getConveyance() +
            ", otherAllowances=" + getOtherAllowances() +
            ", pf=" + getPf() +
            ", esi=" + getEsi() +
            ", pt=" + getPt() +
            ", employee=" + getEmployeeId() +
            "}";
    }
}
