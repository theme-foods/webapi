package com.ashish.themefoods.webapi.service.mapper;

import com.ashish.themefoods.webapi.domain.*;
import com.ashish.themefoods.webapi.service.dto.DocumentsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Documents and its DTO DocumentsDTO.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface DocumentsMapper extends EntityMapper<DocumentsDTO, Documents> {

    @Mapping(source = "employee.id", target = "employeeId")
    DocumentsDTO toDto(Documents documents);

    @Mapping(source = "employeeId", target = "employee")
    Documents toEntity(DocumentsDTO documentsDTO);

    default Documents fromId(Long id) {
        if (id == null) {
            return null;
        }
        Documents documents = new Documents();
        documents.setId(id);
        return documents;
    }
}
