package com.ashish.themefoods.webapi.service;

import com.ashish.themefoods.webapi.domain.LeaveDashboardHistory;
import com.ashish.themefoods.webapi.repository.LeaveDashboardHistoryRepository;
import com.ashish.themefoods.webapi.service.dto.LeaveDashboardHistoryDTO;
import com.ashish.themefoods.webapi.service.mapper.LeaveDashboardHistoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing LeaveDashboardHistory.
 */
@Service
@Transactional
public class LeaveDashboardHistoryService {

    private final Logger log = LoggerFactory.getLogger(LeaveDashboardHistoryService.class);

    private final LeaveDashboardHistoryRepository leaveDashboardHistoryRepository;

    private final LeaveDashboardHistoryMapper leaveDashboardHistoryMapper;

    public LeaveDashboardHistoryService(LeaveDashboardHistoryRepository leaveDashboardHistoryRepository, LeaveDashboardHistoryMapper leaveDashboardHistoryMapper) {
        this.leaveDashboardHistoryRepository = leaveDashboardHistoryRepository;
        this.leaveDashboardHistoryMapper = leaveDashboardHistoryMapper;
    }

    /**
     * Save a leaveDashboardHistory.
     *
     * @param leaveDashboardHistoryDTO the entity to save
     * @return the persisted entity
     */
    public LeaveDashboardHistoryDTO save(LeaveDashboardHistoryDTO leaveDashboardHistoryDTO) {
        log.debug("Request to save LeaveDashboardHistory : {}", leaveDashboardHistoryDTO);
        LeaveDashboardHistory leaveDashboardHistory = leaveDashboardHistoryMapper.toEntity(leaveDashboardHistoryDTO);
        leaveDashboardHistory = leaveDashboardHistoryRepository.save(leaveDashboardHistory);
        return leaveDashboardHistoryMapper.toDto(leaveDashboardHistory);
    }

    /**
     * Get all the leaveDashboardHistories.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<LeaveDashboardHistoryDTO> findAll() {
        log.debug("Request to get all LeaveDashboardHistories");
        return leaveDashboardHistoryRepository.findAll().stream()
            .map(leaveDashboardHistoryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one leaveDashboardHistory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<LeaveDashboardHistoryDTO> findOne(Long id) {
        log.debug("Request to get LeaveDashboardHistory : {}", id);
        return leaveDashboardHistoryRepository.findById(id)
            .map(leaveDashboardHistoryMapper::toDto);
    }

    /**
     * Delete the leaveDashboardHistory by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete LeaveDashboardHistory : {}", id);
        leaveDashboardHistoryRepository.deleteById(id);
    }
}
