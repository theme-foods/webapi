package com.ashish.themefoods.webapi.service.mapper;

import com.ashish.themefoods.webapi.domain.*;
import com.ashish.themefoods.webapi.service.dto.LeaveDashboardHistoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity LeaveDashboardHistory and its DTO LeaveDashboardHistoryDTO.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface LeaveDashboardHistoryMapper extends EntityMapper<LeaveDashboardHistoryDTO, LeaveDashboardHistory> {

    @Mapping(source = "employee.id", target = "employeeId")
    LeaveDashboardHistoryDTO toDto(LeaveDashboardHistory leaveDashboardHistory);

    @Mapping(source = "employeeId", target = "employee")
    LeaveDashboardHistory toEntity(LeaveDashboardHistoryDTO leaveDashboardHistoryDTO);

    default LeaveDashboardHistory fromId(Long id) {
        if (id == null) {
            return null;
        }
        LeaveDashboardHistory leaveDashboardHistory = new LeaveDashboardHistory();
        leaveDashboardHistory.setId(id);
        return leaveDashboardHistory;
    }
}
