package com.ashish.themefoods.webapi.service.dto;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Overtime entity.
 */
public class OvertimeDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    @NotNull
    private ZonedDateTime date;

    @NotNull
    private Float hours;

    @NotNull
    private String reason;

    private Boolean status;

    private Long employeeId;

    private EmployeeDTO employee;

    public Boolean getStatus() {
        return status;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public Float getHours() {
        return hours;
    }

    public void setHours(Float hours) {
        this.hours = hours;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OvertimeDTO overtimeDTO = (OvertimeDTO) o;
        if (overtimeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), overtimeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OvertimeDTO{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", hours=" + getHours() +
            ", reason='" + getReason() + "'" +
            ", status='" + isStatus() + "'" +
            ", employee=" + getEmployeeId() +
            "}";
    }
}
