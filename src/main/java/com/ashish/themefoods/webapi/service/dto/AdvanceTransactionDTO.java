package com.ashish.themefoods.webapi.service.dto;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AdvanceTransaction entity.
 */
public class AdvanceTransactionDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private ZonedDateTime takenOn;

    private Double amount;

    private String typeOfAdvance;

    private Integer tenure;

    private String reason;

    private Long employeeId;

    private EmployeeDTO employee;

    private Double amountRemaining;

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    public Double getAmountRemaining() {
        return amountRemaining;
    }

    public void setAmountRemaining(Double amountRemaining) {
        this.amountRemaining = amountRemaining;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getTakenOn() {
        return takenOn;
    }

    public void setTakenOn(ZonedDateTime takenOn) {
        this.takenOn = takenOn;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTypeOfAdvance() {
        return typeOfAdvance;
    }

    public void setTypeOfAdvance(String typeOfAdvance) {
        this.typeOfAdvance = typeOfAdvance;
    }

    public Integer getTenure() {
        return tenure;
    }

    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdvanceTransactionDTO advanceTransactionDTO = (AdvanceTransactionDTO) o;
        if (advanceTransactionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), advanceTransactionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdvanceTransactionDTO{" +
            "id=" + getId() +
            ", takenOn='" + getTakenOn() + "'" +
            ", amount=" + getAmount() +
            ", typeOfAdvance='" + getTypeOfAdvance() + "'" +
            ", tenure=" + getTenure() +
            ", reason='" + getReason() + "'" +
            ", employee=" + getEmployeeId() +
            "}";
    }
}
