package com.ashish.themefoods.webapi.service;

import com.ashish.themefoods.webapi.domain.AdvanceTransaction;
import com.ashish.themefoods.webapi.domain.AdvanceTransactionHistory;
import com.ashish.themefoods.webapi.repository.AdvanceTransactionHistoryRepository;
import com.ashish.themefoods.webapi.repository.AdvanceTransactionRepository;
import com.ashish.themefoods.webapi.service.dto.AdvanceTransactionDTO;
import com.ashish.themefoods.webapi.service.dto.AdvanceTransactionProjection;
import com.ashish.themefoods.webapi.service.mapper.AdvanceTransactionMapper;
import com.ashish.themefoods.webapi.service.mapper.AdvanceTransactionToHistoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing AdvanceTransaction.
 */
@Service
@Transactional
public class AdvanceTransactionService {

    private final Logger log = LoggerFactory.getLogger(AdvanceTransactionService.class);

    private final AdvanceTransactionRepository advanceTransactionRepository;

    private final AdvanceTransactionMapper advanceTransactionMapper;

    private final AdvanceTransactionHistoryRepository advanceTransactionHistoryRepository;

    private final AdvanceTransactionToHistoryMapper advanceTransactionToHistoryMapper;

    public AdvanceTransactionService(AdvanceTransactionRepository advanceTransactionRepository, AdvanceTransactionMapper advanceTransactionMapper, AdvanceTransactionHistoryRepository advanceTransactionHistoryRepository, AdvanceTransactionToHistoryMapper advanceTransactionToHistoryMapper) {
        this.advanceTransactionRepository = advanceTransactionRepository;
        this.advanceTransactionMapper = advanceTransactionMapper;
        this.advanceTransactionHistoryRepository = advanceTransactionHistoryRepository;
        this.advanceTransactionToHistoryMapper = advanceTransactionToHistoryMapper;
    }

    /**
     * Save a advanceTransaction.
     *
     * @param advanceTransactionDTO the entity to save
     * @return the persisted entity
     */
    public AdvanceTransactionDTO saveNew(AdvanceTransactionDTO advanceTransactionDTO) {
        log.debug("Request to save AdvanceTransaction : {}", advanceTransactionDTO);
        AdvanceTransactionHistory advanceTransactionHistory = advanceTransactionToHistoryMapper.toEntity(advanceTransactionDTO);
        advanceTransactionHistoryRepository.save(advanceTransactionHistory);
        List<AdvanceTransaction> advanceTransactionList = advanceTransactionRepository.findAllByTypeOfAdvanceAndEmployeeId("Credit", advanceTransactionDTO.getEmployeeId());
        if(!advanceTransactionList.isEmpty() && advanceTransactionList.size() == 1) {
           int rowsUpdated = advanceTransactionRepository.updateAdvanceForEmployee(advanceTransactionDTO.getEmployeeId(),
                   advanceTransactionList.get(0).getAmount() + advanceTransactionDTO.getAmount(), advanceTransactionList.get(0).getTenure() + advanceTransactionDTO.getTenure());
            if(rowsUpdated == 1) {
                return advanceTransactionMapper.toDto(advanceTransactionRepository.findAllByTypeOfAdvanceAndEmployeeId("Credit", advanceTransactionDTO.getEmployeeId()).get(0));
            }
            return null;
        }
        else {
            AdvanceTransaction advanceTransaction = advanceTransactionMapper.toEntity(advanceTransactionDTO);
            advanceTransaction = advanceTransactionRepository.save(advanceTransaction);
            return advanceTransactionMapper.toDto(advanceTransaction);
        }
    }

    public AdvanceTransactionDTO save(AdvanceTransactionDTO advanceTransactionDTO) {
        log.debug("Request to save AdvanceTransaction : {}", advanceTransactionDTO);

            AdvanceTransaction advanceTransaction = advanceTransactionMapper.toEntity(advanceTransactionDTO);
            advanceTransaction = advanceTransactionRepository.save(advanceTransaction);
            return advanceTransactionMapper.toDto(advanceTransaction);
    }

    /**
     * Get all the advanceTransactions.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AdvanceTransactionDTO> findAll() {
        log.debug("Request to get all AdvanceTransactions");
        List<AdvanceTransactionProjection> advanceTransactionListByType = advanceTransactionRepository.findAllDetailsOfAdvances();
        List<AdvanceTransactionDTO> advanceTransactionDTOS = advanceTransactionRepository.findAllByTypeOfAdvance("Credit").stream()
                .map(advanceTransactionMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
        advanceTransactionDTOS.forEach(advanceTransactionDTO -> {
            advanceTransactionListByType.forEach(advanceTransactionProjection -> {
                if(advanceTransactionDTO.getEmployeeId().equals(advanceTransactionProjection.getEmployeeId())) {
                    advanceTransactionDTO.setAmount(advanceTransactionProjection.getAmount());
                    advanceTransactionDTO.setAmountRemaining(advanceTransactionProjection.getAmountRemaining());
                }
            });
        });
        return advanceTransactionDTOS;
    }


    /**
     * Get one advanceTransaction by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<AdvanceTransactionDTO> findOne(Long id) {
        log.debug("Request to get AdvanceTransaction : {}", id);
        return advanceTransactionRepository.findById(id)
            .map(advanceTransactionMapper::toDto);
    }

    /**
     * Delete the advanceTransaction by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AdvanceTransaction : {}", id);
        Optional<AdvanceTransaction> advanceTransactionOptional = advanceTransactionRepository.findById(id);
        advanceTransactionOptional.ifPresent(advanceTransaction -> {
            advanceTransactionHistoryRepository.deleteByEmployeeId(advanceTransaction.getEmployee().getId());
        });
        advanceTransactionRepository.deleteById(id);
    }
}
