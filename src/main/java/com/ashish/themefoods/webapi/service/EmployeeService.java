package com.ashish.themefoods.webapi.service;

import com.ashish.themefoods.webapi.config.aws.FileUploadService;
import com.ashish.themefoods.webapi.domain.Documents;
import com.ashish.themefoods.webapi.domain.Employee;
import com.ashish.themefoods.webapi.domain.Salary;
import com.ashish.themefoods.webapi.repository.*;
import com.ashish.themefoods.webapi.service.dto.EmployeeDTO;
import com.ashish.themefoods.webapi.service.mapper.EmployeeMapper;
import com.ashish.themefoods.webapi.web.rest.errors.GlobalCustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.zalando.problem.Status;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.ashish.themefoods.webapi.web.rest.errors.ErrorConstants.EXISTING_EMPLOYEE;
import static com.ashish.themefoods.webapi.web.rest.errors.ErrorConstants.EXISTING_EMPLOYEE_MSG;

/**
 * Service Implementation for managing Employee.
 */
@Service
@Transactional
public class EmployeeService {

    private final Logger log = LoggerFactory.getLogger(EmployeeService.class);

    private final EmployeeRepository employeeRepository;

    private final EmployeeMapper employeeMapper;

    private final SalaryRepository salaryRepository;

    private final FileUploadService uploadService;

    private final DocumentsRepository documentsRepository;

    private final LeaveDashboardService leaveDashboardService;

    private final AdvanceTransactionRepository advanceTransactionRepository;

    private final AdvanceTransactionHistoryRepository advanceTransactionHistoryRepository;

    public EmployeeService(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper, SalaryRepository salaryRepository, FileUploadService uploadService,
                           DocumentsRepository documentsRepository, LeaveDashboardService leaveDashboardService, AdvanceTransactionRepository advanceTransactionRepository,
                           AdvanceTransactionHistoryRepository advanceTransactionHistoryRepository) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
        this.salaryRepository = salaryRepository;
        this.uploadService = uploadService;
        this.documentsRepository = documentsRepository;
        this.leaveDashboardService = leaveDashboardService;
        this.advanceTransactionRepository = advanceTransactionRepository;
        this.advanceTransactionHistoryRepository = advanceTransactionHistoryRepository;
    }

    /**
     * Save a employee.
     *
     * @param employeeDTO the entity to save
     * @return the persisted entity
     */
    public EmployeeDTO save(EmployeeDTO employeeDTO) {
        log.debug("Request to save Employee : {}", employeeDTO);
        if(employeeDTO.getId() == null) {
            Employee existingEmployee = employeeRepository.findByFirstNameIgnoreCaseAndLastNameIgnoreCase(employeeDTO.getFirstName(), employeeDTO.getLastName());
            if(existingEmployee != null && existingEmployee.getId() != null) {
                throw new GlobalCustomException(EXISTING_EMPLOYEE, EXISTING_EMPLOYEE_MSG, Status.BAD_REQUEST);
            }
        }
        Employee employee = employeeMapper.toEntity(employeeDTO);
        employee.setDeleted(false);
        if(employee.getReJoiningDate() != null ) {
            employee.setReJoiningDate(getDate(employee.getReJoiningDate()));
        } else {
            employee.setJoiningDate(getDate(employee.getJoiningDate()));
        }
        employee = employeeRepository.save(employee);
        Salary savedSalary = employee.getSalary();
        savedSalary.setEmployee(employee);
        salaryRepository.save(savedSalary);
        List<Documents> documents = employee.getDocuments();
        for (Documents documents1 : documents) {
            documents1.setEmployee(employee);
        }
        documentsRepository.saveAll(documents);
//        if (employeeDTO.getId() == null) {
            leaveDashboardService.updateLeaveDashboardAfterJoining(employee, false);
//        }
        return employeeMapper.toDto(employee);
    }

    public ZonedDateTime getDate(ZonedDateTime zonedDateTime) {
        ZoneId india = ZoneId.of("Asia/Kolkata");
        DateFormat dfIST = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dfIST.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat dfUtc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dfUtc.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = dfIST.parse(dfIST.format(dfUtc.parse(dfUtc.format(Date.from(zonedDateTime.toInstant())))));
            log.debug("convertedTime using sdf {}", dfIST.format(dfUtc.parse(dfUtc.format(Date.from(zonedDateTime.toInstant())))));
            log.debug("After conversion {}", ZonedDateTime.ofInstant(date.toInstant(), india));
            return ZonedDateTime.ofInstant(date.toInstant(), india);

        }catch (ParseException e) {
            e.printStackTrace();              // invalid input
        }
        return null;
    }



    /**
     * Get all the employees.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<EmployeeDTO> findAll() {
        return employeeRepository.findAllByOrderByIsDeletedAsc()
                .stream().map(employeeMapper::toDto).collect(Collectors.toList());
    }


    /**
     * Get one employee by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<EmployeeDTO> findOne(Long id) {
        log.debug("Request to get Employee : {}", id);
        return employeeRepository.findById(id)
                .map(employeeMapper::toDto);
    }

    /**
     * Delete the employee by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id, Boolean undelete, Long reJoiningDate) {
        log.debug("Request to delete Employee : {} {}", id, reJoiningDate);
        if(reJoiningDate != null) {
            DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS");
            Date result = new Date(reJoiningDate);
            ZonedDateTime date = ZonedDateTime.ofInstant(result.toInstant(), ZoneId.systemDefault());
            log.debug("Date after parsing {}", date);
            employeeRepository.updateDeleteStatus(id, !undelete, date);
            Optional<Employee> employeeOptional = employeeRepository.findById(id);
            employeeOptional.ifPresent(employee -> {
                leaveDashboardService.updateLeaveDashboardAfterJoining(employee, true);
            });
        } else {
            employeeRepository.updateDeleteStatus(id, !undelete);
            advanceTransactionHistoryRepository.deleteByEmployeeId(id);
            advanceTransactionRepository.deleteByEmployeeId(id);
        }
    }

    public String uploadEventFile(MultipartFile file) {
        return uploadService.fileUploadWithMultipart(file, System.currentTimeMillis() + "_" + file.getOriginalFilename());
    }

}
