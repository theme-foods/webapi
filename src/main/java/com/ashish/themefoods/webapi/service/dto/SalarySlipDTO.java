package com.ashish.themefoods.webapi.service.dto;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the SalarySlip entity.
 */
public class SalarySlipDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private Double totalSalary;

    private Double basicDA;

    private Double hra;

    private Double otherAllowances;

    private Double pf;

    private Double esi;

    private Double pt;

    private Double days;

    private Double actualPaid;

    private Double payout;

    private Double advance;

    private EmployeeDTO employee;

    private Long employeeId;

    private ZonedDateTime salaryDate;

    private Double extraLeaves;

    private Double overtime;

    private Double overTimeHours;

    private Double absents;

    public Double getAbsents() {
        return absents;
    }

    public void setAbsents(Double absents) {
        this.absents = absents;
    }

    public Double getExtraLeaves() {
        return extraLeaves;
    }

    public void setExtraLeaves(Double extraLeaves) {
        this.extraLeaves = extraLeaves;
    }

    public Double getOvertime() {
        return overtime;
    }

    public void setOvertime(Double overtime) {
        this.overtime = overtime;
    }

    public Double getOverTimeHours() {
        return overTimeHours;
    }

    public void setOverTimeHours(Double overTimeHours) {
        this.overTimeHours = overTimeHours;
    }

    public ZonedDateTime getSalaryDate() {
        return salaryDate;
    }

    public void setSalaryDate(ZonedDateTime salaryDate) {
        this.salaryDate = salaryDate;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTotalSalary() {
        return totalSalary;
    }

    public void setTotalSalary(Double totalSalary) {
        this.totalSalary = totalSalary;
    }

    public Double getBasicDA() {
        return basicDA;
    }

    public void setBasicDA(Double basicDA) {
        this.basicDA = basicDA;
    }

    public Double getHra() {
        return hra;
    }

    public void setHra(Double hra) {
        this.hra = hra;
    }

    public Double getOtherAllowances() {
        return otherAllowances;
    }

    public void setOtherAllowances(Double otherAllowances) {
        this.otherAllowances = otherAllowances;
    }

    public Double getPf() {
        return pf;
    }

    public void setPf(Double pf) {
        this.pf = pf;
    }

    public Double getEsi() {
        return esi;
    }

    public void setEsi(Double esi) {
        this.esi = esi;
    }

    public Double getPt() {
        return pt;
    }

    public void setPt(Double pt) {
        this.pt = pt;
    }

    public Double getDays() {
        return days;
    }

    public void setDays(Double days) {
        this.days = days;
    }

    public Double getActualPaid() {
        return actualPaid;
    }

    public void setActualPaid(Double actualPaid) {
        this.actualPaid = actualPaid;
    }

    public Double getPayout() {
        return payout;
    }

    public void setPayout(Double payout) {
        this.payout = payout;
    }

    public Double getAdvance() {
        return advance;
    }

    public void setAdvance(Double advance) {
        this.advance = advance;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SalarySlipDTO salarySlipDTO = (SalarySlipDTO) o;
        if (salarySlipDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), salarySlipDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SalarySlipDTO{" +
                "id=" + id +
                ", totalSalary=" + totalSalary +
                ", basicDA=" + basicDA +
                ", hra=" + hra +
                ", otherAllowances=" + otherAllowances +
                ", pf=" + pf +
                ", esi=" + esi +
                ", pt=" + pt +
                ", days=" + days +
                ", actualPaid=" + actualPaid +
                ", payout=" + payout +
                ", advance=" + advance +
                ", employee=" + employee +
                ", employeeId=" + employeeId +
                ", salaryDate=" + salaryDate +
                ", extraLeaves=" + extraLeaves +
                ", overtime=" + overtime +
                ", overTimeHours=" + overTimeHours +
                '}';
    }
}
