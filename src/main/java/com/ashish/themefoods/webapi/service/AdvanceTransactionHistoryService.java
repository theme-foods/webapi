package com.ashish.themefoods.webapi.service;

import com.ashish.themefoods.webapi.domain.AdvanceTransactionHistory;
import com.ashish.themefoods.webapi.repository.AdvanceTransactionHistoryRepository;
import com.ashish.themefoods.webapi.service.dto.AdvanceTransactionHistoryDTO;
import com.ashish.themefoods.webapi.service.mapper.AdvanceTransactionHistoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing AdvanceTransactionHistory.
 */
@Service
@Transactional
public class AdvanceTransactionHistoryService {

    private final Logger log = LoggerFactory.getLogger(AdvanceTransactionHistoryService.class);

    private final AdvanceTransactionHistoryRepository advanceTransactionHistoryRepository;

    private final AdvanceTransactionHistoryMapper advanceTransactionHistoryMapper;

    public AdvanceTransactionHistoryService(AdvanceTransactionHistoryRepository advanceTransactionHistoryRepository, AdvanceTransactionHistoryMapper advanceTransactionHistoryMapper) {
        this.advanceTransactionHistoryRepository = advanceTransactionHistoryRepository;
        this.advanceTransactionHistoryMapper = advanceTransactionHistoryMapper;
    }

    /**
     * Save a advanceTransactionHistory.
     *
     * @param advanceTransactionHistoryDTO the entity to save
     * @return the persisted entity
     */
    public AdvanceTransactionHistoryDTO save(AdvanceTransactionHistoryDTO advanceTransactionHistoryDTO) {
        log.debug("Request to save AdvanceTransactionHistory : {}", advanceTransactionHistoryDTO);
        AdvanceTransactionHistory advanceTransactionHistory = advanceTransactionHistoryMapper.toEntity(advanceTransactionHistoryDTO);
        advanceTransactionHistory = advanceTransactionHistoryRepository.save(advanceTransactionHistory);
        return advanceTransactionHistoryMapper.toDto(advanceTransactionHistory);
    }

    /**
     * Get all the advanceTransactionHistories.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AdvanceTransactionHistoryDTO> findAll() {
        log.debug("Request to get all AdvanceTransactionHistories");
        return advanceTransactionHistoryRepository.findAll().stream()
            .map(advanceTransactionHistoryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one advanceTransactionHistory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<AdvanceTransactionHistoryDTO> findOne(Long id) {
        log.debug("Request to get AdvanceTransactionHistory : {}", id);
        return advanceTransactionHistoryRepository.findById(id)
            .map(advanceTransactionHistoryMapper::toDto);
    }

    /**
     * Delete the advanceTransactionHistory by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AdvanceTransactionHistory : {}", id);
        advanceTransactionHistoryRepository.deleteById(id);
    }
}
