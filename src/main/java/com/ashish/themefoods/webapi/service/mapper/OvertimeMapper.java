package com.ashish.themefoods.webapi.service.mapper;

import com.ashish.themefoods.webapi.domain.*;
import com.ashish.themefoods.webapi.service.dto.OvertimeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Overtime and its DTO OvertimeDTO.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface OvertimeMapper extends EntityMapper<OvertimeDTO, Overtime> {

    @Mapping(source = "employee.id", target = "employeeId")
    OvertimeDTO toDto(Overtime overtime);

    @Mapping(source = "employeeId", target = "employee")
    Overtime toEntity(OvertimeDTO overtimeDTO);

    default Overtime fromId(Long id) {
        if (id == null) {
            return null;
        }
        Overtime overtime = new Overtime();
        overtime.setId(id);
        return overtime;
    }
}
