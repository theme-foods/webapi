package com.ashish.themefoods.webapi.service.dto;

public class GlobalResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GlobalResponse(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "GlobalResponse{" +
                "message='" + message + '\'' +
                '}';
    }
}
