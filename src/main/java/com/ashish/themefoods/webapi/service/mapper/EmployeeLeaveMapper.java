package com.ashish.themefoods.webapi.service.mapper;

import com.ashish.themefoods.webapi.domain.*;
import com.ashish.themefoods.webapi.service.dto.EmployeeLeaveDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity EmployeeLeave and its DTO EmployeeLeaveDTO.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface EmployeeLeaveMapper extends EntityMapper<EmployeeLeaveDTO, EmployeeLeave> {

    @Mapping(source = "employee.id", target = "employeeId")
    EmployeeLeaveDTO toDto(EmployeeLeave employeeLeave);

    @Mapping(source = "employeeId", target = "employee")
    EmployeeLeave toEntity(EmployeeLeaveDTO employeeLeaveDTO);

    default EmployeeLeave fromId(Long id) {
        if (id == null) {
            return null;
        }
        EmployeeLeave employeeLeave = new EmployeeLeave();
        employeeLeave.setId(id);
        return employeeLeave;
    }
}
