package com.ashish.themefoods.webapi.service;

import com.ashish.themefoods.webapi.domain.Employee;
import com.ashish.themefoods.webapi.domain.LeaveDashboard;
import com.ashish.themefoods.webapi.domain.LeaveDashboardHistory;
import com.ashish.themefoods.webapi.repository.LeaveDashboardHistoryRepository;
import com.ashish.themefoods.webapi.repository.LeaveDashboardRepository;
import com.ashish.themefoods.webapi.service.dto.LeaveDashboardDTO;
import com.ashish.themefoods.webapi.service.mapper.LeaveDashboardMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.ashish.themefoods.webapi.config.Constants.LEAVES_PER_MONTH;

/**
 * Service Implementation for managing LeaveDashboard.
 */
@Service
@Transactional
public class LeaveDashboardService {

    private final Logger log = LoggerFactory.getLogger(LeaveDashboardService.class);

    private final LeaveDashboardRepository leaveDashboardRepository;

    private final LeaveDashboardMapper leaveDashboardMapper;

    private final LeaveDashboardHistoryRepository leaveDashboardHistoryRepository;

    private final OvertimeService overtimeService;

    public LeaveDashboardService(LeaveDashboardRepository leaveDashboardRepository, LeaveDashboardMapper leaveDashboardMapper,
                                 LeaveDashboardHistoryRepository leaveDashboardHistoryRepository, OvertimeService overtimeService) {
        this.leaveDashboardRepository = leaveDashboardRepository;
        this.leaveDashboardMapper = leaveDashboardMapper;
        this.leaveDashboardHistoryRepository = leaveDashboardHistoryRepository;
        this.overtimeService = overtimeService;
    }

    /**
     * Save a leaveDashboard.
     *
     * @param leaveDashboardDTO the entity to save
     * @return the persisted entity
     */
    public LeaveDashboardDTO save(LeaveDashboardDTO leaveDashboardDTO) {
        log.debug("Request to save LeaveDashboard : {}", leaveDashboardDTO);
        LeaveDashboard leaveDashboard = leaveDashboardMapper.toEntity(leaveDashboardDTO);
        leaveDashboard = leaveDashboardRepository.save(leaveDashboard);
        return leaveDashboardMapper.toDto(leaveDashboard);
    }

    /**
     * Get all the leaveDashboards.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<LeaveDashboardDTO> findAll() {
        log.debug("Request to get all LeaveDashboards");
        return leaveDashboardRepository.findAll().stream()
            .map(leaveDashboardMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one leaveDashboard by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<LeaveDashboardDTO> findOne(Long id) {
        log.debug("Request to get LeaveDashboard : {}", id);
        return leaveDashboardRepository.findById(id)
            .map(leaveDashboardMapper::toDto);
    }

    /**
     * Delete the leaveDashboard by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete LeaveDashboard : {}", id);
        leaveDashboardRepository.deleteById(id);
    }

    // Run the job on 1st Jan every year to update leaves
    @Scheduled(cron = "0 0 0 1 1 ?")
    public void updateLeaveDashboardOnYearStarting() {
        log.debug("Updating leave dashboard on year starting ");
        List<LeaveDashboard> leaveDashboardList = leaveDashboardRepository.findAll();
        List<LeaveDashboardHistory> leaveDashboardHistoryList = new ArrayList<>();
        leaveDashboardList.forEach(leaveDashboard -> {
            LeaveDashboardHistory leaveDashboardHistory = new LeaveDashboardHistory();
            leaveDashboardHistory.setAvailableLeaves(leaveDashboard.getAvailableLeaves());
            leaveDashboardHistory.setTakenLeaves(leaveDashboard.getTakenLeaves());
            leaveDashboardHistory.setEmployee(leaveDashboard.getEmployee());
            leaveDashboardHistoryList.add(leaveDashboardHistory);
        });
        leaveDashboardHistoryRepository.saveAll(leaveDashboardHistoryList);
    }

    public void updateLeaveDashboardAfterJoining(Employee employee, Boolean checkRejoining) {
        if(employee.getLeaveDashboard() != null && employee.getLeaveDashboard().getId() != null) {
            log.debug("Not updating leave dashboard");
            employee.getLeaveDashboard().setEmployee(employee);
            leaveDashboardRepository.save(employee.getLeaveDashboard());
        } else {
            LeaveDashboard leaveDashboard = new LeaveDashboard();
            ZonedDateTime joiningDate = employee.getReJoiningDate() != null ? employee.getReJoiningDate() : employee.getJoiningDate();
            joiningDate = getDate(joiningDate);
            if(checkRejoining) {
                leaveDashboard = leaveDashboardRepository.findByEmployeeId(employee.getId());
            }
            if(employee.getLeaveDashboard() != null) {
                leaveDashboard = employee.getLeaveDashboard();
            }
            Integer noOfMonths = 0;
            if (joiningDate.getDayOfMonth() > 15) {
                noOfMonths = (12 - joiningDate.getMonthValue());
            } else {
                noOfMonths = (12 - joiningDate.getMonthValue()) + 1;
            }
            Double leavesAvailable = noOfMonths * LEAVES_PER_MONTH;
            leaveDashboard.setAvailableLeaves(leavesAvailable);
            leaveDashboard.setTakenLeaves(0D);
            leaveDashboard.setEmployee(employee);
            leaveDashboardRepository.save(leaveDashboard);
        }
    }


    public ZonedDateTime getDate(ZonedDateTime zonedDateTime) {
        ZoneId india = ZoneId.of("Asia/Kolkata");
        DateFormat dfIST = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dfIST.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat dfUtc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dfUtc.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = dfIST.parse(dfIST.format(dfUtc.parse(dfUtc.format(Date.from(zonedDateTime.toInstant())))));
            log.debug("convertedTime using sdf {}", dfIST.format(dfUtc.parse(dfUtc.format(Date.from(zonedDateTime.toInstant())))));
            log.debug("After conversion {}", ZonedDateTime.ofInstant(date.toInstant(), india));
            return ZonedDateTime.ofInstant(date.toInstant(), india);

        }catch (ParseException e) {
            e.printStackTrace();              // invalid input
        }
        return null;
    }
}
