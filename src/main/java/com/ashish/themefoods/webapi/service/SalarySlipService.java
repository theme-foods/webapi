package com.ashish.themefoods.webapi.service;

import com.ashish.themefoods.webapi.domain.*;
import com.ashish.themefoods.webapi.repository.*;
import com.ashish.themefoods.webapi.service.dto.*;
import com.ashish.themefoods.webapi.service.mapper.*;
import com.ashish.themefoods.webapi.web.rest.errors.ErrorConstants;
import com.ashish.themefoods.webapi.web.rest.errors.GlobalCustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.zalando.problem.Status;

import java.io.ByteArrayInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static com.ashish.themefoods.webapi.config.Constants.LEAVES_PER_MONTH;

/**
 * Service Implementation for managing SalarySlip.
 */
@Service
@Transactional
public class SalarySlipService {

    private final Logger log = LoggerFactory.getLogger(SalarySlipService.class);

    private final SalarySlipRepository salarySlipRepository;

    private final SalarySlipMapper salarySlipMapper;

    private final EmployeeLeaveRepository employeeLeaveRepository;

    private final EmployeeRepository employeeRepository;

    private final AdvanceTransactionHistoryRepository advanceTransactionHistoryRepository;

    private final AdvanceTransactionRepository advanceTransactionRepository;

    private final OvertimeRepository overtimeRepository;

    private final LeaveDashboardRepository leaveDashboardRepository;

    private final AdvanceTransactionHistoryMapper advanceTransactionHistoryMapper;

    private final OvertimeMapper overtimeMapper;

    private final AdvanceTransactionMapper advanceTransactionMapper;

    private final LeaveDashboardMapper leaveDashboardMapper;

    private final EmployeeMapper employeeMapper;

    private final GeneratePDFReport generatePDFReport;

    private final GenerateExcel generateExcel;

    public SalarySlipService(SalarySlipRepository salarySlipRepository, SalarySlipMapper salarySlipMapper, EmployeeLeaveRepository employeeLeaveRepository,
                             EmployeeRepository employeeRepository, AdvanceTransactionHistoryRepository advanceTransactionHistoryRepository,
                             AdvanceTransactionRepository advanceTransactionRepository, OvertimeRepository overtimeRepository, LeaveDashboardRepository leaveDashboardRepository,
                             AdvanceTransactionHistoryMapper advanceTransactionHistoryMapper, OvertimeMapper overtimeMapper, AdvanceTransactionMapper advanceTransactionMapper,
                             LeaveDashboardMapper leaveDashboardMapper, EmployeeMapper employeeMapper, GeneratePDFReport generatePDFReport, GenerateExcel generateExcel) {
        this.salarySlipRepository = salarySlipRepository;
        this.salarySlipMapper = salarySlipMapper;
        this.employeeLeaveRepository = employeeLeaveRepository;
        this.employeeRepository = employeeRepository;
        this.advanceTransactionHistoryRepository = advanceTransactionHistoryRepository;
        this.advanceTransactionRepository = advanceTransactionRepository;
        this.overtimeRepository = overtimeRepository;
        this.leaveDashboardRepository = leaveDashboardRepository;
        this.advanceTransactionHistoryMapper = advanceTransactionHistoryMapper;
        this.overtimeMapper = overtimeMapper;
        this.advanceTransactionMapper = advanceTransactionMapper;
        this.leaveDashboardMapper = leaveDashboardMapper;
        this.employeeMapper = employeeMapper;
        this.generatePDFReport = generatePDFReport;
        this.generateExcel = generateExcel;
    }

    /**
     * Save a salarySlip.
     *
     * @param salarySlipDTO the entity to save
     * @return the persisted entity
     */
    public SalarySlipDTO save(SalarySlipDTO salarySlipDTO) {
        log.debug("Request to save SalarySlip : {}", salarySlipDTO);
        SalarySlip salarySlip = salarySlipMapper.toEntity(salarySlipDTO);
        salarySlip = salarySlipRepository.save(salarySlip);
        return salarySlipMapper.toDto(salarySlip);
    }

    /**
     * Get all the salarySlips.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SalarySlipDTO> findAll() {
        log.debug("Request to get all SalarySlips");
        return salarySlipRepository.findAll().stream()
                .map(salarySlipMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one salarySlip by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<SalarySlipDTO> findOne(Long id) {
        log.debug("Request to get SalarySlip : {}", id);
        return salarySlipRepository.findById(id)
                .map(salarySlipMapper::toDto);
    }

    /**
     * Delete the salarySlip by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SalarySlip : {}", id);
        salarySlipRepository.deleteById(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void generateSalarySlip(Long employeeId, ZonedDateTime salaryDate, Long salaryId) {
        if (salaryId != null) {
            salarySlipRepository.deleteById(salaryId);
        }
        generateSalarySlipForEmployeeAndMonth(employeeId, salaryDate, true);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public SalarySlip generateSalarySlipForEmployeeAndMonth(Long employeeId, ZonedDateTime salaryDate, Boolean saveSalary) {
        log.debug("Generating salary slip for employee {} and for month {}", employeeId, salaryDate);
        Optional<Employee> employeeOptional = employeeRepository.findById(employeeId);
        if (employeeOptional.isPresent() && employeeOptional.get().getSalary() != null) {
            Employee employee = employeeOptional.get();
            SalarySlip salarySlip = new SalarySlip();
            salarySlip.setSalaryDate(getDate(salaryDate));
            int noOfDaysInMonth = YearMonth.of(salaryDate.getYear(), salaryDate.getMonthValue()).lengthOfMonth();
            log.debug("Number of days in month {}", noOfDaysInMonth);
            /* Leaves calculation starts */
            log.debug("Starting leave calculation");
            Double noOfLeaves = employeeLeaveRepository.totalLeavesInSalaryMonth(employeeId, salaryDate.getMonthValue());
            Double absents = employeeLeaveRepository.totalAbsentsInSalaryMonth(employeeId, salaryDate.getMonthValue());
            salarySlip.setAbsents(absents);
            noOfLeaves = noOfLeaves != null ? noOfLeaves : 0D;
            LeaveDashboard leaveDashboard = leaveDashboardRepository.findByEmployeeId(employeeId);
            log.debug("Leave Dashboard {}", leaveDashboard);

            log.debug("No of leaves {}", noOfLeaves);
            Double extraLeaves = 0D;
            Double leavesAvailableToTake = 0D;
            if (leaveDashboard != null && leaveDashboard.getAvailableLeaves() > 0) {
                leavesAvailableToTake = leaveDashboard.getAvailableLeaves();
//                Double totalExtraLeavesTaken = salarySlipRepository.totalExtraLeavesTakenByEmployeeInYear(employeeId, salaryDate.getYear());
//                log.debug("Extra leaves taken in the year {}", totalExtraLeavesTaken);
//                Double validLeavesTaken = 0D;
//                if (leaveDashboard != null && leaveDashboard.getTakenLeaves() != null && totalExtraLeavesTaken != null) {
//                    validLeavesTaken = leaveDashboard.getTakenLeaves() - totalExtraLeavesTaken;
//                }
//                ZonedDateTime joiningDate = getDate(employee.getReJoiningDate() != null ? employee.getReJoiningDate() : employee.getJoiningDate());
//                if (joiningDate.getYear() != salaryDate.getYear()) {
//                    leavesAvailableToTake = salaryDate.getMonthValue() * LEAVES_PER_MONTH - validLeavesTaken;
//                } else if (joiningDate.getMonthValue() > 1) {
//                    leavesAvailableToTake = (salaryDate.getMonthValue() - employee.getCreatedDate().atZone(ZoneId.systemDefault()).getMonthValue() + 1) * LEAVES_PER_MONTH - validLeavesTaken;
//                } else {
//                    // if employee has joined in january this year
//                    leavesAvailableToTake = salaryDate.getMonthValue() * LEAVES_PER_MONTH - validLeavesTaken;
//                }

                log.debug("Leaves available to take {} for employee {}", leavesAvailableToTake, employee.getFirstName());
//                extraLeaves = leavesAvailableToTake >= 0 && noOfLeaves > leavesAvailableToTake ? noOfLeaves - leavesAvailableToTake : noOfLeaves;
                if (leavesAvailableToTake >= noOfLeaves) {
                    extraLeaves = 0D;
                } else {
                    extraLeaves = noOfLeaves - leavesAvailableToTake;
                }
            } else {
                extraLeaves = noOfLeaves;
            }
            salarySlip.setExtraLeaves(extraLeaves);
            Double noOfDaysWorked = (saveSalary ? noOfDaysInMonth : (salaryDate.getDayOfMonth())) - extraLeaves - (absents != null ? absents : 0);
            salarySlip.setDays(noOfDaysWorked);
            if (saveSalary)
                leaveDashboardRepository.updateTakenLeaves(noOfLeaves, employeeId);
            /* Leaves calculation end*/

            /* Advance Calculation Starts */
            log.debug("Starting advance calculations");
            List<AdvanceTransactionProjection> advanceTransactionProjectionList = advanceTransactionRepository.findAllDetailsOfAdvancesByEmployeeId(employee.getId());
            log.debug("Advance transaction projection list {}", advanceTransactionProjectionList);
            if (!advanceTransactionProjectionList.isEmpty()) {
                AdvanceTransactionProjection advanceTransactionProjection = advanceTransactionProjectionList.get(0);

                if (advanceTransactionProjection != null && advanceTransactionProjection.getAmountRemaining() > 0) {
                    log.debug("Advance Transaction projection {}", advanceTransactionProjection);
                    Double advanceToBeDeducted = advanceTransactionProjection.getAmountRemaining() / advanceTransactionProjection.getTenure();
                    AdvanceTransactionHistory advanceTransactionHistory = new AdvanceTransactionHistory();
                    advanceTransactionHistory.setAmount(advanceToBeDeducted);
                    advanceTransactionHistory.setEmployee(employee);
                    advanceTransactionHistory.setTakenOn(ZonedDateTime.now());
                    advanceTransactionHistory.setTypeOfAdvance("Debit");
                    if (saveSalary)
                        advanceTransactionHistoryRepository.save(advanceTransactionHistory);
                    List<AdvanceTransaction> advanceTransactionList = advanceTransactionRepository.findAllByTypeOfAdvanceAndEmployeeId("Debit", employee.getId());
                    if (advanceTransactionList.isEmpty()) {
                        AdvanceTransaction advanceTransaction = new AdvanceTransaction();
                        advanceTransaction.setTenure(1);
                        advanceTransaction.setTypeOfAdvance("Debit");
                        advanceTransaction.setAmount(advanceToBeDeducted);
                        advanceTransaction.setEmployee(employee);
                        advanceTransaction.setTakenOn(ZonedDateTime.now());
                        if (saveSalary)
                            advanceTransactionRepository.save(advanceTransaction);
                    } else {
                        /* update the debit tenure by 1 */
                        AdvanceTransaction advanceTransaction = advanceTransactionList.get(0);
                        advanceTransaction.setAmount(advanceTransaction.getAmount() + advanceToBeDeducted);
                        advanceTransaction.setTenure(advanceTransaction.getTenure() + 1);
                        if (saveSalary)
                            advanceTransactionRepository.save(advanceTransaction);
                    }
                    salarySlip.setAdvance(advanceToBeDeducted);
                } else {
                    salarySlip.setAdvance(0D);
                    /* If remaining amount is 0, set the debit and credit tenure to zero.*/
                    List<AdvanceTransaction> advanceTransactionList = advanceTransactionRepository.findAllByTypeOfAdvanceAndEmployeeId("Credit", employee.getId());
                    if (!advanceTransactionList.isEmpty()) {
                        AdvanceTransaction advanceTransaction = advanceTransactionList.get(0);
                        List<AdvanceTransaction> advanceTransactionDebitList = advanceTransactionRepository.findAllByTypeOfAdvanceAndEmployeeId("Debit", employee.getId());
                        if (!advanceTransactionDebitList.isEmpty()) {
                            AdvanceTransaction advanceTransactionDebit = advanceTransactionDebitList.get(0);
                            advanceTransactionDebit.setTenure(advanceTransaction.getTenure());
                            if (saveSalary)
                                advanceTransactionRepository.save(advanceTransactionDebit);
                        }
                    }

                }
            } else {
                salarySlip.setAdvance(0D);
            }
            /* Advance Calculation Ends */

            /* Salary basket calculation starts*/
            log.debug("Starting salary basket calculations");
            // One day salary calculation
            Salary employeeSalary = employee.getSalary();
            Double oneDayBasicDA = employeeSalary.getBasicDA() != null ? employeeSalary.getBasicDA() / noOfDaysInMonth : 0;
            Double oneDayHRA = employeeSalary.getHra() != null ? employeeSalary.getHra() / noOfDaysInMonth : 0;
            Double oneDayPF = employeeSalary.getPf() != null ? employeeSalary.getPf() / noOfDaysInMonth : 0;
//            Double oneDayPT = employeeSalary.getPt() / 30;
//            Double oneDayESI = employeeSalary.getEsi() / 30;
            Double oneDayOtherAllowances = employeeSalary.getOtherAllowances() != null ? employeeSalary.getOtherAllowances() / noOfDaysInMonth : 0;
            Double oneDayTotalSalary = employeeSalary.getTotalSalary() != null ? employeeSalary.getTotalSalary() / noOfDaysInMonth : 0;
            Double hourlyTotalSalary = oneDayTotalSalary / 9;
            Double totalOvertimeHours = overtimeRepository.totalOvertimeHoursForEmployeeInMonth(employeeId, salaryDate.getYear(), salaryDate.getMonthValue());

            Double calculatedPT = 0.0D;
            Double generatedTotalSalary = oneDayTotalSalary * noOfDaysWorked;

            if (employeeSalary.getPt() != null && employeeSalary.getPt() > 0) {
                if (generatedTotalSalary >= 6000 && generatedTotalSalary < 8000) {
                    calculatedPT = 80D;
                } else if (generatedTotalSalary >= 8000 && generatedTotalSalary < 12000) {
                    calculatedPT = 150D;
                } else if (generatedTotalSalary >= 12000) {
                    calculatedPT = 200D;
                }
            }
            salarySlip.setPt(calculatedPT);
            Double calculatedESI = 0D;
            Double generatedSalaryForESICalculation = employeeSalary.getEsi() != null && employeeSalary.getEsi() > 0 ? (generatedTotalSalary + ((totalOvertimeHours != null ? totalOvertimeHours : 0) * hourlyTotalSalary)) : 0;
            if (employeeSalary.getEsi() != null && employeeSalary.getEsi() > 0 && generatedSalaryForESICalculation < 22000) {
                calculatedESI = 0.0175 * generatedSalaryForESICalculation;
            }
            salarySlip.setEsi(calculatedESI);
            salarySlip.setOverTimeHours(totalOvertimeHours);
            salarySlip.setBasicDA(noOfDaysWorked * oneDayBasicDA);
//            salarySlip.setEsi(oneDayESI * noOfDaysWorked);
            salarySlip.setHra(oneDayHRA * noOfDaysWorked);
            Double calculatedPf = employeeSalary.getPf() != null && employeeSalary.getPf() > 0 ? ((oneDayPF * noOfDaysWorked) > 1800 ? 1800 : (oneDayPF * noOfDaysWorked)) : 0;
            salarySlip.setPf(calculatedPf);
//                salarySlip.setPt(oneDayPT * noOfDaysWorked);
            salarySlip.setOtherAllowances(oneDayOtherAllowances * noOfDaysWorked);
            salarySlip.setPayout(oneDayTotalSalary * noOfDaysWorked
                    - calculatedPf - calculatedPT - calculatedESI);
            salarySlip.setActualPaid(salarySlip.getPayout() - salarySlip.getAdvance());
            salarySlip.setTotalSalary(employee.getSalary().getTotalSalary());
            log.debug("Ending salary basket calculations");
            /* Overtime calculation starts*/
            log.debug("Starting overtime calculations");
            if (totalOvertimeHours != null && totalOvertimeHours > 0) {
                Double totalOvertimeCharges = hourlyTotalSalary * totalOvertimeHours;
                salarySlip.setOvertime(totalOvertimeCharges);
                salarySlip.setPayout(salarySlip.getPayout() + totalOvertimeCharges);
                salarySlip.setActualPaid(salarySlip.getActualPaid() + totalOvertimeCharges);
            }

            /* Overtime calculation ends*/
//            }

            salarySlip.setEmployee(employee);
            if (saveSalary) {
                SalarySlip savedSalarySlip = salarySlipRepository.save(salarySlip);
            }
            log.debug("Saved salary slip is {}", salarySlip);
            return salarySlip;
        }
        /* Salary basket calculation ends*/
        return null;
    }

    public EmployeeDashboardDTO getEmployeeDashboard(Long employeeId) {
        EmployeeDashboardDTO employeeDashboardDTO = new EmployeeDashboardDTO();
        List<SalarySlip> salarySlipList = salarySlipRepository.findAllByEmployeeId(employeeId);
        employeeDashboardDTO.setSalarySlips(salarySlipMapper.toDto(salarySlipList));
        List<AdvanceTransactionHistory> advanceTransactionHistories = advanceTransactionHistoryRepository.findAllByEmployeeId(employeeId);
        employeeDashboardDTO.setAdvancesList(advanceTransactionHistoryMapper.toDto(advanceTransactionHistories));
        List<AdvanceTransactionProjection> advanceTransactionProjectionList = advanceTransactionRepository.findAllDetailsOfAdvancesByEmployeeId(employeeId);
        if (!advanceTransactionProjectionList.isEmpty()) {
            AdvanceTransactionDTO advanceTransactionDTO = new AdvanceTransactionDTO();
            advanceTransactionProjectionList.forEach(advanceTransactionProjection -> {
                advanceTransactionDTO.setAmountRemaining(advanceTransactionProjection.getAmountRemaining());
                advanceTransactionDTO.setAmount(advanceTransactionProjection.getAmount());
                advanceTransactionDTO.setTenure(advanceTransactionProjection.getTenure());
            });
            employeeDashboardDTO.setAdvanceTransaction(advanceTransactionDTO);
        }
        List<Overtime> overtimeList = overtimeRepository.findAllByEmployeeId(employeeId);
        employeeDashboardDTO.setOvertimeList(overtimeMapper.toDto(overtimeList));
        LeaveDashboard leaveDashboard = leaveDashboardRepository.findByEmployeeId(employeeId);
        employeeDashboardDTO.setLeaveDashboard(leaveDashboardMapper.toDto(leaveDashboard));
        Optional<Employee> employeeOptional = employeeRepository.findById(employeeId);
        if (employeeOptional.isPresent()) {
            employeeDashboardDTO.setEmployee(employeeMapper.toDto(employeeOptional.get()));
        }
        return employeeDashboardDTO;
    }

    public AdminDashboardDTO getAdminDashboard() {
        AdminDashboardDTO adminDashboardDTO = new AdminDashboardDTO();
        List<Employee> employeeList = employeeRepository.findAllByIsDeleted(false);
        adminDashboardDTO.setTotalEmployees(employeeList.size());
        adminDashboardDTO.setLePriveEmployees(employeeRepository.findAllByOffice("Le Prive").size());
        adminDashboardDTO.setOldSchoolEmployees(employeeRepository.findAllByOffice("Old School").size());
        adminDashboardDTO.setPrivateBanquetEmployees(employeeRepository.findAllByOffice("The Private Banquet").size());
        adminDashboardDTO.setTotalAdvanceCurrentMonth(advanceTransactionRepository.findTotalAdvanceForAMonth());
        adminDashboardDTO.setTotalAdvance(advanceTransactionRepository.findTotalAdvance());
        return adminDashboardDTO;
    }

    public ZonedDateTime getDate(ZonedDateTime zonedDateTime) {
        ZoneId india = ZoneId.of("Asia/Kolkata");
        DateFormat dfIST = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dfIST.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat dfUtc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dfUtc.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = dfIST.parse(dfIST.format(dfUtc.parse(dfUtc.format(Date.from(zonedDateTime.toInstant())))));
            log.debug("convertedTime using sdf {}", dfIST.format(dfUtc.parse(dfUtc.format(Date.from(zonedDateTime.toInstant())))));
            log.debug("After conversion {}", ZonedDateTime.ofInstant(date.toInstant(), india));
            return ZonedDateTime.ofInstant(date.toInstant(), india);

        } catch (ParseException e) {
            e.printStackTrace();              // invalid input
        }
        return null;
    }

    @Transactional
    public void updateAdvance(Long salaryId, Double advanceAmount) {
        log.debug("Inside service to update advance {} {}", salaryId, advanceAmount);
        Optional<SalarySlip> optionalSalarySlip = salarySlipRepository.findById(salaryId);
        optionalSalarySlip.ifPresent(salarySlip -> {
            Double previousAdvanceAmount = salarySlip.getAdvance();
            if (previousAdvanceAmount != advanceAmount) {
                Double differenceAmount = previousAdvanceAmount - advanceAmount;
                salarySlip.setAdvance(advanceAmount);
                salarySlip.setActualPaid(salarySlip.getActualPaid() + differenceAmount);
                salarySlipRepository.save(salarySlip);
                AdvanceTransactionHistory advanceTransactionHistory = advanceTransactionHistoryRepository.findTopByTypeOfAdvanceAndEmployeeIdOrderByLastModifiedDateDesc("DEBIT", salarySlip.getEmployee().getId());
                if (advanceTransactionHistory != null) {
                    advanceTransactionHistory.setAmount(advanceAmount);
                    advanceTransactionHistoryRepository.save(advanceTransactionHistory);
                }
                List<AdvanceTransaction> advanceTransactionList = advanceTransactionRepository.findAllByTypeOfAdvanceAndEmployeeId("DEBIT", salarySlip.getEmployee().getId());
                if (!advanceTransactionList.isEmpty()) {
                    advanceTransactionList.forEach(advanceTransaction -> {
                        advanceTransaction.setAmount(advanceTransaction.getAmount() - differenceAmount);
                        advanceTransactionRepository.save(advanceTransaction);
                    });
                }
            }
        });
    }

    public SalarySlip getSalaryBetweenDates(Long employeeId, ZonedDateTime fromDate, ZonedDateTime toDate, Boolean settleAdvance) {
        log.debug("Generating salary slip for employee {} and from  {} to {}", employeeId, fromDate, toDate);
        Optional<Employee> employeeOptional = employeeRepository.findById(employeeId);
        if (employeeOptional.isPresent() && employeeOptional.get().getSalary() != null) {
            Employee employee = employeeOptional.get();
            SalarySlip salarySlip = new SalarySlip();
//            salarySlip.setSalaryDate(salaryDate);
            /* Leaves calculation starts */
            log.debug("Starting leave calculation");
            int noOfDaysToDivide = 30;
            if (getDate(fromDate).getMonthValue() == getDate(fromDate).getMonthValue()) {
                noOfDaysToDivide = YearMonth.of(getDate(fromDate).getYear(), getDate(fromDate).getMonthValue()).lengthOfMonth();
            }
            Double noOfLeaves = employeeLeaveRepository.totalLeavesBetweenDates(employeeId, fromDate, toDate);
            Double absents = employeeLeaveRepository.totalAbsentsBetweenDates(employeeId, fromDate, toDate);
            salarySlip.setAbsents(absents);
            noOfLeaves = noOfLeaves != null ? noOfLeaves : 0D;
            LeaveDashboard leaveDashboard = leaveDashboardRepository.findByEmployeeId(employeeId);
            log.debug("Leave Dashboard {}", leaveDashboard);

            log.debug("No of leaves {}", noOfLeaves);
            Double extraLeaves = 0D;
            Double leavesAvailableToTake = 0D;
            if (leaveDashboard != null && leaveDashboard.getAvailableLeaves() > 0) {
                  leavesAvailableToTake = leaveDashboard.getAvailableLeaves();
//                Double totalExtraLeavesTaken = salarySlipRepository.totalExtraLeavesTakenByEmployeeInYear(employeeId, fromDate.getYear());
//                log.debug("Extra leaves taken in the year {}", totalExtraLeavesTaken);
//                Double validLeavesTaken = 0D;
//                if (leaveDashboard != null && leaveDashboard.getTakenLeaves() != null && totalExtraLeavesTaken != null) {
//                    validLeavesTaken = leaveDashboard.getTakenLeaves() - totalExtraLeavesTaken;
//                }
//                ZonedDateTime joiningDate = getDate(employee.getReJoiningDate() != null ? employee.getReJoiningDate() : employee.getJoiningDate());
//                if (joiningDate.getYear() != fromDate.getYear()) {
//                    leavesAvailableToTake = fromDate.getMonthValue() * LEAVES_PER_MONTH - validLeavesTaken;
//                } else if (joiningDate.getMonthValue() > 1) {
//                    leavesAvailableToTake = (fromDate.getMonthValue() - employee.getCreatedDate().atZone(ZoneId.systemDefault()).getMonthValue() + 1) * LEAVES_PER_MONTH - validLeavesTaken;
//                } else {
//                    // if employee has joined in january this year
//                    leavesAvailableToTake = fromDate.getMonthValue() * LEAVES_PER_MONTH - validLeavesTaken;
//                }
//
//                log.debug("Leaves available to take {} for employee {}", leavesAvailableToTake, employee.getFirstName());
//                extraLeaves = leavesAvailableToTake >= 0 && noOfLeaves > leavesAvailableToTake ? noOfLeaves - leavesAvailableToTake : 0;
                if (leavesAvailableToTake >= noOfLeaves) {
                    extraLeaves = 0D;
                } else {
                    extraLeaves = noOfLeaves - leavesAvailableToTake;
                }
            } else {
                extraLeaves = noOfLeaves;
            }
            salarySlip.setExtraLeaves(extraLeaves);
            Double noOfDaysWorked = 0D;
            if (ChronoUnit.DAYS.between(fromDate, toDate) == 0) {
                noOfDaysWorked = 1D;
            } else if (ChronoUnit.DAYS.between(fromDate, toDate) > 0) {
                noOfDaysWorked = ChronoUnit.DAYS.between(fromDate, toDate) + 1 - extraLeaves - (absents != null ? absents : 0);
            }
            salarySlip.setDays(noOfDaysWorked);
            /* Leaves calculation end*/

            /* Advance Calculation Starts */
            log.debug("Starting advance calculations");
            List<AdvanceTransactionProjection> advanceTransactionProjectionList = advanceTransactionRepository.findAllDetailsOfAdvancesByEmployeeId(employee.getId());
            log.debug("Advance transaction projection list {}", advanceTransactionProjectionList);
            if (!advanceTransactionProjectionList.isEmpty()) {
                AdvanceTransactionProjection advanceTransactionProjection = advanceTransactionProjectionList.get(0);

                if (advanceTransactionProjection != null && advanceTransactionProjection.getAmountRemaining() > 0) {
                    log.debug("Advance Transaction projection {}", advanceTransactionProjection);
                    Double advanceToBeDeducted = settleAdvance ? advanceTransactionProjection.getAmountRemaining() : (advanceTransactionProjection.getAmountRemaining() / advanceTransactionProjection.getTenure());
                    AdvanceTransactionHistory advanceTransactionHistory = new AdvanceTransactionHistory();
                    advanceTransactionHistory.setAmount(advanceToBeDeducted);
                    advanceTransactionHistory.setEmployee(employee);
                    advanceTransactionHistory.setTakenOn(ZonedDateTime.now());
                    advanceTransactionHistory.setTypeOfAdvance("Debit");
                    List<AdvanceTransaction> advanceTransactionList = advanceTransactionRepository.findAllByTypeOfAdvanceAndEmployeeId("Debit", employee.getId());
                    if (advanceTransactionList.isEmpty()) {
                        AdvanceTransaction advanceTransaction = new AdvanceTransaction();
                        advanceTransaction.setTenure(1);
                        advanceTransaction.setTypeOfAdvance("Debit");
                        advanceTransaction.setAmount(advanceToBeDeducted);
                        advanceTransaction.setEmployee(employee);
                        advanceTransaction.setTakenOn(ZonedDateTime.now());
                    } else {
                        /* update the debit tenure by 1 */
                        AdvanceTransaction advanceTransaction = advanceTransactionList.get(0);
                        advanceTransaction.setAmount(advanceTransaction.getAmount() + advanceToBeDeducted);
                        advanceTransaction.setTenure(advanceTransaction.getTenure() + 1);
                    }
                    salarySlip.setAdvance(advanceToBeDeducted);
                } else {
                    salarySlip.setAdvance(0D);
                    /* If remaining amount is 0, set the debit and credit tenure to zero.*/
                    List<AdvanceTransaction> advanceTransactionList = advanceTransactionRepository.findAllByTypeOfAdvanceAndEmployeeId("Credit", employee.getId());
                    if (!advanceTransactionList.isEmpty()) {
                        AdvanceTransaction advanceTransaction = advanceTransactionList.get(0);
                        List<AdvanceTransaction> advanceTransactionDebitList = advanceTransactionRepository.findAllByTypeOfAdvanceAndEmployeeId("Debit", employee.getId());
                        if (!advanceTransactionDebitList.isEmpty()) {
                            AdvanceTransaction advanceTransactionDebit = advanceTransactionDebitList.get(0);
                            advanceTransactionDebit.setTenure(advanceTransaction.getTenure());
                        }
                    }

                }
            } else {
                salarySlip.setAdvance(0D);
            }
            /* Advance Calculation Ends */

            /* Salary basket calculation starts*/
            log.debug("Starting salary basket calculations");
            // One day salary calculation
            Salary employeeSalary = employee.getSalary();
//            Double daysToDivide = 30D;
            if (noOfDaysWorked.intValue() > 31) {
                noOfDaysToDivide = noOfDaysWorked.intValue();
            }
            Double oneDayBasicDA = employeeSalary.getBasicDA() != null ? employeeSalary.getBasicDA() / noOfDaysToDivide : 0;
            Double oneDayHRA = employeeSalary.getHra() != null ? employeeSalary.getHra() / noOfDaysToDivide : 0;
            Double oneDayPF = employeeSalary.getPf() != null ? employeeSalary.getPf() / noOfDaysToDivide : 0;
//            Double oneDayPT = employeeSalary.getPt() / 30;
//            Double oneDayESI = employeeSalary.getEsi() / 30;
            Double oneDayOtherAllowances = employeeSalary.getOtherAllowances() != null ? employeeSalary.getOtherAllowances() / noOfDaysToDivide : 0;
            Double oneDayTotalSalary = employeeSalary.getTotalSalary() != null ? employeeSalary.getTotalSalary() / noOfDaysToDivide : 0;
            Double totalOvertimeHours = overtimeRepository.totalOvertimeHoursForEmployeeBetweenDates(employeeId, fromDate, toDate);
            Double hourlyTotalSalary = oneDayTotalSalary / 9;
            salarySlip.setOverTimeHours(totalOvertimeHours);
            salarySlip.setBasicDA(noOfDaysWorked * oneDayBasicDA);
            salarySlip.setHra(oneDayHRA * noOfDaysWorked);
            Double calculatedPf = employeeSalary.getPf() != null && employeeSalary.getPf() > 0 ? ((oneDayPF * noOfDaysWorked) > 1800 ? 1800 : (oneDayPF * noOfDaysWorked)) : 0;
            salarySlip.setPf(calculatedPf);
            Double calculatedPT = 0D;
            Double generatedTotalSalary = oneDayTotalSalary * noOfDaysWorked;
            if (employeeSalary.getPt() != null && employeeSalary.getPt() > 0) {
                if (generatedTotalSalary >= 6000 && generatedTotalSalary < 8000) {
                    calculatedPT = 80D;
                } else if (generatedTotalSalary >= 8000 && generatedTotalSalary < 12000) {
                    calculatedPT = 150D;
                } else if (generatedTotalSalary >= 12000) {
                    calculatedPT = 200D;
                }
            }
            salarySlip.setPt(calculatedPT);
            Double calculatedESI = 0D;
            Double generatedSalaryForESICalculation = employeeSalary.getEsi() != null && employeeSalary.getEsi() > 0 ? (generatedTotalSalary + ((totalOvertimeHours != null ? totalOvertimeHours : 0) * hourlyTotalSalary)) : 0;
            if (employeeSalary.getEsi() != null && employeeSalary.getEsi() > 0 && generatedSalaryForESICalculation < 22000) {
                calculatedESI = 0.0175 * generatedSalaryForESICalculation;
            }
            salarySlip.setEsi(calculatedESI);
            salarySlip.setOtherAllowances(oneDayOtherAllowances * noOfDaysWorked);
            salarySlip.setPayout(oneDayTotalSalary * noOfDaysWorked
                    - calculatedPf - calculatedPT - calculatedESI);
            salarySlip.setActualPaid(salarySlip.getPayout() - salarySlip.getAdvance());
            salarySlip.setTotalSalary(employee.getSalary().getTotalSalary());
            log.debug("Ending salary basket calculations");
            /* Overtime calculation starts*/
            log.debug("Starting overtime calculations");
            if (totalOvertimeHours != null && totalOvertimeHours > 0) {
                Double totalOvertimeCharges = hourlyTotalSalary * totalOvertimeHours;
                salarySlip.setOvertime(totalOvertimeCharges);
                salarySlip.setPayout(salarySlip.getPayout() + totalOvertimeCharges);
                salarySlip.setActualPaid(salarySlip.getActualPaid() + totalOvertimeCharges);
            }

            /* Overtime calculation ends*/
//            }

            salarySlip.setEmployee(employee);
            log.debug("Saved salary slip is {}", salarySlip);
            return salarySlip;
        }
        /* Salary basket calculation ends*/
        return null;
    }

    public ByteArrayInputStream getSalarySlip(Long employeeId, Integer month, Integer year) {
        SalarySlip salarySlip = salarySlipRepository.findByEmployeeIdAndMonth(employeeId, month, year);
        return generatePDFReport.salarySlips(salarySlip);
    }

    public ByteArrayInputStream getSalarySlip(Long employeeId, ZonedDateTime fromDate, ZonedDateTime toDate, Boolean settleAdvance) {
        SalarySlip salarySlip = getSalaryBetweenDates(employeeId, getDate(fromDate), getDate(toDate), settleAdvance);
        return generatePDFReport.salarySlips(salarySlip, fromDate, toDate);
    }

    public ByteArrayInputStream getSalarySlip(Integer month, Integer year, Boolean isExcel) {
        log.debug("Generating slips");
        List<SalarySlip> salarySlipList = salarySlipRepository.findByMonth(month, year);
        if (salarySlipList.isEmpty()) {
            throw new GlobalCustomException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "No Salary Slips available", Status.BAD_REQUEST);
        }
        if (isExcel) {
            return generateExcel.salaryInExcel(salarySlipList);
        }
        return generatePDFReport.salarySlips(salarySlipList);
    }
}
