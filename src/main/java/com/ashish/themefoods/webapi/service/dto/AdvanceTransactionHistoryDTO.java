package com.ashish.themefoods.webapi.service.dto;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AdvanceTransactionHistory entity.
 */
public class AdvanceTransactionHistoryDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private ZonedDateTime takenOn;

    private Double amount;

    private String typeOfAdvance;

    private Integer tenure;

    private String reason;


    private Long employeeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getTakenOn() {
        return takenOn;
    }

    public void setTakenOn(ZonedDateTime takenOn) {
        this.takenOn = takenOn;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTypeOfAdvance() {
        return typeOfAdvance;
    }

    public void setTypeOfAdvance(String typeOfAdvance) {
        this.typeOfAdvance = typeOfAdvance;
    }

    public Integer getTenure() {
        return tenure;
    }

    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdvanceTransactionHistoryDTO advanceTransactionHistoryDTO = (AdvanceTransactionHistoryDTO) o;
        if (advanceTransactionHistoryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), advanceTransactionHistoryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdvanceTransactionHistoryDTO{" +
            "id=" + getId() +
            ", takenOn='" + getTakenOn() + "'" +
            ", amount=" + getAmount() +
            ", typeOfAdvance='" + getTypeOfAdvance() + "'" +
            ", tenure=" + getTenure() +
            ", reason='" + getReason() + "'" +
            ", employee=" + getEmployeeId() +
            "}";
    }
}
