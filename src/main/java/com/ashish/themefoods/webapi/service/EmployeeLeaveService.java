package com.ashish.themefoods.webapi.service;

import com.ashish.themefoods.webapi.domain.EmployeeLeave;
import com.ashish.themefoods.webapi.domain.SalarySlip;
import com.ashish.themefoods.webapi.repository.EmployeeLeaveRepository;
import com.ashish.themefoods.webapi.repository.SalarySlipRepository;
import com.ashish.themefoods.webapi.service.dto.EmployeeLeaveDTO;
import com.ashish.themefoods.webapi.service.mapper.EmployeeLeaveMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing EmployeeLeave.
 */
@Service
@Transactional
public class EmployeeLeaveService {

    private final Logger log = LoggerFactory.getLogger(EmployeeLeaveService.class);

    private final EmployeeLeaveRepository employeeLeaveRepository;

    private final EmployeeLeaveMapper employeeLeaveMapper;

    private final SalarySlipRepository salarySlipRepository;

    private final SalarySlipService salarySlipService;

    public EmployeeLeaveService(EmployeeLeaveRepository employeeLeaveRepository, EmployeeLeaveMapper employeeLeaveMapper, SalarySlipRepository salarySlipRepository, SalarySlipService salarySlipService) {
        this.employeeLeaveRepository = employeeLeaveRepository;
        this.employeeLeaveMapper = employeeLeaveMapper;
        this.salarySlipRepository = salarySlipRepository;
        this.salarySlipService = salarySlipService;
    }

    /**
     * Save a employeeLeave.
     *
     * @param employeeLeaveDTO the entity to save
     * @return the persisted entity
     */
    public EmployeeLeaveDTO save(EmployeeLeaveDTO employeeLeaveDTO) {
        log.debug("Request to save EmployeeLeave : {}", employeeLeaveDTO);
        EmployeeLeave employeeLeave = employeeLeaveMapper.toEntity(employeeLeaveDTO);
        employeeLeave = employeeLeaveRepository.save(employeeLeave);
        checkLeavesAndSalaries(employeeLeave);
        return employeeLeaveMapper.toDto(employeeLeave);
    }
    
    public EmployeeLeaveDTO saveLeave(EmployeeLeaveDTO employeeLeaveDTO) {
        log.debug("Saving leave {}", employeeLeaveDTO);
        if(employeeLeaveDTO.getToDate() != null && getDate(employeeLeaveDTO.getFromDate()).getMonthValue() != getDate(employeeLeaveDTO.getToDate()).getMonthValue()) {
            LocalDate currentLocalFromDate = getDate(employeeLeaveDTO.getFromDate()).toLocalDate();
            LocalDate fromEndDate = currentLocalFromDate.withDayOfMonth(currentLocalFromDate.lengthOfMonth());
            Long noOfLeaves = ChronoUnit.DAYS.between(getDate(employeeLeaveDTO.getFromDate()), fromEndDate.atStartOfDay(ZoneId.systemDefault())) + 1;
            EmployeeLeave employeeLeave1 = employeeLeaveMapper.toEntity(employeeLeaveDTO);
            employeeLeave1.setToDate(fromEndDate.atStartOfDay(ZoneId.systemDefault()));
            employeeLeave1.setNoOfDays(Float.valueOf(noOfLeaves));
            save(employeeLeaveMapper.toDto(employeeLeave1));

            LocalDate currentLocalToDate = getDate(employeeLeaveDTO.getToDate()).toLocalDate();
            LocalDate toStartingDate = currentLocalToDate.withDayOfMonth(1);
            Float noOfLeavesInNextMonth = employeeLeaveDTO.getNoOfDays() - noOfLeaves;
            EmployeeLeave employeeLeave2 = employeeLeaveMapper.toEntity(employeeLeaveDTO);
            employeeLeave2.setFromDate(toStartingDate.atStartOfDay(ZoneId.systemDefault()));
            employeeLeave2.setNoOfDays(noOfLeavesInNextMonth);
            save(employeeLeaveMapper.toDto(employeeLeave2));
        } else {
            employeeLeaveDTO = save(employeeLeaveDTO);
        }
        return employeeLeaveDTO;
    }

    /**
     * Get all the employeeLeaves.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<EmployeeLeaveDTO> findAll(String filterFromDate, String filterToDate) {
        log.debug("Request to get all EmployeeLeaves");
        if(Objects.nonNull(filterFromDate) && Objects.isNull(filterToDate)) {
            return employeeLeaveRepository.findByFromDateGreaterThanEqualOrderByFromDateDesc(ZonedDateTime.parse(filterFromDate)).stream()
                    .map(employeeLeaveMapper::toDto)
                    .collect(Collectors.toCollection(LinkedList::new));
        }
        if(Objects.nonNull(filterToDate) && Objects.isNull(filterFromDate)) {
            return employeeLeaveRepository.findByToDateLessThanEqualOrderByToDateDesc(ZonedDateTime.parse(filterToDate)).stream()
                    .map(employeeLeaveMapper::toDto)
                    .collect(Collectors.toCollection(LinkedList::new));
        }
        if(Objects.nonNull(filterFromDate) && Objects.nonNull(filterToDate)) {
            return employeeLeaveRepository.findByFromDateGreaterThanEqualAndToDateLessThanEqualOrderByFromDateDesc(ZonedDateTime.parse(filterFromDate), ZonedDateTime.parse(filterToDate)).stream()
                    .map(employeeLeaveMapper::toDto)
                    .collect(Collectors.toCollection(LinkedList::new));
        }
        return employeeLeaveRepository.findAllByOrderByFromDateDesc().stream()
            .map(employeeLeaveMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one employeeLeave by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<EmployeeLeaveDTO> findOne(Long id) {
        log.debug("Request to get EmployeeLeave : {}", id);
        return employeeLeaveRepository.findById(id)
            .map(employeeLeaveMapper::toDto);
    }

    /**
     * Delete the employeeLeave by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete EmployeeLeave : {}", id);
        Optional<EmployeeLeave> employeeLeave = employeeLeaveRepository.findById(id);
        employeeLeaveRepository.deleteById(id);
        checkLeavesAndSalaries(employeeLeave.get());
    }

    public void checkLeavesAndSalaries(EmployeeLeave employeeLeave) {
        List<SalarySlip> salarySlipList = new ArrayList<>();

        if(employeeLeave.getToDate() == null) {
            salarySlipList = salarySlipRepository.findBySalaryDateBetweenAndEmployeeId(getDate(employeeLeave.getFromDate()).getMonthValue(), employeeLeave.getEmployee().getId(),
                    getDate(employeeLeave.getFromDate()).getYear());
        } else if(employeeLeave.getToDate() != null && employeeLeave.getFromDate() != null) {
            salarySlipList = salarySlipRepository.findBySalaryDateBetweenAndEmployeeId(getDate(employeeLeave.getFromDate()).getMonthValue(), getDate(employeeLeave.getToDate()).getMonthValue(),
                    employeeLeave.getEmployee().getId(), getDate(employeeLeave.getFromDate()).getYear());
        }
        log.debug("Salary found after adding leave {}", salarySlipList);
        if(!salarySlipList.isEmpty()) {
            salarySlipList.forEach(salarySlip -> salarySlipService.generateSalarySlip(employeeLeave.getEmployee().getId(), salarySlip.getSalaryDate(), salarySlip.getId()));
        }
    }

    public ZonedDateTime getDate(ZonedDateTime zonedDateTime) {
        ZoneId india = ZoneId.of("Asia/Kolkata");
        DateFormat dfIST = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dfIST.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat dfUtc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dfUtc.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = dfIST.parse(dfIST.format(dfUtc.parse(dfUtc.format(Date.from(zonedDateTime.toInstant())))));
            log.debug("After conversion {}", ZonedDateTime.ofInstant(date.toInstant(), india));
            return ZonedDateTime.ofInstant(date.toInstant(), india);

        }catch (ParseException e) {
            e.printStackTrace();              // invalid input
        }
        return null;
    }
}
