package com.ashish.themefoods.webapi.service;

import com.ashish.themefoods.webapi.domain.SalarySlip;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
@Transactional
public class GeneratePDFReport {

    public static ByteArrayInputStream salarySlips(SalarySlip salarySlip) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {

            Paragraph para;
            Document document = new Document();
            PdfWriter.getInstance(document, out);

            document.open();
            Font font1 = new Font(Font.FontFamily.COURIER, 10, Font.BOLD);


            para = new Paragraph("Theme Foods, Vadodara", font1);
            para.setAlignment(Element.ALIGN_CENTER);
            document.add(para);

            Date date = Date.from(salarySlip.getSalaryDate().toInstant());
            String month = new SimpleDateFormat("MMMM").format(date);

            para = new Paragraph("Pay-slip for the month of " + month + " " + salarySlip.getSalaryDate().getYear(), font1);
            para.setAlignment(Element.ALIGN_CENTER);
            document.add(para);
            document.add(Chunk.NEWLINE);
            PdfPTable table = new PdfPTable(2); // Code 1
            table.getDefaultCell().setBorder(0);
            // Code 2

            table.addCell(new Phrase("Name of Employee  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(salarySlip.getEmployee().getFirstName() + " " + (Objects.nonNull(salarySlip.getEmployee().getLastName()) ? salarySlip.getEmployee().getLastName() : ""), FontFactory.getFont(FontFactory.COURIER, 10)));
            // Code 3
            table.addCell(new Phrase("Office  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getOffice()) ? salarySlip.getEmployee().getOffice() : "", FontFactory.getFont(FontFactory.COURIER, 10)));

            table.addCell(new Phrase("Designation  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getDesignation()) ? salarySlip.getEmployee().getDesignation() : "", FontFactory.getFont(FontFactory.COURIER, 10)));

            table.addCell(new Phrase("Bank Details  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase((Objects.nonNull(salarySlip.getEmployee().getBankName()) ? (salarySlip.getEmployee().getBankName() + " | ")  : "") + (Objects.nonNull(salarySlip.getEmployee().getBankIFSC()) ? (salarySlip.getEmployee().getBankIFSC() + " | ")  : "") + (Objects.nonNull(salarySlip.getEmployee().getBankAcNumber()) ? (salarySlip.getEmployee().getBankAcNumber() )  : ""), FontFactory.getFont(FontFactory.COURIER, 10)));

            table.addCell(new Phrase("UAN  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getUanNumber()) ? salarySlip.getEmployee().getUanNumber()  : "", FontFactory.getFont(FontFactory.COURIER, 10)));

            table.addCell(new Phrase("ESI Number  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getEsiNumber()) ? salarySlip.getEmployee().getEsiNumber() : "", FontFactory.getFont(FontFactory.COURIER, 10)));

            table.addCell(new Phrase("Days Worked  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(Double.toString(salarySlip.getDays()), FontFactory.getFont(FontFactory.COURIER, 10)));

            // Code 5

            document.add(table);

            document.add(Chunk.NEWLINE);

            Double totalSalary = salarySlip.getEmployee().getOffice().equalsIgnoreCase("The Private Banquet") ? salarySlip.getTotalSalary() : ((salarySlip.getBasicDA() != null ? salarySlip.getBasicDA() : 0) + (salarySlip.getOvertime() != null ? salarySlip.getOvertime() : 0)
                    + (salarySlip.getHra() != null ? salarySlip.getHra() : 0) + (salarySlip.getOtherAllowances() != null ? salarySlip.getOtherAllowances() : 0));
            Double totalDeductions = (salarySlip.getEsi() != null ? salarySlip.getEsi() : 0) + (salarySlip.getPf() != null ? salarySlip.getPf() : 0)
                    + (salarySlip.getPt() != null ? salarySlip.getPt() : 0) + (salarySlip.getAdvance() != null ? salarySlip.getAdvance() : 0);

            PdfPTable table1 = new PdfPTable(4); // 3 columns.

            table1.getDefaultCell().setBorderWidth(1);

            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase("INCOME", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
            table1.addCell(new Phrase("AMOUNT ", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
            table1.addCell(new Phrase("DEDUCTIONS", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
            table1.addCell(new Phrase("AMOUNT", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));

            if(salarySlip.getBasicDA() != null && salarySlip.getBasicDA() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Basic & DA", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getBasicDA())), FontFactory.getFont(FontFactory.COURIER, 10)));
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }
            if(salarySlip.getPt() != null && salarySlip.getPt() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("PT ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getPt())), FontFactory.getFont(FontFactory.COURIER, 10)));
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }


            if(salarySlip.getHra() != null && salarySlip.getHra() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("HRA ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getHra())), FontFactory.getFont(FontFactory.COURIER, 10)));
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }

            if(salarySlip.getPf() != null && salarySlip.getPf() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("PF ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getPf())), FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }


            if(salarySlip.getOtherAllowances() != null && salarySlip.getOtherAllowances() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Other Allowances ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getOtherAllowances())), FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }

            if(salarySlip.getEsi() != null && salarySlip.getEsi() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("ESI ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getEsi())), FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }

            if(salarySlip.getOvertime()!=null && salarySlip.getOvertime() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Overtime ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getOvertime())), FontFactory.getFont(FontFactory.COURIER, 10)));
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }

            if(salarySlip.getAdvance() != null && salarySlip.getAdvance() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Advance ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getAdvance())), FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }


            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));

            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase("", FontFactory.getFont(FontFactory.COURIER, 10)));

            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase("Total Deductions ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(Double.toString(Math.round(totalDeductions)), FontFactory.getFont(FontFactory.COURIER, 10)));

            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase( " ", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase("Total Payout ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getPayout())), FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));

            table1.addCell("  ");
            table1.addCell("  ");
            table1.addCell("  ");
            table1.addCell("  ");

            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase("Total Salary  ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(Double.toString(Math.round(totalSalary)), FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase("Total Paid ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getActualPaid())), FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));

            document.add(table1);
            document.add(Chunk.NEWLINE);
            document.close();
            System.out.println("Pdf generated normally");


        } catch (Exception e) {
            Logger.getLogger(GeneratePDFReport.class.getName()).log(Level.SEVERE, null, e);
        }
        return new ByteArrayInputStream(out.toByteArray());
    }

    public static ByteArrayInputStream salarySlips(List<SalarySlip> salarySlipList) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, out);
            document.open();
            for(int i = 0; i< salarySlipList.size(); i++) {
                SalarySlip salarySlip = salarySlipList.get(i);
                Paragraph para;
                Font font1 = new Font(Font.FontFamily.COURIER, 10, Font.BOLD);

                para = new Paragraph("Theme Foods, Vadodara", font1);
                para.setAlignment(Element.ALIGN_CENTER);
                document.add(para);
                Date date = Date.from(salarySlip.getSalaryDate().toInstant());
                String month = new SimpleDateFormat("MMMM").format(date);

                para = new Paragraph("Pay-slip for the month of " + month + " " + salarySlip.getSalaryDate().getYear(), font1);
                para.setAlignment(Element.ALIGN_CENTER);
                document.add(para);
                document.add(Chunk.NEWLINE);
                PdfPTable table = new PdfPTable(2); // Code 1
                table.getDefaultCell().setBorder(0);
                // Code 2

                table.addCell(new Phrase("Name of Employee  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//                table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
                table.addCell(new Phrase(salarySlip.getEmployee().getFirstName() + " " + (Objects.nonNull(salarySlip.getEmployee().getLastName()) ? salarySlip.getEmployee().getLastName() : ""), FontFactory.getFont(FontFactory.COURIER, 10)));

                table.addCell(new Phrase("Office  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//                table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
                table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getOffice()) ? salarySlip.getEmployee().getOffice() : "", FontFactory.getFont(FontFactory.COURIER, 10)));

                // Code 3
                table.addCell(new Phrase("Designation  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//                table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
                table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getDesignation()) ? salarySlip.getEmployee().getDesignation() : "", FontFactory.getFont(FontFactory.COURIER, 10)));

                table.addCell(new Phrase("Bank Details  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//                table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
                table.addCell(new Phrase((Objects.nonNull(salarySlip.getEmployee().getBankName()) ? (salarySlip.getEmployee().getBankName() + " | ")  : "") + (Objects.nonNull(salarySlip.getEmployee().getBankIFSC()) ? (salarySlip.getEmployee().getBankIFSC() + " | ")  : "") + (Objects.nonNull(salarySlip.getEmployee().getBankAcNumber()) ? (salarySlip.getEmployee().getBankAcNumber() )  : ""), FontFactory.getFont(FontFactory.COURIER, 10)));

                table.addCell(new Phrase("UAN  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//                table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
                table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getUanNumber()) ? salarySlip.getEmployee().getUanNumber()  : "", FontFactory.getFont(FontFactory.COURIER, 10)));

                table.addCell(new Phrase("ESI Number  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//                table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
                table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getEsiNumber()) ? salarySlip.getEmployee().getEsiNumber() : "", FontFactory.getFont(FontFactory.COURIER, 10)));

                table.addCell(new Phrase("Days Worked  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//                table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
                table.addCell(new Phrase(Double.toString(salarySlip.getDays()), FontFactory.getFont(FontFactory.COURIER, 10)));

                // Code 5

                document.add(table);

                document.add(Chunk.NEWLINE);

                Double totalSalary = salarySlip.getEmployee().getOffice().equalsIgnoreCase("The Private Banquet") ? salarySlip.getTotalSalary() : ((salarySlip.getBasicDA() != null ? salarySlip.getBasicDA() : 0) + (salarySlip.getOvertime() != null ? salarySlip.getOvertime() : 0)
                        + (salarySlip.getHra() != null ? salarySlip.getHra() : 0) + (salarySlip.getOtherAllowances() != null ? salarySlip.getOtherAllowances() : 0));
                Double totalDeductions = (salarySlip.getEsi() != null ? salarySlip.getEsi() : 0) + (salarySlip.getPf() != null ? salarySlip.getPf() : 0)
                        + (salarySlip.getPt() != null ? salarySlip.getPt() : 0) + (salarySlip.getAdvance() != null ? salarySlip.getAdvance() : 0);


                PdfPTable table1 = new PdfPTable(4); // 3 columns.

                table1.getDefaultCell().setBorderWidth(1);

                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase("INCOME", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
                table1.addCell(new Phrase("AMOUNT ", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
                table1.addCell(new Phrase("DEDUCTIONS", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
                table1.addCell(new Phrase("AMOUNT", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));

                if(salarySlip.getBasicDA() != null && salarySlip.getBasicDA() > 0) {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase("Basic & DA", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getBasicDA())), FontFactory.getFont(FontFactory.COURIER, 10)));
                } else {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                }
                if(salarySlip.getPt() != null && salarySlip.getPt() > 0) {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase("PT ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getPt())), FontFactory.getFont(FontFactory.COURIER, 10)));
                } else {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                }


                if(salarySlip.getHra() != null && salarySlip.getHra() > 0) {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase("HRA ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getHra())), FontFactory.getFont(FontFactory.COURIER, 10)));
                } else {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                }

                if(salarySlip.getPf() != null && salarySlip.getPf() > 0) {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase("PF ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getPf())), FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                } else {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                }


                if(salarySlip.getOtherAllowances() != null && salarySlip.getOtherAllowances() > 0) {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase("Other Allowances ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getOtherAllowances())), FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                } else {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                }

                if(salarySlip.getEsi() != null && salarySlip.getEsi() > 0) {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase("ESI ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getEsi())), FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                } else {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                }

                if(salarySlip.getOvertime()!=null && salarySlip.getOvertime() > 0) {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase("Overtime ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getOvertime())), FontFactory.getFont(FontFactory.COURIER, 10)));
                } else {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                }

                if(salarySlip.getAdvance() != null && salarySlip.getAdvance() > 0) {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase("Advance ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getAdvance())), FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                } else {
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                }


                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));

                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase("", FontFactory.getFont(FontFactory.COURIER, 10)));

                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Total Deductions ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(totalDeductions)), FontFactory.getFont(FontFactory.COURIER, 10)));

                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase( " ", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Total Payout ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getPayout())), FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));

                table1.addCell("  ");
                table1.addCell("  ");
                table1.addCell("  ");
                table1.addCell("  ");

                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Total Salary  ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(totalSalary)), FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Total Paid ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getActualPaid())), FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));

                document.add(table1);
                document.add(Chunk.NEWLINE);
                if(i % 2 != 0) {
                    document.newPage();
                } else {
                    LineSeparator ls = new LineSeparator();
                    document.add(new Chunk(ls));
                    document.add(Chunk.NEWLINE);
                    document.add(Chunk.NEWLINE);
                }
            }

            document.close();
            System.out.println("Pdf generated normally");


        } catch (Exception e) {
            Logger.getLogger(GeneratePDFReport.class.getName()).log(Level.SEVERE, null, e);
        }
        return new ByteArrayInputStream(out.toByteArray());
    }

    public static ByteArrayInputStream salarySlips(SalarySlip salarySlip, ZonedDateTime fromDate, ZonedDateTime toDate) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {

            Paragraph para;
            Document document = new Document();
            PdfWriter.getInstance(document, out);

            document.open();
            Font font1 = new Font(Font.FontFamily.COURIER, 10, Font.BOLD);


            para = new Paragraph("Theme Foods, Vadodara", font1);
            para.setAlignment(Element.ALIGN_CENTER);
            document.add(para);
            DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String fromDateString = DateTimeFormatter.ofPattern("dd-MMM-yyyy").format(getDate(fromDate));
            String toDateString = DateTimeFormatter.ofPattern("dd-MMM-yyyy").format(getDate(toDate));

            para = new Paragraph("Pay-slip for the period  " + fromDateString + " - " + toDateString, font1);
            para.setAlignment(Element.ALIGN_CENTER);
            document.add(para);
            document.add(Chunk.NEWLINE);
            PdfPTable table = new PdfPTable(2); // Code 1
            table.getDefaultCell().setBorder(0);
            // Code 2

            table.addCell(new Phrase("Name of Employee  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(salarySlip.getEmployee().getFirstName() + " " + (Objects.nonNull(salarySlip.getEmployee().getLastName()) ? salarySlip.getEmployee().getLastName() : ""), FontFactory.getFont(FontFactory.COURIER, 10)));
            // Code 3
            table.addCell(new Phrase("Office  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getOffice()) ? salarySlip.getEmployee().getOffice() : "", FontFactory.getFont(FontFactory.COURIER, 10)));

            table.addCell(new Phrase("Designation  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getDesignation()) ? salarySlip.getEmployee().getDesignation() : "", FontFactory.getFont(FontFactory.COURIER, 10)));

            table.addCell(new Phrase("Bank Details  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase((Objects.nonNull(salarySlip.getEmployee().getBankName()) ? (salarySlip.getEmployee().getBankName() + " | ")  : "") + (Objects.nonNull(salarySlip.getEmployee().getBankIFSC()) ? (salarySlip.getEmployee().getBankIFSC() + " | ")  : "") + (Objects.nonNull(salarySlip.getEmployee().getBankAcNumber()) ? (salarySlip.getEmployee().getBankAcNumber() )  : ""), FontFactory.getFont(FontFactory.COURIER, 10)));

            table.addCell(new Phrase("UAN  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getUanNumber()) ? salarySlip.getEmployee().getUanNumber()  : "", FontFactory.getFont(FontFactory.COURIER, 10)));

            table.addCell(new Phrase("ESI Number  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(Objects.nonNull(salarySlip.getEmployee().getEsiNumber()) ? salarySlip.getEmployee().getEsiNumber() : "", FontFactory.getFont(FontFactory.COURIER, 10)));

            table.addCell(new Phrase("Days Worked  :  ", FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD)));
//            table.addCell(new Phrase(":", FontFactory.getFont(FontFactory.COURIER, 10)));
            table.addCell(new Phrase(Double.toString(salarySlip.getDays()), FontFactory.getFont(FontFactory.COURIER, 10)));

            // Code 5

            document.add(table);

            document.add(Chunk.NEWLINE);

            Double totalSalary = salarySlip.getEmployee().getOffice().equalsIgnoreCase("The Private Banquet") ? salarySlip.getTotalSalary() : ((salarySlip.getBasicDA() != null ? salarySlip.getBasicDA() : 0) + (salarySlip.getOvertime() != null ? salarySlip.getOvertime() : 0)
                    + (salarySlip.getHra() != null ? salarySlip.getHra() : 0) + (salarySlip.getOtherAllowances() != null ? salarySlip.getOtherAllowances() : 0));
            Double totalDeductions = (salarySlip.getEsi() != null ? salarySlip.getEsi() : 0) + (salarySlip.getPf() != null ? salarySlip.getPf() : 0)
                    + (salarySlip.getPt() != null ? salarySlip.getPt() : 0) + (salarySlip.getAdvance() != null ? salarySlip.getAdvance() : 0);


            PdfPTable table1 = new PdfPTable(4); // 3 columns.

            table1.getDefaultCell().setBorderWidth(1);

            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase("INCOME", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
            table1.addCell(new Phrase("AMOUNT ", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
            table1.addCell(new Phrase("DEDUCTIONS", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
            table1.addCell(new Phrase("AMOUNT", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));

            if(salarySlip.getBasicDA() != null && salarySlip.getBasicDA() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Basic & DA", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getBasicDA())), FontFactory.getFont(FontFactory.COURIER, 10)));
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }
            if(salarySlip.getPt() != null && salarySlip.getPt() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("PT ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getPt())), FontFactory.getFont(FontFactory.COURIER, 10)));
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }


            if(salarySlip.getHra() != null && salarySlip.getHra() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("HRA ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getHra())), FontFactory.getFont(FontFactory.COURIER, 10)));
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }

            if(salarySlip.getPf() != null && salarySlip.getPf() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("PF ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getPf())), FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }


            if(salarySlip.getOtherAllowances() != null && salarySlip.getOtherAllowances() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Other Allowances ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getOtherAllowances())), FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }

            if(salarySlip.getEsi() != null && salarySlip.getEsi() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("ESI ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getEsi())), FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }

            if(salarySlip.getOvertime()!=null && salarySlip.getOvertime() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Overtime ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getOvertime())), FontFactory.getFont(FontFactory.COURIER, 10)));
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }

            if(salarySlip.getAdvance() != null && salarySlip.getAdvance() > 0) {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase("Advance ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getAdvance())), FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            } else {
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            }


            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));

            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase("", FontFactory.getFont(FontFactory.COURIER, 10)));

            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase("Total Deductions ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(Double.toString(Math.round(totalDeductions)), FontFactory.getFont(FontFactory.COURIER, 10)));

            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase(" ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase( " ", FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase("Total Payout ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getPayout())), FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));

            table1.addCell("  ");
            table1.addCell("  ");
            table1.addCell("  ");
            table1.addCell("  ");

            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase("Total Salary  ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(Double.toString(Math.round(totalSalary)), FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(new Phrase("Total Paid ", FontFactory.getFont(FontFactory.COURIER, 10)));
            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(new Phrase(Double.toString(Math.round(salarySlip.getActualPaid())), FontFactory.getFont(FontFactory.COURIER_BOLD, 10)));

            document.add(table1);
            document.add(Chunk.NEWLINE);
            document.close();
            System.out.println("Pdf generated normally");


        } catch (Exception e) {
            Logger.getLogger(GeneratePDFReport.class.getName()).log(Level.SEVERE, null, e);
        }
        return new ByteArrayInputStream(out.toByteArray());
    }

    public static ZonedDateTime getDate(ZonedDateTime zonedDateTime) {
        ZoneId india = ZoneId.of("Asia/Kolkata");
        DateFormat dfIST = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dfIST.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat dfUtc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dfUtc.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = dfIST.parse(dfIST.format(dfUtc.parse(dfUtc.format(Date.from(zonedDateTime.toInstant())))));
            return ZonedDateTime.ofInstant(date.toInstant(), india);

        } catch (ParseException e) {
            e.printStackTrace();              // invalid input
        }
        return null;
    }

}
