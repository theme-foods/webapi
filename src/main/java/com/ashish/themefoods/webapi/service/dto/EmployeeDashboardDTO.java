package com.ashish.themefoods.webapi.service.dto;

import java.util.List;

public class EmployeeDashboardDTO {

    private EmployeeDTO employee;
    private List<SalarySlipDTO> salarySlips;
    private List<AdvanceTransactionHistoryDTO> advancesList;
    private List<OvertimeDTO> overtimeList;
    private LeaveDashboardDTO leaveDashboard;
    private AdvanceTransactionDTO advanceTransaction;

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    public List<SalarySlipDTO> getSalarySlips() {
        return salarySlips;
    }

    public void setSalarySlips(List<SalarySlipDTO> salarySlips) {
        this.salarySlips = salarySlips;
    }

    public List<AdvanceTransactionHistoryDTO> getAdvancesList() {
        return advancesList;
    }

    public void setAdvancesList(List<AdvanceTransactionHistoryDTO> advancesList) {
        this.advancesList = advancesList;
    }

    public List<OvertimeDTO> getOvertimeList() {
        return overtimeList;
    }

    public void setOvertimeList(List<OvertimeDTO> overtimeList) {
        this.overtimeList = overtimeList;
    }

    public LeaveDashboardDTO getLeaveDashboard() {
        return leaveDashboard;
    }

    public void setLeaveDashboard(LeaveDashboardDTO leaveDashboard) {
        this.leaveDashboard = leaveDashboard;
    }

    public AdvanceTransactionDTO getAdvanceTransaction() {
        return advanceTransaction;
    }

    public void setAdvanceTransaction(AdvanceTransactionDTO advanceTransaction) {
        this.advanceTransaction = advanceTransaction;
    }

    @Override
    public String toString() {
        return "EmployeeDashboardDTO{" +
                "employeeDTO=" + employee +
                ", salarySlips=" + salarySlips +
                ", advancesList=" + advancesList +
                ", overtimeList=" + overtimeList +
                ", leaveDashboard=" + leaveDashboard +
                ", advanceTransaction=" + advanceTransaction +
                '}';
    }
}
