package com.ashish.themefoods.webapi.service.mapper;

import com.ashish.themefoods.webapi.domain.AdvanceTransactionHistory;
import com.ashish.themefoods.webapi.service.dto.AdvanceTransactionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface AdvanceTransactionToHistoryMapper extends EntityMapper<AdvanceTransactionDTO, AdvanceTransactionHistory>{
    @Mapping(source = "employee.id", target = "employeeId")
    AdvanceTransactionDTO toDto(AdvanceTransactionHistory advanceTransactionHistory);

    @Mapping(source = "employeeId", target = "employee")
    AdvanceTransactionHistory toEntity(AdvanceTransactionDTO advanceTransactionDTO);

    default AdvanceTransactionHistory fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdvanceTransactionHistory advanceTransaction = new AdvanceTransactionHistory();
        advanceTransaction.setId(id);
        return advanceTransaction;
    }
}
