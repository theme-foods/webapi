package com.ashish.themefoods.webapi.service.dto;

public class AdminDashboardDTO {

    private Integer totalEmployees;
    private Integer oldSchoolEmployees;
    private Integer lePriveEmployees;
    private Integer privateBanquetEmployees;
    private Double totalAdvance;
    private Double totalAdvanceCurrentMonth;

    public Double getTotalAdvanceCurrentMonth() {
        return totalAdvanceCurrentMonth;
    }

    public void setTotalAdvanceCurrentMonth(Double totalAdvanceCurrentMonth) {
        this.totalAdvanceCurrentMonth = totalAdvanceCurrentMonth;
    }

    public Integer getTotalEmployees() {
        return totalEmployees;
    }

    public void setTotalEmployees(Integer totalEmployees) {
        this.totalEmployees = totalEmployees;
    }

    public Integer getOldSchoolEmployees() {
        return oldSchoolEmployees;
    }

    public void setOldSchoolEmployees(Integer oldSchoolEmployees) {
        this.oldSchoolEmployees = oldSchoolEmployees;
    }

    public Integer getLePriveEmployees() {
        return lePriveEmployees;
    }

    public void setLePriveEmployees(Integer lePriveEmployees) {
        this.lePriveEmployees = lePriveEmployees;
    }

    public Integer getPrivateBanquetEmployees() {
        return privateBanquetEmployees;
    }

    public void setPrivateBanquetEmployees(Integer privateBanquetEmployees) {
        this.privateBanquetEmployees = privateBanquetEmployees;
    }

    public Double getTotalAdvance() {
        return totalAdvance;
    }

    public void setTotalAdvance(Double totalAdvance) {
        this.totalAdvance = totalAdvance;
    }

    @Override
    public String toString() {
        return "AdminDashboardDTO{" +
                "totalEmployees=" + totalEmployees +
                ", oldSchoolEmployees=" + oldSchoolEmployees +
                ", lePriveEmployees=" + lePriveEmployees +
                ", privateBanquetEmployees=" + privateBanquetEmployees +
                ", totalAdvance=" + totalAdvance +
                '}';
    }
}
