package com.ashish.themefoods.webapi.service;

import com.ashish.themefoods.webapi.domain.Employee;
import com.ashish.themefoods.webapi.domain.Salary;
import com.ashish.themefoods.webapi.repository.EmployeeRepository;
import com.ashish.themefoods.webapi.repository.SalaryRepository;
import com.ashish.themefoods.webapi.service.dto.SalaryDTO;
import com.ashish.themefoods.webapi.service.mapper.SalaryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Salary.
 */
@Service
@Transactional
public class SalaryService {

    private final Logger log = LoggerFactory.getLogger(SalaryService.class);

    private final SalaryRepository salaryRepository;

    private final SalaryMapper salaryMapper;

    private final SalarySlipService salarySlipService;

    private final EmployeeRepository employeeRepository;

    public SalaryService(SalaryRepository salaryRepository, SalaryMapper salaryMapper, SalarySlipService salarySlipService, EmployeeRepository employeeRepository) {
        this.salaryRepository = salaryRepository;
        this.salaryMapper = salaryMapper;
        this.salarySlipService = salarySlipService;
        this.employeeRepository = employeeRepository;
    }

    /**
     * Save a salary.
     *
     * @param salaryDTO the entity to save
     * @return the persisted entity
     */
    public SalaryDTO save(SalaryDTO salaryDTO) {
        log.debug("Request to save Salary : {}", salaryDTO);
        Salary salary = salaryMapper.toEntity(salaryDTO);
        salary = salaryRepository.save(salary);
        return salaryMapper.toDto(salary);
    }

    /**
     * Get all the salaries.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SalaryDTO> findAll() {
        log.debug("Request to get all Salaries");
        return salaryRepository.findAll().stream()
            .map(salaryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one salary by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<SalaryDTO> findOne(Long id) {
        log.debug("Request to get Salary : {}", id);
        return salaryRepository.findById(id)
            .map(salaryMapper::toDto);
    }

    /**
     * Delete the salary by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Salary : {}", id);
        salaryRepository.deleteById(id);
    }


    // run on 1st of every month at morning 1.
    @Scheduled(cron = "0 0 1 1 * ?")
//    @Scheduled(cron = "0 0/2 * * * ?")
    @Transactional
    public void generateSalarySlip() {
        log.debug("Generating salary slip");
        List<Employee> employeeList = employeeRepository.findAllByIsDeleted(false);
        employeeList.forEach(employee -> {
            LocalDate localDate = LocalDate.now();
            log.debug("Current data {}", localDate);
            ZonedDateTime salaryDate = localDate.minusMonths(1).atStartOfDay(ZoneId.of("Asia/Kolkata"));
            log.debug("Salary date {}", salaryDate);
            salarySlipService.generateSalarySlipForEmployeeAndMonth(employee.getId(), salaryDate.plusDays(1), true);
        });
    }
}
