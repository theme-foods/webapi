package com.ashish.themefoods.webapi.service.mapper;

import com.ashish.themefoods.webapi.domain.*;
import com.ashish.themefoods.webapi.service.dto.AdvanceTransactionHistoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AdvanceTransactionHistory and its DTO AdvanceTransactionHistoryDTO.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface AdvanceTransactionHistoryMapper extends EntityMapper<AdvanceTransactionHistoryDTO, AdvanceTransactionHistory> {

    @Mapping(source = "employee.id", target = "employeeId")
    AdvanceTransactionHistoryDTO toDto(AdvanceTransactionHistory advanceTransactionHistory);

    @Mapping(source = "employeeId", target = "employee")
    AdvanceTransactionHistory toEntity(AdvanceTransactionHistoryDTO advanceTransactionHistoryDTO);

    default AdvanceTransactionHistory fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdvanceTransactionHistory advanceTransactionHistory = new AdvanceTransactionHistory();
        advanceTransactionHistory.setId(id);
        return advanceTransactionHistory;
    }
}
