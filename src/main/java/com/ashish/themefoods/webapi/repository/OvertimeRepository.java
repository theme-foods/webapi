package com.ashish.themefoods.webapi.repository;

import com.ashish.themefoods.webapi.domain.Overtime;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;


/**
 * Spring Data  repository for the Overtime entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OvertimeRepository extends JpaRepository<Overtime, Long> {

    List<Overtime> findAllByOrderByDateDesc();

    @Query("Select sum(o.hours) from Overtime o where o.employee.id = ?1 and month(o.date) = ?3 and year(o.date) = ?2")
    Double totalOvertimeHoursForEmployeeInMonth(Long employeeId, Integer year, Integer month);

    @Query("Select sum(o.hours) from Overtime o where o.employee.id = ?1 and o.date between ?2 and ?3")
    Double totalOvertimeHoursForEmployeeBetweenDates(Long employeeId, ZonedDateTime fromDate, ZonedDateTime toDate);

    @Query("SELECT o from Overtime o where o.employee.id = ?1 and year(o.createdDate) = year(current_date)")
    List<Overtime> findAllByEmployeeId(Long id);

}
