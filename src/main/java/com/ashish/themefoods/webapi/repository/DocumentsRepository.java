package com.ashish.themefoods.webapi.repository;

import com.ashish.themefoods.webapi.domain.Documents;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Documents entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DocumentsRepository extends JpaRepository<Documents, Long> {

    void deleteByDocumentUrl(String docUrl);

}
