package com.ashish.themefoods.webapi.repository;

import com.ashish.themefoods.webapi.domain.LeaveDashboard;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the LeaveDashboard entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LeaveDashboardRepository extends JpaRepository<LeaveDashboard, Long> {

    LeaveDashboard findByEmployeeId(Long employeeId);

    @Modifying
    @Query("UPDATE LeaveDashboard l set l.takenLeaves = ?1, l.availableLeaves = 30 - ?1 where l.employee.id = ?2")
    int updateTakenLeaves(Double leavesTaken, Long employeeId);

}
