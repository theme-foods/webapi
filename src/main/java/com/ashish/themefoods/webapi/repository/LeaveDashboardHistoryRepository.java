package com.ashish.themefoods.webapi.repository;

import com.ashish.themefoods.webapi.domain.LeaveDashboardHistory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the LeaveDashboardHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LeaveDashboardHistoryRepository extends JpaRepository<LeaveDashboardHistory, Long> {

}
