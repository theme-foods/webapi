package com.ashish.themefoods.webapi.repository;

import com.ashish.themefoods.webapi.domain.EmployeeLeave;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;


/**
 * Spring Data  repository for the EmployeeLeave entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmployeeLeaveRepository extends JpaRepository<EmployeeLeave, Long> {

    List<EmployeeLeave> findAllByOrderByFromDateDesc();

    List<EmployeeLeave> findByFromDateGreaterThanEqualOrderByFromDateDesc(ZonedDateTime filterFromDate);
    List<EmployeeLeave> findByToDateLessThanEqualOrderByToDateDesc(ZonedDateTime filterFromDate);
    List<EmployeeLeave> findByFromDateGreaterThanEqualAndToDateLessThanEqualOrderByFromDateDesc(ZonedDateTime filterFromDate, ZonedDateTime filterToDate);

    @Query("SELECT sum(el.noOfDays) from EmployeeLeave el where el.employee.id = ?1 and month(el.fromDate) = ?2 and el.absence = false")
    Double totalLeavesInSalaryMonth(Long employeeId, Integer salaryMonth);

    @Query("SELECT sum(el.noOfDays) from EmployeeLeave el where el.employee.id = ?1 and month(el.fromDate) = ?2 and el.absence = true")
    Double totalAbsentsInSalaryMonth(Long employeeId, Integer salaryMonth);

    @Query("SELECT sum(el.noOfDays) from EmployeeLeave el where el.employee.id = ?1 and el.fromDate >= ?2 and el.toDate <= ?3 and el.absence = false")
    Double totalLeavesBetweenDates(Long employeeId, ZonedDateTime fromDate, ZonedDateTime toDate);

    @Query("SELECT sum(el.noOfDays) from EmployeeLeave el where el.employee.id = ?1 and el.fromDate >= ?2 and el.toDate <= ?3 and el.absence = true")
    Double totalAbsentsBetweenDates(Long employeeId, ZonedDateTime fromDate, ZonedDateTime toDate);

    List<EmployeeLeave> findAllByEmployeeId(Long employeeId);
}
