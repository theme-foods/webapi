package com.ashish.themefoods.webapi.repository;

import com.ashish.themefoods.webapi.domain.Employee;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;


/**
 * Spring Data  repository for the Employee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query("UPDATE Employee e set isDeleted = ?2 WHERE e.id = ?1")
    @Modifying
    int updateDeleteStatus(Long id, Boolean status);

    @Query("UPDATE Employee e set isDeleted = ?2, e.reJoiningDate = ?3 WHERE e.id = ?1")
    @Modifying
    int updateDeleteStatus(Long id, Boolean status, ZonedDateTime rejoiningDate);


    List<Employee> findAllByOrderByIsDeletedAsc();

    List<Employee> findAllByIsDeleted(Boolean isDeleted);

    List<Employee> findAllByOffice(String office);

    Employee findByFirstNameIgnoreCaseAndLastNameIgnoreCase(String firstName, String lastName);

}
