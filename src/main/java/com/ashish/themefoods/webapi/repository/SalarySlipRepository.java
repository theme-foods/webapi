package com.ashish.themefoods.webapi.repository;

import com.ashish.themefoods.webapi.domain.SalarySlip;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;


/**
 * Spring Data  repository for the SalarySlip entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SalarySlipRepository extends JpaRepository<SalarySlip, Long> {

    @Query("SELECT SUM(s.extraLeaves) from SalarySlip s where s.employee.id = ?1 and year(s.salaryDate) = ?2")
    Double totalExtraLeavesTakenByEmployeeInYear(Long employeeId, Integer year);

    @Query("SELECT s from SalarySlip s where s.employee.id = ?1 and year(s.salaryDate) = year(current_date)")
    List<SalarySlip> findAllByEmployeeId(Long employeeId);

    @Query("SELECT s from SalarySlip s where s.employee.id = ?3 and month(s.salaryDate) between ?1 and ?2 and year(s.salaryDate) = ?4")
    List<SalarySlip> findBySalaryDateBetweenAndEmployeeId(Integer fromDate, Integer toDate, Long employeeId, Integer year);

    @Query("SELECT s from SalarySlip s where s.employee.id = ?2 and month(s.salaryDate) = ?1 and year(s.salaryDate) = ?3")
    List<SalarySlip> findBySalaryDateBetweenAndEmployeeId(Integer fromDate, Long employeeId, Integer year);

    @Query("SELECT s from SalarySlip s where s.employee.id = ?1 and month(s.salaryDate) = ?2 and year(s.salaryDate) = ?3")
    SalarySlip findByEmployeeIdAndMonth(Long employeeId, Integer month, Integer year);

    @Query("SELECT s from SalarySlip s where month(s.salaryDate) = ?1 and year(s.salaryDate) = ?2")
    List<SalarySlip> findByMonth(Integer month, Integer year);
}
