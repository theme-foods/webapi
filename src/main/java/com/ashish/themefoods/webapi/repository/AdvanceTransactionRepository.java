package com.ashish.themefoods.webapi.repository;

import com.ashish.themefoods.webapi.domain.AdvanceTransaction;
import com.ashish.themefoods.webapi.service.dto.AdvanceTransactionProjection;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the AdvanceTransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdvanceTransactionRepository extends JpaRepository<AdvanceTransaction, Long> {

    List<AdvanceTransaction> findAllByTypeOfAdvance(String typeOfAdvance);

    List<AdvanceTransaction> findAllByOrderByTakenOnDesc();

    @Query(value = "select employee_id as employeeId, sum(case when type_of_advance = 'Credit' then amount else 0 end) as amount, " +
            "sum(case when type_of_advance = 'Credit' then amount else 0 end) - sum(case when type_of_advance = 'Debit' then amount else 0 end) as amountRemaining" +
            " from advance_transaction group by employee_id", nativeQuery = true)
    List<AdvanceTransactionProjection> findAllDetailsOfAdvances();

    @Query(value = "select employee_id as employeeId, sum(case when type_of_advance = 'Credit' then amount else 0 end) as amount, " +
            "sum(case when type_of_advance = 'Credit' then amount else 0 end) - sum(case when type_of_advance = 'Debit' then amount else 0 end) as amountRemaining, " +
            "sum(case when type_of_advance = 'Credit' then tenure else 0 end) - sum(case when type_of_advance = 'Debit' then tenure else 0 end) as tenure " +
            "from advance_transaction where advance_transaction.employee_id = ?1 group by employee_id", nativeQuery = true)
    List<AdvanceTransactionProjection> findAllDetailsOfAdvancesByEmployeeId(Long employeeId);

    List<AdvanceTransaction> findAllByTypeOfAdvanceAndEmployeeId(String typeOfAdvance, Long employeeId);

    @Modifying
    @Query("Update AdvanceTransaction a set a.amount = ?2, a.tenure = ?3 where a.employee.id = ?1 and a.typeOfAdvance = 'Credit'")
    int updateAdvanceForEmployee(Long employeeId, Double amount, Integer tenure);

    @Query("Select sum(a.amount) from AdvanceTransaction a where month(a.takenOn) = month(current_date) and a.typeOfAdvance = 'Credit'")
    Double findTotalAdvanceForAMonth();

    @Query("Select sum(a.amount) from AdvanceTransaction a where a.typeOfAdvance = 'Credit'")
    Double findTotalAdvance();

    void deleteByEmployeeId(Long employeeId);
}
