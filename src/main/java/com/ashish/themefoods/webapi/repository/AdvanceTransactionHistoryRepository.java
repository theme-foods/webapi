package com.ashish.themefoods.webapi.repository;

import com.ashish.themefoods.webapi.domain.AdvanceTransactionHistory;
import com.ashish.themefoods.webapi.domain.AdvanceTransactionHistory_;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the AdvanceTransactionHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdvanceTransactionHistoryRepository extends JpaRepository<AdvanceTransactionHistory, Long> {

    void deleteByEmployeeId(Long employeeId);

    @Query("SELECT a from AdvanceTransactionHistory a where a.employee.id = ?1 and year(a.createdDate) = year(current_date) and a.typeOfAdvance = 'Credit'")
    List<AdvanceTransactionHistory> findAllByEmployeeId(Long id);

    AdvanceTransactionHistory findTopByTypeOfAdvanceAndEmployeeIdOrderByLastModifiedDateDesc(String typeOfAdvance, Long employeeId);

}

