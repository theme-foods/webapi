package com.ashish.themefoods.webapi.web.rest;
import com.ashish.themefoods.webapi.service.LeaveDashboardService;
import com.ashish.themefoods.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.themefoods.webapi.web.rest.util.HeaderUtil;
import com.ashish.themefoods.webapi.service.dto.LeaveDashboardDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LeaveDashboard.
 */
@RestController
@RequestMapping("/api")
public class LeaveDashboardResource {

    private final Logger log = LoggerFactory.getLogger(LeaveDashboardResource.class);

    private static final String ENTITY_NAME = "leaveDashboard";

    private final LeaveDashboardService leaveDashboardService;

    public LeaveDashboardResource(LeaveDashboardService leaveDashboardService) {
        this.leaveDashboardService = leaveDashboardService;
    }

    /**
     * POST  /leave-dashboards : Create a new leaveDashboard.
     *
     * @param leaveDashboardDTO the leaveDashboardDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new leaveDashboardDTO, or with status 400 (Bad Request) if the leaveDashboard has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/leave-dashboards")
    public ResponseEntity<LeaveDashboardDTO> createLeaveDashboard(@RequestBody LeaveDashboardDTO leaveDashboardDTO) throws URISyntaxException {
        log.debug("REST request to save LeaveDashboard : {}", leaveDashboardDTO);
        if (leaveDashboardDTO.getId() != null) {
            throw new BadRequestAlertException("A new leaveDashboard cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LeaveDashboardDTO result = leaveDashboardService.save(leaveDashboardDTO);
        return ResponseEntity.created(new URI("/api/leave-dashboards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /leave-dashboards : Updates an existing leaveDashboard.
     *
     * @param leaveDashboardDTO the leaveDashboardDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated leaveDashboardDTO,
     * or with status 400 (Bad Request) if the leaveDashboardDTO is not valid,
     * or with status 500 (Internal Server Error) if the leaveDashboardDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/leave-dashboards")
    public ResponseEntity<LeaveDashboardDTO> updateLeaveDashboard(@RequestBody LeaveDashboardDTO leaveDashboardDTO) throws URISyntaxException {
        log.debug("REST request to update LeaveDashboard : {}", leaveDashboardDTO);
        if (leaveDashboardDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LeaveDashboardDTO result = leaveDashboardService.save(leaveDashboardDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, leaveDashboardDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /leave-dashboards : get all the leaveDashboards.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of leaveDashboards in body
     */
    @GetMapping("/leave-dashboards")
    public List<LeaveDashboardDTO> getAllLeaveDashboards() {
        log.debug("REST request to get all LeaveDashboards");
        return leaveDashboardService.findAll();
    }

    /**
     * GET  /leave-dashboards/:id : get the "id" leaveDashboard.
     *
     * @param id the id of the leaveDashboardDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the leaveDashboardDTO, or with status 404 (Not Found)
     */
    @GetMapping("/leave-dashboards/{id}")
    public ResponseEntity<LeaveDashboardDTO> getLeaveDashboard(@PathVariable Long id) {
        log.debug("REST request to get LeaveDashboard : {}", id);
        Optional<LeaveDashboardDTO> leaveDashboardDTO = leaveDashboardService.findOne(id);
        return ResponseUtil.wrapOrNotFound(leaveDashboardDTO);
    }

    /**
     * DELETE  /leave-dashboards/:id : delete the "id" leaveDashboard.
     *
     * @param id the id of the leaveDashboardDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/leave-dashboards/{id}")
    public ResponseEntity<Void> deleteLeaveDashboard(@PathVariable Long id) {
        log.debug("REST request to delete LeaveDashboard : {}", id);
        leaveDashboardService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
