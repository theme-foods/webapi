package com.ashish.themefoods.webapi.web.rest;
import com.ashish.themefoods.webapi.service.SalaryService;
import com.ashish.themefoods.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.themefoods.webapi.web.rest.util.HeaderUtil;
import com.ashish.themefoods.webapi.service.dto.SalaryDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Salary.
 */
@RestController
@RequestMapping("/api")
public class SalaryResource {

    private final Logger log = LoggerFactory.getLogger(SalaryResource.class);

    private static final String ENTITY_NAME = "salary";

    private final SalaryService salaryService;

    public SalaryResource(SalaryService salaryService) {
        this.salaryService = salaryService;
    }

    /**
     * POST  /salaries : Create a new salary.
     *
     * @param salaryDTO the salaryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new salaryDTO, or with status 400 (Bad Request) if the salary has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/salaries")
    public ResponseEntity<SalaryDTO> createSalary(@RequestBody SalaryDTO salaryDTO) throws URISyntaxException {
        log.debug("REST request to save Salary : {}", salaryDTO);
        if (salaryDTO.getId() != null) {
            throw new BadRequestAlertException("A new salary cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SalaryDTO result = salaryService.save(salaryDTO);
        return ResponseEntity.created(new URI("/api/salaries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /salaries : Updates an existing salary.
     *
     * @param salaryDTO the salaryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated salaryDTO,
     * or with status 400 (Bad Request) if the salaryDTO is not valid,
     * or with status 500 (Internal Server Error) if the salaryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/salaries")
    public ResponseEntity<SalaryDTO> updateSalary(@RequestBody SalaryDTO salaryDTO) throws URISyntaxException {
        log.debug("REST request to update Salary : {}", salaryDTO);
        if (salaryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SalaryDTO result = salaryService.save(salaryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, salaryDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /salaries : get all the salaries.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of salaries in body
     */
    @GetMapping("/salaries")
    public List<SalaryDTO> getAllSalaries() {
        log.debug("REST request to get all Salaries");
        return salaryService.findAll();
    }

    /**
     * GET  /salaries/:id : get the "id" salary.
     *
     * @param id the id of the salaryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the salaryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/salaries/{id}")
    public ResponseEntity<SalaryDTO> getSalary(@PathVariable Long id) {
        log.debug("REST request to get Salary : {}", id);
        Optional<SalaryDTO> salaryDTO = salaryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(salaryDTO);
    }

    /**
     * DELETE  /salaries/:id : delete the "id" salary.
     *
     * @param id the id of the salaryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/salaries/{id}")
    public ResponseEntity<Void> deleteSalary(@PathVariable Long id) {
        log.debug("REST request to delete Salary : {}", id);
        salaryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
