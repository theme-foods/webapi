package com.ashish.themefoods.webapi.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class GlobalCustomException extends AbstractThrowableProblem {
    public GlobalCustomException(URI type, String message, Status statusCode) {
        super(type, message, statusCode);
    }
}
