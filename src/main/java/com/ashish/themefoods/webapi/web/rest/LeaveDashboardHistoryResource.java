package com.ashish.themefoods.webapi.web.rest;
import com.ashish.themefoods.webapi.service.LeaveDashboardHistoryService;
import com.ashish.themefoods.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.themefoods.webapi.web.rest.util.HeaderUtil;
import com.ashish.themefoods.webapi.service.dto.LeaveDashboardHistoryDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LeaveDashboardHistory.
 */
@RestController
@RequestMapping("/api")
public class LeaveDashboardHistoryResource {

    private final Logger log = LoggerFactory.getLogger(LeaveDashboardHistoryResource.class);

    private static final String ENTITY_NAME = "leaveDashboardHistory";

    private final LeaveDashboardHistoryService leaveDashboardHistoryService;

    public LeaveDashboardHistoryResource(LeaveDashboardHistoryService leaveDashboardHistoryService) {
        this.leaveDashboardHistoryService = leaveDashboardHistoryService;
    }

    /**
     * POST  /leave-dashboard-histories : Create a new leaveDashboardHistory.
     *
     * @param leaveDashboardHistoryDTO the leaveDashboardHistoryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new leaveDashboardHistoryDTO, or with status 400 (Bad Request) if the leaveDashboardHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/leave-dashboard-histories")
    public ResponseEntity<LeaveDashboardHistoryDTO> createLeaveDashboardHistory(@RequestBody LeaveDashboardHistoryDTO leaveDashboardHistoryDTO) throws URISyntaxException {
        log.debug("REST request to save LeaveDashboardHistory : {}", leaveDashboardHistoryDTO);
        if (leaveDashboardHistoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new leaveDashboardHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LeaveDashboardHistoryDTO result = leaveDashboardHistoryService.save(leaveDashboardHistoryDTO);
        return ResponseEntity.created(new URI("/api/leave-dashboard-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /leave-dashboard-histories : Updates an existing leaveDashboardHistory.
     *
     * @param leaveDashboardHistoryDTO the leaveDashboardHistoryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated leaveDashboardHistoryDTO,
     * or with status 400 (Bad Request) if the leaveDashboardHistoryDTO is not valid,
     * or with status 500 (Internal Server Error) if the leaveDashboardHistoryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/leave-dashboard-histories")
    public ResponseEntity<LeaveDashboardHistoryDTO> updateLeaveDashboardHistory(@RequestBody LeaveDashboardHistoryDTO leaveDashboardHistoryDTO) throws URISyntaxException {
        log.debug("REST request to update LeaveDashboardHistory : {}", leaveDashboardHistoryDTO);
        if (leaveDashboardHistoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LeaveDashboardHistoryDTO result = leaveDashboardHistoryService.save(leaveDashboardHistoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, leaveDashboardHistoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /leave-dashboard-histories : get all the leaveDashboardHistories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of leaveDashboardHistories in body
     */
    @GetMapping("/leave-dashboard-histories")
    public List<LeaveDashboardHistoryDTO> getAllLeaveDashboardHistories() {
        log.debug("REST request to get all LeaveDashboardHistories");
        return leaveDashboardHistoryService.findAll();
    }

    /**
     * GET  /leave-dashboard-histories/:id : get the "id" leaveDashboardHistory.
     *
     * @param id the id of the leaveDashboardHistoryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the leaveDashboardHistoryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/leave-dashboard-histories/{id}")
    public ResponseEntity<LeaveDashboardHistoryDTO> getLeaveDashboardHistory(@PathVariable Long id) {
        log.debug("REST request to get LeaveDashboardHistory : {}", id);
        Optional<LeaveDashboardHistoryDTO> leaveDashboardHistoryDTO = leaveDashboardHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(leaveDashboardHistoryDTO);
    }

    /**
     * DELETE  /leave-dashboard-histories/:id : delete the "id" leaveDashboardHistory.
     *
     * @param id the id of the leaveDashboardHistoryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/leave-dashboard-histories/{id}")
    public ResponseEntity<Void> deleteLeaveDashboardHistory(@PathVariable Long id) {
        log.debug("REST request to delete LeaveDashboardHistory : {}", id);
        leaveDashboardHistoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
