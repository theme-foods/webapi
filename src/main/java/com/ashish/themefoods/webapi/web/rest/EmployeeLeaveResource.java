package com.ashish.themefoods.webapi.web.rest;
import com.ashish.themefoods.webapi.service.EmployeeLeaveService;
import com.ashish.themefoods.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.themefoods.webapi.web.rest.util.HeaderUtil;
import com.ashish.themefoods.webapi.service.dto.EmployeeLeaveDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EmployeeLeave.
 */
@RestController
@RequestMapping("/api")
public class EmployeeLeaveResource {

    private final Logger log = LoggerFactory.getLogger(EmployeeLeaveResource.class);

    private static final String ENTITY_NAME = "employeeLeave";

    private final EmployeeLeaveService employeeLeaveService;

    public EmployeeLeaveResource(EmployeeLeaveService employeeLeaveService) {
        this.employeeLeaveService = employeeLeaveService;
    }

    /**
     * POST  /employee-leaves : Create a new employeeLeave.
     *
     * @param employeeLeaveDTO the employeeLeaveDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new employeeLeaveDTO, or with status 400 (Bad Request) if the employeeLeave has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/employee-leaves")
    public ResponseEntity<EmployeeLeaveDTO> createEmployeeLeave(@Valid @RequestBody EmployeeLeaveDTO employeeLeaveDTO) throws URISyntaxException {
        log.debug("REST request to save EmployeeLeave : {}", employeeLeaveDTO);
        if (employeeLeaveDTO.getId() != null) {
            throw new BadRequestAlertException("A new employeeLeave cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmployeeLeaveDTO result = employeeLeaveService.saveLeave(employeeLeaveDTO);
        return ResponseEntity.created(new URI("/api/employee-leaves/"))
            .body(result);
    }

    /**
     * PUT  /employee-leaves : Updates an existing employeeLeave.
     *
     * @param employeeLeaveDTO the employeeLeaveDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated employeeLeaveDTO,
     * or with status 400 (Bad Request) if the employeeLeaveDTO is not valid,
     * or with status 500 (Internal Server Error) if the employeeLeaveDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/employee-leaves")
    public ResponseEntity<EmployeeLeaveDTO> updateEmployeeLeave(@Valid @RequestBody EmployeeLeaveDTO employeeLeaveDTO) throws URISyntaxException {
        log.debug("REST request to update EmployeeLeave : {}", employeeLeaveDTO);
        if (employeeLeaveDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EmployeeLeaveDTO result = employeeLeaveService.save(employeeLeaveDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, employeeLeaveDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /employee-leaves : get all the employeeLeaves.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of employeeLeaves in body
     */
    @GetMapping("/employee-leaves")
    public List<EmployeeLeaveDTO> getAllEmployeeLeaves(@RequestParam(required = false) String filterFromDate, @RequestParam(required = false) String filterToDate) {
        log.debug("REST request to get all EmployeeLeaves");
        return employeeLeaveService.findAll(filterFromDate, filterToDate);
    }

    /**
     * GET  /employee-leaves/:id : get the "id" employeeLeave.
     *
     * @param id the id of the employeeLeaveDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the employeeLeaveDTO, or with status 404 (Not Found)
     */
    @GetMapping("/employee-leaves/{id}")
    public ResponseEntity<EmployeeLeaveDTO> getEmployeeLeave(@PathVariable Long id) {
        log.debug("REST request to get EmployeeLeave : {}", id);
        Optional<EmployeeLeaveDTO> employeeLeaveDTO = employeeLeaveService.findOne(id);
        return ResponseUtil.wrapOrNotFound(employeeLeaveDTO);
    }

    /**
     * DELETE  /employee-leaves/:id : delete the "id" employeeLeave.
     *
     * @param id the id of the employeeLeaveDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/employee-leaves/{id}")
    public ResponseEntity<Void> deleteEmployeeLeave(@PathVariable Long id) {
        log.debug("REST request to delete EmployeeLeave : {}", id);
        employeeLeaveService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
