package com.ashish.themefoods.webapi.web.rest;

import com.ashish.themefoods.webapi.domain.Employee;
import com.ashish.themefoods.webapi.domain.SalarySlip;
import com.ashish.themefoods.webapi.service.EmployeeService;
import com.ashish.themefoods.webapi.service.SalarySlipService;
import com.ashish.themefoods.webapi.service.dto.AdminDashboardDTO;
import com.ashish.themefoods.webapi.service.dto.EmployeeDTO;
import com.ashish.themefoods.webapi.service.dto.EmployeeDashboardDTO;
import com.ashish.themefoods.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.themefoods.webapi.web.rest.errors.ErrorConstants;
import com.ashish.themefoods.webapi.web.rest.errors.GlobalCustomException;
import com.ashish.themefoods.webapi.web.rest.util.HeaderUtil;
import com.ashish.themefoods.webapi.service.dto.SalarySlipDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.zalando.problem.Status;

import java.net.URI;
import java.net.URISyntaxException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SalarySlip.
 */
@RestController
@RequestMapping("/api")
public class SalarySlipResource {

    private static final String ENTITY_NAME = "salarySlip";
    private final Logger log = LoggerFactory.getLogger(SalarySlipResource.class);
    private final SalarySlipService salarySlipService;

    private final EmployeeService employeeService;

    public SalarySlipResource(SalarySlipService salarySlipService, EmployeeService employeeService) {
        this.salarySlipService = salarySlipService;
        this.employeeService = employeeService;
    }

    /**
     * POST  /salary-slips : Create a new salarySlip.
     *
     * @param salarySlipDTO the salarySlipDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new salarySlipDTO, or with status 400 (Bad Request) if the salarySlip has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/salary-slips")
    public ResponseEntity<SalarySlipDTO> createSalarySlip(@RequestBody SalarySlipDTO salarySlipDTO) throws URISyntaxException {
        log.debug("REST request to save SalarySlip : {}", salarySlipDTO);
        if (salarySlipDTO.getId() != null) {
            throw new BadRequestAlertException("A new salarySlip cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SalarySlipDTO result = salarySlipService.save(salarySlipDTO);
        return ResponseEntity.created(new URI("/api/salary-slips/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /salary-slips : Updates an existing salarySlip.
     *
     * @param salarySlipDTO the salarySlipDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated salarySlipDTO,
     * or with status 400 (Bad Request) if the salarySlipDTO is not valid,
     * or with status 500 (Internal Server Error) if the salarySlipDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/salary-slips")
    public ResponseEntity<SalarySlipDTO> updateSalarySlip(@RequestBody SalarySlipDTO salarySlipDTO) throws URISyntaxException {
        log.debug("REST request to update SalarySlip : {}", salarySlipDTO);
        if (salarySlipDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SalarySlipDTO result = salarySlipService.save(salarySlipDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, salarySlipDTO.getId().toString()))
                .body(result);
    }

    /**
     * GET  /salary-slips : get all the salarySlips.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of salarySlips in body
     */
    @GetMapping("/salary-slips")
    public List<SalarySlipDTO> getAllSalarySlips() {
        log.debug("REST request to get all SalarySlips");
        return salarySlipService.findAll();
    }

    /**
     * GET  /salary-slips/:id : get the "id" salarySlip.
     *
     * @param id the id of the salarySlipDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the salarySlipDTO, or with status 404 (Not Found)
     */
    @GetMapping("/salary-slips/{id}")
    public ResponseEntity<SalarySlipDTO> getSalarySlip(@PathVariable Long id) {
        log.debug("REST request to get SalarySlip : {}", id);
        Optional<SalarySlipDTO> salarySlipDTO = salarySlipService.findOne(id);
        return ResponseUtil.wrapOrNotFound(salarySlipDTO);
    }

    /**
     * DELETE  /salary-slips/:id : delete the "id" salarySlip.
     *
     * @param id the id of the salarySlipDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/salary-slips/{id}")
    public ResponseEntity<Void> deleteSalarySlip(@PathVariable Long id) {
        log.debug("REST request to delete SalarySlip : {}", id);
        salarySlipService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/employee-dashboard/{id}")
    public ResponseEntity<EmployeeDashboardDTO> getEmployeeDashboard(@PathVariable Long id) {
        log.debug("RES request to get employee dashboard {}", id);
        return ResponseEntity.ok().body(salarySlipService.getEmployeeDashboard(id));
    }

    @GetMapping("/admin-dashboard")
    public ResponseEntity<AdminDashboardDTO> getAdminDashboard() {
        log.debug("RES request to get admin dashboard {}");
        return ResponseEntity.ok().body(salarySlipService.getAdminDashboard());
    }

    @GetMapping("/salary-slip/today/{id}")
    public ResponseEntity<SalarySlip> getSalaryAsOfToday(@PathVariable Long id, @RequestParam(required = false) Long fromDate, @RequestParam(required = false) Long toDate) {
        log.debug("REST Request to get salary as of today");
        DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS");
        Date result = new Date(fromDate);
        ZonedDateTime fromZonedDate = ZonedDateTime.ofInstant(result.toInstant(), ZoneId.systemDefault());
        log.debug("Date after parsing {}", fromZonedDate);
        Date result1 = new Date(toDate);
        ZonedDateTime toZonedDate = ZonedDateTime.ofInstant(result1.toInstant(), ZoneId.systemDefault());
        log.debug("Date after parsing {}", toZonedDate);
        return ResponseEntity.ok().body(salarySlipService.getSalaryBetweenDates(id, fromZonedDate, toZonedDate, false));
    }

    @GetMapping(value = "/salary-pdf", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getSalarySlipPDF(@RequestParam Long employeeId, @RequestParam Integer month, @RequestParam Integer year) {
        HttpHeaders headers = new HttpHeaders();
        Optional<EmployeeDTO> employeeOptional = employeeService.findOne(employeeId);
        if (employeeOptional.isPresent()) {
            String fileName = "salary_slip_" + employeeOptional.get().getFirstName() + "_" + employeeOptional.get().getLastName() + ".pdf";
            headers.add("Content-Disposition", "attachment; filename=" + fileName);
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(new InputStreamResource(salarySlipService.getSalarySlip(employeeId, month, year)));
        } else {
            throw new GlobalCustomException(ErrorConstants.EMAIL_NOT_FOUND_TYPE, "User Not Found", Status.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/salary-pdf/all", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getSalarySlipPDFForAllEmployees(@RequestParam Integer month, @RequestParam Integer year, @RequestParam(required = false, defaultValue = "false") boolean isExcel) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=salary_slips.pdf");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(salarySlipService.getSalarySlip(month, year, isExcel)));

    }

    @GetMapping(value = "/salary-pdf/date", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getSalarySlipPDFBetweenDates(@RequestParam Long employeeId, @RequestParam Long fromDate, @RequestParam Long toDate,
                                                                            @RequestParam(required =  false, defaultValue = "false") Boolean settleAdvance) {
        HttpHeaders headers = new HttpHeaders();
        Optional<EmployeeDTO> employeeOptional = employeeService.findOne(employeeId);
        DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS");
        Date result = new Date(fromDate);
        ZonedDateTime fromZonedDate = ZonedDateTime.ofInstant(result.toInstant(), ZoneId.of("Asia/Kolkata"));
        Date result1 = new Date(toDate);
        ZonedDateTime toZonedDate = ZonedDateTime.ofInstant(result1.toInstant(), ZoneId.of("Asia/Kolkata"));
        if (employeeOptional.isPresent()) {
            String fileName = "salary_slip_" + employeeOptional.get().getFirstName() + "_" + employeeOptional.get().getLastName() + ".pdf";
            headers.add("Content-Disposition", "attachment; filename=" + fileName);
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(new InputStreamResource(salarySlipService.getSalarySlip(employeeId, fromZonedDate, toZonedDate, settleAdvance)));
        } else {
            throw new GlobalCustomException(ErrorConstants.EMAIL_NOT_FOUND_TYPE, "User Not Found", Status.BAD_REQUEST);
        }
    }
}
