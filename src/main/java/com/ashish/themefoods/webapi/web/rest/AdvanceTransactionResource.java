package com.ashish.themefoods.webapi.web.rest;
import com.ashish.themefoods.webapi.service.AdvanceTransactionService;
import com.ashish.themefoods.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.themefoods.webapi.web.rest.util.HeaderUtil;
import com.ashish.themefoods.webapi.service.dto.AdvanceTransactionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AdvanceTransaction.
 */
@RestController
@RequestMapping("/api")
public class AdvanceTransactionResource {

    private final Logger log = LoggerFactory.getLogger(AdvanceTransactionResource.class);

    private static final String ENTITY_NAME = "advanceTransaction";

    private final AdvanceTransactionService advanceTransactionService;

    public AdvanceTransactionResource(AdvanceTransactionService advanceTransactionService) {
        this.advanceTransactionService = advanceTransactionService;
    }

    /**
     * POST  /advance-transactions : Create a new advanceTransaction.
     *
     * @param advanceTransactionDTO the advanceTransactionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new advanceTransactionDTO, or with status 400 (Bad Request) if the advanceTransaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/advance-transactions")
    public ResponseEntity<AdvanceTransactionDTO> createAdvanceTransaction(@RequestBody AdvanceTransactionDTO advanceTransactionDTO) throws URISyntaxException {
        log.debug("REST request to save AdvanceTransaction : {}", advanceTransactionDTO);
        if (advanceTransactionDTO.getId() != null) {
            throw new BadRequestAlertException("A new advanceTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdvanceTransactionDTO result = advanceTransactionService.saveNew(advanceTransactionDTO);
        return ResponseEntity.created(new URI("/api/advance-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /advance-transactions : Updates an existing advanceTransaction.
     *
     * @param advanceTransactionDTO the advanceTransactionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated advanceTransactionDTO,
     * or with status 400 (Bad Request) if the advanceTransactionDTO is not valid,
     * or with status 500 (Internal Server Error) if the advanceTransactionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/advance-transactions")
    public ResponseEntity<AdvanceTransactionDTO> updateAdvanceTransaction(@RequestBody AdvanceTransactionDTO advanceTransactionDTO) throws URISyntaxException {
        log.debug("REST request to update AdvanceTransaction : {}", advanceTransactionDTO);
        if (advanceTransactionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AdvanceTransactionDTO result = advanceTransactionService.save(advanceTransactionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, advanceTransactionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /advance-transactions : get all the advanceTransactions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of advanceTransactions in body
     */
    @GetMapping("/advance-transactions")
    public List<AdvanceTransactionDTO> getAllAdvanceTransactions() {
        log.debug("REST request to get all AdvanceTransactions");
        return advanceTransactionService.findAll();
    }

    /**
     * GET  /advance-transactions/:id : get the "id" advanceTransaction.
     *
     * @param id the id of the advanceTransactionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the advanceTransactionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/advance-transactions/{id}")
    public ResponseEntity<AdvanceTransactionDTO> getAdvanceTransaction(@PathVariable Long id) {
        log.debug("REST request to get AdvanceTransaction : {}", id);
        Optional<AdvanceTransactionDTO> advanceTransactionDTO = advanceTransactionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(advanceTransactionDTO);
    }

    /**
     * DELETE  /advance-transactions/:id : delete the "id" advanceTransaction.
     *
     * @param id the id of the advanceTransactionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/advance-transactions/{id}")
    public ResponseEntity<Void> deleteAdvanceTransaction(@PathVariable Long id) {
        log.debug("REST request to delete AdvanceTransaction : {}", id);
        advanceTransactionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
