package com.ashish.themefoods.webapi.web.rest;
import com.ashish.themefoods.webapi.service.OvertimeService;
import com.ashish.themefoods.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.themefoods.webapi.web.rest.util.HeaderUtil;
import com.ashish.themefoods.webapi.service.dto.OvertimeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Overtime.
 */
@RestController
@RequestMapping("/api")
public class OvertimeResource {

    private final Logger log = LoggerFactory.getLogger(OvertimeResource.class);

    private static final String ENTITY_NAME = "overtime";

    private final OvertimeService overtimeService;

    public OvertimeResource(OvertimeService overtimeService) {
        this.overtimeService = overtimeService;
    }

    /**
     * POST  /overtimes : Create a new overtime.
     *
     * @param overtimeDTO the overtimeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new overtimeDTO, or with status 400 (Bad Request) if the overtime has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/overtimes")
    public ResponseEntity<OvertimeDTO> createOvertime(@Valid @RequestBody OvertimeDTO overtimeDTO) throws URISyntaxException {
        log.debug("REST request to save Overtime : {}", overtimeDTO);
        if (overtimeDTO.getId() != null) {
            throw new BadRequestAlertException("A new overtime cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OvertimeDTO result = overtimeService.save(overtimeDTO);
        return ResponseEntity.created(new URI("/api/overtimes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /overtimes : Updates an existing overtime.
     *
     * @param overtimeDTO the overtimeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated overtimeDTO,
     * or with status 400 (Bad Request) if the overtimeDTO is not valid,
     * or with status 500 (Internal Server Error) if the overtimeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/overtimes")
    public ResponseEntity<OvertimeDTO> updateOvertime(@Valid @RequestBody OvertimeDTO overtimeDTO) throws URISyntaxException {
        log.debug("REST request to update Overtime : {}", overtimeDTO);
        if (overtimeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OvertimeDTO result = overtimeService.save(overtimeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, overtimeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /overtimes : get all the overtimes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of overtimes in body
     */
    @GetMapping("/overtimes")
    public List<OvertimeDTO> getAllOvertimes() {
        log.debug("REST request to get all Overtimes");
        return overtimeService.findAll();
    }

    /**
     * GET  /overtimes/:id : get the "id" overtime.
     *
     * @param id the id of the overtimeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the overtimeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/overtimes/{id}")
    public ResponseEntity<OvertimeDTO> getOvertime(@PathVariable Long id) {
        log.debug("REST request to get Overtime : {}", id);
        Optional<OvertimeDTO> overtimeDTO = overtimeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(overtimeDTO);
    }

    /**
     * DELETE  /overtimes/:id : delete the "id" overtime.
     *
     * @param id the id of the overtimeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/overtimes/{id}")
    public ResponseEntity<Void> deleteOvertime(@PathVariable Long id) {
        log.debug("REST request to delete Overtime : {}", id);
        overtimeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
