package com.ashish.themefoods.webapi.web.rest;
import com.ashish.themefoods.webapi.service.AdvanceTransactionHistoryService;
import com.ashish.themefoods.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.themefoods.webapi.web.rest.util.HeaderUtil;
import com.ashish.themefoods.webapi.service.dto.AdvanceTransactionHistoryDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AdvanceTransactionHistory.
 */
@RestController
@RequestMapping("/api")
public class AdvanceTransactionHistoryResource {

    private final Logger log = LoggerFactory.getLogger(AdvanceTransactionHistoryResource.class);

    private static final String ENTITY_NAME = "advanceTransactionHistory";

    private final AdvanceTransactionHistoryService advanceTransactionHistoryService;

    public AdvanceTransactionHistoryResource(AdvanceTransactionHistoryService advanceTransactionHistoryService) {
        this.advanceTransactionHistoryService = advanceTransactionHistoryService;
    }

    /**
     * POST  /advance-transaction-histories : Create a new advanceTransactionHistory.
     *
     * @param advanceTransactionHistoryDTO the advanceTransactionHistoryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new advanceTransactionHistoryDTO, or with status 400 (Bad Request) if the advanceTransactionHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/advance-transaction-histories")
    public ResponseEntity<AdvanceTransactionHistoryDTO> createAdvanceTransactionHistory(@RequestBody AdvanceTransactionHistoryDTO advanceTransactionHistoryDTO) throws URISyntaxException {
        log.debug("REST request to save AdvanceTransactionHistory : {}", advanceTransactionHistoryDTO);
        if (advanceTransactionHistoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new advanceTransactionHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdvanceTransactionHistoryDTO result = advanceTransactionHistoryService.save(advanceTransactionHistoryDTO);
        return ResponseEntity.created(new URI("/api/advance-transaction-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /advance-transaction-histories : Updates an existing advanceTransactionHistory.
     *
     * @param advanceTransactionHistoryDTO the advanceTransactionHistoryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated advanceTransactionHistoryDTO,
     * or with status 400 (Bad Request) if the advanceTransactionHistoryDTO is not valid,
     * or with status 500 (Internal Server Error) if the advanceTransactionHistoryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/advance-transaction-histories")
    public ResponseEntity<AdvanceTransactionHistoryDTO> updateAdvanceTransactionHistory(@RequestBody AdvanceTransactionHistoryDTO advanceTransactionHistoryDTO) throws URISyntaxException {
        log.debug("REST request to update AdvanceTransactionHistory : {}", advanceTransactionHistoryDTO);
        if (advanceTransactionHistoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AdvanceTransactionHistoryDTO result = advanceTransactionHistoryService.save(advanceTransactionHistoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, advanceTransactionHistoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /advance-transaction-histories : get all the advanceTransactionHistories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of advanceTransactionHistories in body
     */
    @GetMapping("/advance-transaction-histories")
    public List<AdvanceTransactionHistoryDTO> getAllAdvanceTransactionHistories() {
        log.debug("REST request to get all AdvanceTransactionHistories");
        return advanceTransactionHistoryService.findAll();
    }

    /**
     * GET  /advance-transaction-histories/:id : get the "id" advanceTransactionHistory.
     *
     * @param id the id of the advanceTransactionHistoryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the advanceTransactionHistoryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/advance-transaction-histories/{id}")
    public ResponseEntity<AdvanceTransactionHistoryDTO> getAdvanceTransactionHistory(@PathVariable Long id) {
        log.debug("REST request to get AdvanceTransactionHistory : {}", id);
        Optional<AdvanceTransactionHistoryDTO> advanceTransactionHistoryDTO = advanceTransactionHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(advanceTransactionHistoryDTO);
    }

    /**
     * DELETE  /advance-transaction-histories/:id : delete the "id" advanceTransactionHistory.
     *
     * @param id the id of the advanceTransactionHistoryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/advance-transaction-histories/{id}")
    public ResponseEntity<Void> deleteAdvanceTransactionHistory(@PathVariable Long id) {
        log.debug("REST request to delete AdvanceTransactionHistory : {}", id);
        advanceTransactionHistoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
