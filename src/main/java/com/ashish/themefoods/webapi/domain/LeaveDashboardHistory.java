package com.ashish.themefoods.webapi.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A LeaveDashboardHistory.
 */
@Entity
@Table(name = "leave_dashboard_history")
public class LeaveDashboardHistory extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "available_leaves")
    private Double availableLeaves;

    @Column(name = "taken_leaves")
    private Double takenLeaves;

    @ManyToOne
    @JsonIgnoreProperties("leaveDashboardHistories")
    private Employee employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAvailableLeaves() {
        return availableLeaves;
    }

    public LeaveDashboardHistory availableLeaves(Double availableLeaves) {
        this.availableLeaves = availableLeaves;
        return this;
    }

    public void setAvailableLeaves(Double availableLeaves) {
        this.availableLeaves = availableLeaves;
    }

    public Double getTakenLeaves() {
        return takenLeaves;
    }

    public LeaveDashboardHistory takenLeaves(Double takenLeaves) {
        this.takenLeaves = takenLeaves;
        return this;
    }

    public void setTakenLeaves(Double takenLeaves) {
        this.takenLeaves = takenLeaves;
    }

    public Employee getEmployee() {
        return employee;
    }

    public LeaveDashboardHistory employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LeaveDashboardHistory leaveDashboardHistory = (LeaveDashboardHistory) o;
        if (leaveDashboardHistory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), leaveDashboardHistory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LeaveDashboardHistory{" +
            "id=" + getId() +
            ", availableLeaves=" + getAvailableLeaves() +
            ", takenLeaves=" + getTakenLeaves() +
            "}";
    }
}
