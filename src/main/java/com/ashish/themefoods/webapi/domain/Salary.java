package com.ashish.themefoods.webapi.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Salary.
 */
@Entity
@Table(name = "salary")
public class Salary extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "total_salary")
    private Double totalSalary;

    @Column(name = "basic_da")
    private Double basicDA;

    @Column(name = "hra")
    private Double hra;

    @Column(name = "conveyance")
    private Double conveyance;

    @Column(name = "other_allowances")
    private Double otherAllowances;

    @Column(name = "pf")
    private Double pf;

    @Column(name = "esi")
    private Double esi;

    @Column(name = "pt")
    private Double pt;

    @OneToOne
    @JoinColumn(unique = true, name = "employee_id")
    private Employee employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTotalSalary() {
        return totalSalary;
    }

    public Salary totalSalary(Double totalSalary) {
        this.totalSalary = totalSalary;
        return this;
    }

    public void setTotalSalary(Double totalSalary) {
        this.totalSalary = totalSalary;
    }

    public Double getBasicDA() {
        return basicDA;
    }

    public Salary basicDA(Double basicDA) {
        this.basicDA = basicDA;
        return this;
    }

    public void setBasicDA(Double basicDA) {
        this.basicDA = basicDA;
    }

    public Double getHra() {
        return hra;
    }

    public Salary hra(Double hra) {
        this.hra = hra;
        return this;
    }

    public void setHra(Double hra) {
        this.hra = hra;
    }

    public Double getConveyance() {
        return conveyance;
    }

    public Salary conveyance(Double conveyance) {
        this.conveyance = conveyance;
        return this;
    }

    public void setConveyance(Double conveyance) {
        this.conveyance = conveyance;
    }

    public Double getOtherAllowances() {
        return otherAllowances;
    }

    public Salary otherAllowances(Double otherAllowances) {
        this.otherAllowances = otherAllowances;
        return this;
    }

    public void setOtherAllowances(Double otherAllowances) {
        this.otherAllowances = otherAllowances;
    }

    public Double getPf() {
        return pf;
    }

    public Salary pf(Double pf) {
        this.pf = pf;
        return this;
    }

    public void setPf(Double pf) {
        this.pf = pf;
    }

    public Double getEsi() {
        return esi;
    }

    public Salary esi(Double esi) {
        this.esi = esi;
        return this;
    }

    public void setEsi(Double esi) {
        this.esi = esi;
    }

    public Double getPt() {
        return pt;
    }

    public Salary pt(Double pt) {
        this.pt = pt;
        return this;
    }

    public void setPt(Double pt) {
        this.pt = pt;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Salary employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Salary salary = (Salary) o;
        if (salary.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), salary.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Salary{" +
            "id=" + getId() +
            ", totalSalary=" + getTotalSalary() +
            ", basicDA=" + getBasicDA() +
            ", hra=" + getHra() +
            ", conveyance=" + getConveyance() +
            ", otherAllowances=" + getOtherAllowances() +
            ", pf=" + getPf() +
            ", esi=" + getEsi() +
            ", pt=" + getPt() +
            "}";
    }
}
