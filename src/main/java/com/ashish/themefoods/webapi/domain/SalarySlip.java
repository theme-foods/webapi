package com.ashish.themefoods.webapi.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A SalarySlip.
 */
@Entity
@Table(name = "salary_slip")
public class SalarySlip extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "total_salary")
    private Double totalSalary;

    @Column(name = "basic_da")
    private Double basicDA;

    @Column(name = "hra")
    private Double hra;

    @Column(name = "other_allowances")
    private Double otherAllowances;

    @Column(name = "pf")
    private Double pf;

    @Column(name = "esi")
    private Double esi;

    @Column(name = "pt")
    private Double pt;

    @Column(name = "days")
    private Double days;

    @Column(name = "actual_paid")
    private Double actualPaid;

    @Column(name = "payout")
    private Double payout;

    @Column(name = "advance")
    private Double advance;

    @Column(name = "salary_date")
    private ZonedDateTime salaryDate;

    @Column(name = "extra_leaves")
    private Double extraLeaves;

    @Column(name = "overtime")
    private Double overtime;

    @Column(name = "overtime_hours")
    private Double overTimeHours;

    @Column(name = "absents")
    private Double absents;

    public Double getAbsents() {
        return absents;
    }

    public void setAbsents(Double absents) {
        this.absents = absents;
    }

    @ManyToOne
    @JsonIgnoreProperties("salarySlips")
    private Employee employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTotalSalary() {
        return totalSalary;
    }

    public Double getExtraLeaves() {
        return extraLeaves;
    }

    public void setExtraLeaves(Double extraLeaves) {
        this.extraLeaves = extraLeaves;
    }

    public Double getOvertime() {
        return overtime;
    }

    public void setOvertime(Double overtime) {
        this.overtime = overtime;
    }

    public Double getOverTimeHours() {
        return overTimeHours;
    }

    public void setOverTimeHours(Double overTimeHours) {
        this.overTimeHours = overTimeHours;
    }

    public ZonedDateTime getSalaryDate() {
        return salaryDate;
    }

    public void setSalaryDate(ZonedDateTime salaryDate) {
        this.salaryDate = salaryDate;
    }

    public SalarySlip totalSalary(Double totalSalary) {
        this.totalSalary = totalSalary;
        return this;
    }

    public void setTotalSalary(Double totalSalary) {
        this.totalSalary = totalSalary;
    }

    public Double getBasicDA() {
        return basicDA;
    }

    public SalarySlip basicDA(Double basicDA) {
        this.basicDA = basicDA;
        return this;
    }

    public void setBasicDA(Double basicDA) {
        this.basicDA = basicDA;
    }

    public Double getHra() {
        return hra;
    }

    public SalarySlip hra(Double hra) {
        this.hra = hra;
        return this;
    }

    public void setHra(Double hra) {
        this.hra = hra;
    }

    public Double getOtherAllowances() {
        return otherAllowances;
    }

    public SalarySlip otherAllowances(Double otherAllowances) {
        this.otherAllowances = otherAllowances;
        return this;
    }

    public void setOtherAllowances(Double otherAllowances) {
        this.otherAllowances = otherAllowances;
    }

    public Double getPf() {
        return pf;
    }

    public SalarySlip pf(Double pf) {
        this.pf = pf;
        return this;
    }

    public void setPf(Double pf) {
        this.pf = pf;
    }

    public Double getEsi() {
        return esi;
    }

    public SalarySlip esi(Double esi) {
        this.esi = esi;
        return this;
    }

    public void setEsi(Double esi) {
        this.esi = esi;
    }

    public Double getPt() {
        return pt;
    }

    public SalarySlip pt(Double pt) {
        this.pt = pt;
        return this;
    }

    public void setPt(Double pt) {
        this.pt = pt;
    }

    public Double getDays() {
        return days;
    }

    public SalarySlip days(Double days) {
        this.days = days;
        return this;
    }

    public void setDays(Double days) {
        this.days = days;
    }

    public Double getActualPaid() {
        return actualPaid;
    }

    public SalarySlip actualPaid(Double actualPaid) {
        this.actualPaid = actualPaid;
        return this;
    }

    public void setActualPaid(Double actualPaid) {
        this.actualPaid = actualPaid;
    }

    public Double getPayout() {
        return payout;
    }

    public SalarySlip payout(Double payout) {
        this.payout = payout;
        return this;
    }

    public void setPayout(Double payout) {
        this.payout = payout;
    }

    public Double getAdvance() {
        return advance;
    }

    public SalarySlip advance(Double advance) {
        this.advance = advance;
        return this;
    }

    public void setAdvance(Double advance) {
        this.advance = advance;
    }

    public Employee getEmployee() {
        return employee;
    }

    public SalarySlip employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SalarySlip salarySlip = (SalarySlip) o;
        if (salarySlip.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), salarySlip.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SalarySlip{" +
                "id=" + id +
                ", totalSalary=" + totalSalary +
                ", basicDA=" + basicDA +
                ", hra=" + hra +
                ", otherAllowances=" + otherAllowances +
                ", pf=" + pf +
                ", esi=" + esi +
                ", pt=" + pt +
                ", days=" + days +
                ", actualPaid=" + actualPaid +
                ", payout=" + payout +
                ", advance=" + advance +
                ", salaryDate=" + salaryDate +
                ", extraLeaves=" + extraLeaves +
                ", overtime=" + overtime +
                ", overTimeHours=" + overTimeHours +
                ", employee=" + employee +
                '}';
    }
}
