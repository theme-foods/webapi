package com.ashish.themefoods.webapi.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Documents.
 */
@Entity
@Table(name = "documents")
public class Documents extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "document_url")
    private String documentUrl;

    @ManyToOne
    @JsonIgnoreProperties("documents")
    private Employee employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public Documents documentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
        return this;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Documents employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Documents documents = (Documents) o;
        if (documents.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), documents.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Documents{" +
            "id=" + getId() +
            ", documentUrl='" + getDocumentUrl() + "'" +
            "}";
    }
}
