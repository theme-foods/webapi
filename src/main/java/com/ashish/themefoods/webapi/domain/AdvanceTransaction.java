package com.ashish.themefoods.webapi.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A AdvanceTransaction.
 */
@Entity
@Table(name = "advance_transaction")
public class AdvanceTransaction extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "taken_on")
    private ZonedDateTime takenOn;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "type_of_advance")
    private String typeOfAdvance;

    @Column(name = "tenure")
    private Integer tenure;

    @Column(name = "reason")
    private String reason;

    @ManyToOne
    @JsonIgnoreProperties("advanceTransactions")
    private Employee employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getTakenOn() {
        return takenOn;
    }

    public AdvanceTransaction takenOn(ZonedDateTime takenOn) {
        this.takenOn = takenOn;
        return this;
    }

    public void setTakenOn(ZonedDateTime takenOn) {
        this.takenOn = takenOn;
    }

    public Double getAmount() {
        return amount;
    }

    public AdvanceTransaction amount(Double amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTypeOfAdvance() {
        return typeOfAdvance;
    }

    public AdvanceTransaction typeOfAdvance(String typeOfAdvance) {
        this.typeOfAdvance = typeOfAdvance;
        return this;
    }

    public void setTypeOfAdvance(String typeOfAdvance) {
        this.typeOfAdvance = typeOfAdvance;
    }

    public Integer getTenure() {
        return tenure;
    }

    public AdvanceTransaction tenure(Integer tenure) {
        this.tenure = tenure;
        return this;
    }

    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    public String getReason() {
        return reason;
    }

    public AdvanceTransaction reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Employee getEmployee() {
        return employee;
    }

    public AdvanceTransaction employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AdvanceTransaction advanceTransaction = (AdvanceTransaction) o;
        if (advanceTransaction.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), advanceTransaction.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdvanceTransaction{" +
            "id=" + getId() +
            ", takenOn='" + getTakenOn() + "'" +
            ", amount=" + getAmount() +
            ", typeOfAdvance='" + getTypeOfAdvance() + "'" +
            ", tenure=" + getTenure() +
            ", reason='" + getReason() + "'" +
            "}";
    }
}
