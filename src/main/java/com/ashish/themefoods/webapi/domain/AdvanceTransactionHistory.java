package com.ashish.themefoods.webapi.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A AdvanceTransactionHistory.
 */
@Entity
@Table(name = "advance_transaction_history")
public class AdvanceTransactionHistory extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "taken_on")
    private ZonedDateTime takenOn;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "type_of_advance")
    private String typeOfAdvance;

    @Column(name = "tenure")
    private Integer tenure;

    @Column(name = "reason")
    private String reason;

    @ManyToOne
    @JsonIgnoreProperties("advanceTransactionHistories")
    private Employee employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getTakenOn() {
        return takenOn;
    }

    public AdvanceTransactionHistory takenOn(ZonedDateTime takenOn) {
        this.takenOn = takenOn;
        return this;
    }

    public void setTakenOn(ZonedDateTime takenOn) {
        this.takenOn = takenOn;
    }

    public Double getAmount() {
        return amount;
    }

    public AdvanceTransactionHistory amount(Double amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTypeOfAdvance() {
        return typeOfAdvance;
    }

    public AdvanceTransactionHistory typeOfAdvance(String typeOfAdvance) {
        this.typeOfAdvance = typeOfAdvance;
        return this;
    }

    public void setTypeOfAdvance(String typeOfAdvance) {
        this.typeOfAdvance = typeOfAdvance;
    }

    public Integer getTenure() {
        return tenure;
    }

    public AdvanceTransactionHistory tenure(Integer tenure) {
        this.tenure = tenure;
        return this;
    }

    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    public String getReason() {
        return reason;
    }

    public AdvanceTransactionHistory reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Employee getEmployee() {
        return employee;
    }

    public AdvanceTransactionHistory employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AdvanceTransactionHistory advanceTransactionHistory = (AdvanceTransactionHistory) o;
        if (advanceTransactionHistory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), advanceTransactionHistory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdvanceTransactionHistory{" +
            "id=" + getId() +
            ", takenOn='" + getTakenOn() + "'" +
            ", amount=" + getAmount() +
            ", typeOfAdvance='" + getTypeOfAdvance() + "'" +
            ", tenure=" + getTenure() +
            ", reason='" + getReason() + "'" +
            "}";
    }
}
